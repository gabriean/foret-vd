from datetime import date

from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from leaflet.admin import LeafletGeoAdmin

from inventory import models
from inventory.models.db_views import AME_CE
from .forms import VDOSMWidget, FutureProtocolForm, PassedProtocolForm


class VocabValueInline(admin.TabularInline):
    model = models.VocabValue

@admin.register(models.Vocabulary)
class VocabularyAdmin(admin.ModelAdmin):
    inlines = [VocabValueInline]


@admin.register(models.OwnerType)
class OwnerTypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'num_code', 'alpha_code', 'description')
    ordering = ('num_code',)


@admin.register(models.Owner)
class OwnerAdmin(admin.ModelAdmin):
    list_display = ('code', 'name')
    list_filter = ('typ',)
    search_fields = ('num', 'name')
    ordering = ('typ__alpha_code', 'num')


@admin.register(models.Intervention)
class InterventionAdmin(admin.ModelAdmin):
    list_display = ['name', 'active', 'code', 'default_pos']
    list_filter = ['active']


@admin.register(models.Nature)
class NatureAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'code', 'woods')


@admin.register(models.Density)
class DensityAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'surface')


@admin.register(models.Protocol)
class ProtocolAdmin(LeafletGeoAdmin):
    filter_horizontal = ('owners',)
    fieldsets = (
        (None, {
            'fields': (('name', 'pfile'), ('year', 'inv_date', 'draft', 'passed'),  #'geom',
                       ('vocab_posp8', 'vocab_posp9'), ('vocab_posd6', 'vocab_posd7')),
        }),
        ("Intervention", {
            'fields': (('interv_0', 'interv_1', 'interv_2'), ('interv_3', 'interv_4', 'interv_5'),
                       ('interv_6', 'interv_7', 'interv_8'), ('interv_9',)),
        }),
        ("Essences", {
            'description': "1: <b>RD</b> (résineux divers), 2: <b>Do</b> (Douglas), "
                           "3: <b>Me</b> (Mélèze), 4: <b>Sa</b> (Sapin), 5: <b>Ep</b> (Epicéa), "
                           "6: <b>He</b> (Hêtre), 7: <b>Er</b> (Erable),<br> 8: <b>Ch</b> (Chêne), "
                           "9: <b>FD</b> (Feuillus divers), A: <b>Fr</b> (Frêne), "
                           "B: <b>Mr</b> (Merisier), </b>C: <b>Pe</b> (Peuplier), D: <b>Pi</b> (Pin)",
            'fields': (('spec_posE', 'spec_posF'),),
        }),
        (None, {
            'fields': ('owners', 'geom', 'comments'),
        }),
    )
    list_display = ['name_or_filename', 'pfile', 'year', 'passed', 'owners_list']
    list_editable = ['passed']
    search_fields = ['name', 'pfile']
    ordering = ('-year',)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs.prefetch_related('owners')
        return qs

    #def get_form(self, request, obj=None, **kwargs):
    #    return PassedProtocolForm if (obj and obj.passed) else FutureProtocolForm

    def name_or_filename(self, obj):
        name = obj.name if obj.name else obj.pfile.name.rsplit('.', 1)[0].split('/')[-1]
        if len(name) > 30:
            name = name[:15] + '…' + name[-15:]
        return name
    name_or_filename.short_description = "Protocole"

    def owners_list(self, obj):
        return ", ".join([str(p) for p in obj.owners.all()])
    owners_list.short_description = "Propriétaires"


@admin.register(models.TreeSpecies)
class TreeSpeciesAdmin(admin.ModelAdmin):
    ordering = ('order',)
    list_display = ['species', 'abbrev', 'typ', 'active', 'order', 'code', 'code_fbase']
    list_filter = ['active']


class PlotForm(forms.ModelForm):
    class Meta:
        model = models.Plot
        fields = '__all__'
        widgets = {
            'center': VDOSMWidget,
        }


@admin.register(models.Plot)
class PlotAdmin(admin.ModelAdmin):
    form = PlotForm


@admin.register(models.PlotObsPlanned)
class PlotObsAdmin(admin.ModelAdmin):
    list_display = ('plot', 'protocol')


@admin.register(models.PlotObs)
class PlotObsAdmin(admin.ModelAdmin):
    list_display = ('plot', 'year')
    list_select_related = ('plot', 'imp_file')
    raw_id_fields = ['plot', 'protocol', 'imp_file', 'owner']


@admin.register(models.Tree)
class TreeAdmin(admin.ModelAdmin):
    raw_id_fields = ['obs']


@admin.register(AME_CE)
class AME_CEAdmin(admin.ModelAdmin):
    list_display = ['CoordX', 'CoordY', 'inv_Protocole']


@admin.register(models.Topap)
class TopapAdmin(admin.ModelAdmin):
    list_display = ['guidpa', 'lieu_dit', 'serie_div', 'propri', 'code']
    search_fields = ['lieu_dit', 'triage_ge', 'propri']


@admin.register(models.User)
class UserAdmin(DjangoUserAdmin):
    list_display = list(DjangoUserAdmin.list_display) + ['is_superuser', 'groupes']

    def groupes(self, obj):
        return ', '.join([g.name for g in obj.groups.all()])
