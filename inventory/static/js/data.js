function populate_grouping() {
    // Dynamically populate grouping checkboxes depending on chosen views
    $('div#grouping-optional').html('');
    if ($('input[name=bio]:checked').val()) {
        $.each($("input[name=bio]:checked"), function() {
            var model = views[$(this).val()];
            var keys = model['groupables'];
            for (i=0; i<keys.length; i++) {
                var label = model['model'][keys[i]];
                if (label === undefined) {
                    if (keys[i] == 'spec') label = "Essence";
                    else label = views['plotobs']['model'][keys[i]];
                }
                $('div#grouping-optional').append('<input id="cb_'+keys[i]+'" type="checkbox" name="aggr" value="'+keys[i]+'"> <label for="cb_'+keys[i]+'">'+label+'</label><br>');
            }
        });
    }
}

function highlight_perimeter_choices(choice_groups) {
    $.each(choice_groups, function(idx, val) {
        var num_checked = $(choice_groups[idx]).find('input:checked').length;
        var group = $(choice_groups[idx]);
        if (num_checked > 0) {
            group.addClass('selected');
            group.find('span.num_choices').html(num_checked);
            group.find('img.clear_choices').show();
        } else {
            group.removeClass('selected');
            group.find('span.num_choices').html('');
            group.find('img.clear_choices').hide();
        }
    });
}

function filter_choices(container, value) {
    // With a value coming from a text input, this allows filtering lengthy
    // checkboxes lists.
    var search_regex = new RegExp("(" + value + ")", "gi");
    if (value) {
        var cbs = $(container).find('input[type=checkbox]');
        $.each(cbs, function(idx, val) {
            var cont_div = $(this.parentNode);
            if (cont_div.text().match(search_regex) || $(this).is(':checked')) {
                if (!cont_div.is(':visible')) cont_div.show();
            } else cont_div.hide();
        });
    }
    else {
        $(container).find(':hidden').show();
    }
    return false;
}

$(document).ready(function() {
    $(":button", "div#refresh").click(function (ev) {
        $('img#wait').show();
        form_data = $("#dataform").serialize();
        // metadata may be used by the map
        //$.getJSON($("#dataform").attr('action'), form_data + '&metadata=1&format=json', function(data) {
        //    metadata = data;
        //});
        if ($('li#data-tab').hasClass('active')) {
            $("div#datamain").load($("#dataform").attr('action'), form_data, function(response, status, xhr) {
                $('img#wait').hide();
                if (status == "error") $('div#server_error').show();
                else {
                    $('div#server_error').hide();
                    $("table.tablesorter").tablesorter({
                        sortList: [[0,0]]
                        //widgets: ["filter"]
                    });
                }
            });
        }// else {
        //    map.addLayer(form_data);
        //}
    });
    $("#csv-export").click(function (ev) {
        ev.preventDefault();
        form_data = $("#dataform").serialize();
        window.location.href = $("#dataform").attr('action') + '?format=csv&' + form_data;
    });
    $('li.tab').click(function(ev) {
        if ($(this).hasClass('active')) return false;
        $('li.tab').removeClass('active');
        $(this).addClass('active');
        $('div#mapmain, div#datamain').toggle();
        if (this.id == 'map-tab') {
            if (map == null) {
                map = new mapObject();
            }
            if (form_data != null && map.form_data != form_data+'&format=json') {
                // Load data into the map
                map.addLayer(form_data);
            }
        } else {
            $('div#client_error').hide();
        }
    });
    $('ul#cols').on('click', 'input', function (ev) {
        if (map.vector_layer.styled_with == this.value) {
            map.vector_layer.setStyle();
            map.vector_layer.styled_with = 'default';
            $(this).prop('checked', false);
        } else map.styleMap(this.value);
    });
    $('img.clear_choices').click(function(ev) {
        ev.stopPropagation();
        ev.preventDefault();
        $(this).parent().next().find('input:checked').prop('checked', false);
        highlight_perimeter_choices($(this).closest('div.perim_choice'));
    });
    $('input[name=bio]').click(function () {
        $('div.view-details').hide();
        $('div#details-' + $(this).attr('id')).show();
        populate_grouping();
    });

    $(document).tooltip();
    populate_grouping();

    $('div.perim_choice input').change(function() {
        highlight_perimeter_choices($(this).closest('div.perim_choice'));
    });
    highlight_perimeter_choices($('div.perim_choice'));

    // Delay when user type in choice filter input
    var timer;
    $("input.choice_filter").keyup(function() {
        clearTimeout(timer);
        var ms = 1000;
        var val = this.value;
        var parent = this.parentNode;
        timer = setTimeout(function() {
            filter_choices(parent, val);
        }, ms);
    });
});
