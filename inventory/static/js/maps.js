"use strict";

function defaultPointFunc (feature, latlng) {
    return L.circleMarker(latlng, {radius: 8, fillColor: "#ff7800", color: "#000", weight: 1});
}

/* WMS VD
* Couches disponibles:
* https://www.asitvd.ch/media/easysdi/md-attachment/1fab243b-0af1-4564-f1b6-2baefc54bb81_wmsVD_Metadonnees_2020-09.pdf
* GetCapabilities:
* https://www.ogc.vd.ch/public/services/OGC/wmsVD/Mapserver/WMSServer?request=GetCapabilities&service=WMS
*/
const VD_WMS_URL = 'https://www.ogc.vd.ch/public/services/OGC/wmsVD/Mapserver/WMSServer?';
const epsg2056String = "+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs";

class GeoMap {
    constructor(mapDiv, options={}) {
        this.crs = L.TileLayer.Swiss.EPSG_2056;
        this.map = L.map(
            mapDiv, {
                crs: this.crs,
                maxBounds: L.TileLayer.Swiss.latLngBounds
            }
        );
        this.baseLayers = {
            'Carte couleur': L.tileLayer.swiss({layer: 'ch.swisstopo.pixelkarte-farbe'}),
            'Orthophoto': L.tileLayer.swiss({layer: 'ch.swisstopo.swissimage'}),
            'Plan d’ensemble': L.tileLayer.wms(
                'http://guaraci.vserver.softronics.ch/cgi-bin/inv-vd?', {
                layers: 'Plan ensemble VD',
                maxZoom: 28
            })
        }
        const communesLayer = L.tileLayer.wms(
            VD_WMS_URL,
            {layers: 'vd.commune', crs: this.crs, format: 'image/png', version: '1.3.0', transparent: true, maxZoom: 28}
        );
        /*const bienfondsLayer = L.tileLayer.wms(
            VD_WMS_URL,
            {layers: 'vd.bien_fond', crs: this.crs, format: 'image/png', version: '1.3.0', transparent: true, maxZoom: 28}
        );*/
        this.overLayers = {
            'Hauteur végé': L.tileLayer.swiss({
                layer: 'ch.bafu.landesforstinventar-vegetationshoehenmodell', format: 'png', opacity: 0.5
            }),
            'Communes': communesLayer
            //'Biens-fonds': bienfondsLayer,
        }
        this.baseLayers['Carte couleur'].addTo(this.map);
        if (options.gridLayer) {
            const gridLayer = new L.MetricGrid({
                proj4ProjDef: epsg2056String,
                bounds: [[2485000, 1075000] , [2830000, 1300000]],
                color: '#5272E5'
            });
            gridLayer.addTo(this.map);
        }

        this.map.setView(L.TileLayer.Swiss.unproject_2056(L.point([2530000, 1155000])), 17);
        this.layerControl = L.control.layers(this.baseLayers, this.overLayers);
        this.layerControl.addTo(this.map);
        if (options.withLasso) {
            const lassoControl = L.control.lasso().addTo(this.map);
            this.map.on('lasso.finished', event => {
                this.selectPoints(event.layers);
                options.lassoSelected();
            });
        }
        this.map.on('click', this.getFeatureInfo, this);
        this.selectedPoints = [];
    }

    addLayerFromGeoJSON(geojson, fitMap) {
        // geojson is expected to use the 4326 SRID.
        var lyr = L.geoJSON().addTo(this.map);
        lyr.addData(geojson);
        if (fitMap) this.map.fitBounds(lyr.getBounds());
    }

    addLayerFromURL(url, options) {
        const loading = document.getElementById('loading');
        loading.style.display = 'block';
        return fetch(url).then((response) => response.json())
        .then((data) => {
            var geoJsonLayer = L.geoJSON(data, {
                pointToLayer: options.pointFunction || defaultPointFunc
            }).on('click', options.clickHandler);
            if (options.clustered) {
                var cluster = L.markerClusterGroup({maxClusterRadius: 38});
                cluster.addLayer(geoJsonLayer);
                if (options.initial !== 'hidden') this.map.addLayer(cluster);
                loading.style.display = 'none';
                return [geoJsonLayer, cluster];
            } else {
                if (options.initial !== 'hidden') this.map.addLayer(geoJsonLayer);
                loading.style.display = 'none';
                return geoJsonLayer;
            }
        });
    }

    hideLayer(lyr) {
        lyr.clearLayers();
    }

    selectPoints(lyrs) {
        const prevSelected = document.querySelectorAll('div.leaflet-marker-icon.selected');
        if (prevSelected.length) {
            prevSelected.forEach(item => item.classList.remove('selected'));
        }
        this.selectedPoints = [];
        lyrs.forEach(lyr => {
            if (typeof lyr.getElement === 'function') {
                let el = lyr.getElement();
                if (el) {
                    el.classList.add('selected');
                    this.selectedPoints.push(lyr);
                }
            }
        });
    }

    getFeatureInfo(evt, layer) {
        if (layer === undefined) return;
        const url = this.getFeatureInfoUrl(evt.latlng, layer);
        return fetch(url).then(response => {
            return response.json().then(json => {
                return json;
            });
        });
    }

    // Inspiré de https://gist.github.com/rclark/6908938
    getFeatureInfoUrl(latlng, layer) {
        // Construct a GetFeatureInfo request URL given a point
        const point = this.map.latLngToContainerPoint(latlng, this.map.getZoom()),
              size = this.map.getSize(),
              crs_bbox = L.bounds(
                this.crs.project(this.map.getBounds().getSouthWest()),
                this.crs.project(this.map.getBounds().getNorthEast())
              ),
              crs_bbox_str = crs_bbox.min.x + ',' + crs_bbox.min.y + ',' + crs_bbox.max.x + ',' + crs_bbox.max.y;
        const params = {
            request: 'GetFeatureInfo',
            service: 'WMS',
            crs: 'EPSG:2056',
            //styles: this.wmsParams.styles,
            version: '1.3.0',
            //format: this.wmsParams.format,
            bbox: crs_bbox_str,
            height: size.y,
            width: size.x,
            layers: layer,
            query_layers: layer,
            info_format: 'application/geojson',
            i: point.x,
            j: point.y
        };
        return VD_WMS_URL + L.Util.getParamString(params, VD_WMS_URL, false);
    }
}
