import math
import re
from decimal import Decimal, ROUND_HALF_UP

from django.contrib.auth.models import AbstractUser
from django.contrib.gis.db import models as gis_models
from django.contrib.gis.db.models.functions import Centroid, Envelope
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import OuterRef, Subquery
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.text import Truncator

from municipality.models import Municipality
from inventory.utils import diff_lines

TARIF_VOLUME_VD = {
    10: 7, 11: 8, 12: 9, 13: 10, 14: 11, 15: 13,
    16: 15, 17: 17, 18: 19, 19: 21, 20: 24, 21: 27,
    22: 31, 23: 35, 24: 39, 25: 43, 26: 48, 27: 53,
    28: 58, 29: 64, 30: 70, 31: 76, 32: 82, 33: 88,
    34: 95, 35: 102, 36: 109, 37: 117, 38: 125, 39: 133,
    40: 142, 41: 151, 42: 160, 43: 169, 44: 178, 45: 188,
    46: 198, 47: 208, 48: 218, 49: 229, 50: 240, 51: 251,
    52: 262, 53: 273, 54: 284, 55: 296, 56: 308, 57: 320,
    58: 333, 59: 346, 60: 359, 61: 373, 62: 387, 63: 401,
    64: 415, 65: 429, 66: 444, 67: 459, 68: 475, 69: 491,
    70: 507, 71: 523, 72: 540, 73: 557, 74: 574, 75: 591,
    76: 608, 77: 625, 78: 643, 79: 661, 80: 679, 81: 697,
    82: 715, 83: 733, 84: 752, 85: 771, 86: 790, 87: 809,
    88: 828, 89: 848, 90: 868, 91: 888, 92: 908, 93: 928,
    94: 949, 95: 970, 96: 991, 97: 1012, 98: 1033, 99: 1055,
    100: 1077, 101: 1099, 102: 1121, 103: 1143, 104: 1166, 105: 1189,
    106: 1212, 107: 1236, 108: 1260, 109: 1284, 110: 1310, 111: 1337,
}


class User(AbstractUser):
    phone = models.CharField("Téléphone", max_length=40, blank=True)

    class Meta:
        db_table = 'auth_user'

    def __str__(self):
        return self.get_full_name() or self.username


class Vocabulary(models.Model):
    name = models.CharField(max_length=250)
    comment = models.TextField(blank=True)

    class Meta:
        db_table = 'vocabulary'
        verbose_name = 'vocabulaire'
        ordering = ('name',)

    def __str__(self):
        return self.name


class VocabValue(models.Model):
    vocab = models.ForeignKey(Vocabulary, on_delete=models.CASCADE)
    code = models.CharField(max_length=1)
    label = models.CharField(max_length=100)
    class Meta:
        db_table = 'vocabvalue'
        verbose_name = 'valeur de vocabulaire'

    def __str__(self):
        return "%s (%s)" % (self.label, self.code)


class OwnerType(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    num_code = models.SmallIntegerField()
    alpha_code = models.CharField(max_length=2)

    class Meta:
        db_table = 'owner_type'
        verbose_name = 'type de propriétaire'
        indexes = [models.Index(name='alpha_code', fields=['alpha_code'])]

    def __str__(self):
        return self.name


class Owner(models.Model):
    name = models.CharField(max_length=120, blank=True)
    typ = models.ForeignKey(OwnerType, db_column='type_id', on_delete=models.PROTECT)
    num = models.CharField(max_length=3, help_text="Position Ipasmofix")

    class Meta:
        db_table = 'owner'
        verbose_name = "propriétaire"
        indexes = [models.Index(name='num_index', fields=['num'])]
        unique_together = ('typ', 'num')

    def __str__(self):
        return self.title

    @property
    def code(self):
        return "%s%s" % (self.typ.alpha_code, self.num)

    @classmethod
    def get_by_code(cls, code):
        try:
            return Owner.objects.get(typ__alpha_code=code[0], num=code[1:4])
        except (Owner.DoesNotExist, IndexError):
            return None

    @property
    def title(self):
        num = self.code
        if self.name:
            num += " - %s" % self.name
        return num


class PlotQuerySet(models.QuerySet):
    def as_geojson(self):
        geojson = {"type": "FeatureCollection", "features": []}
        for obj in self:
            obj.center.transform(4326)
            geojson['features'].append({
                "type": "Feature",
                "id": obj.pk,
                "geometry": eval(obj.center.json),
                "properties": {"sealevel": obj.sealevel},
            })
        return geojson


class Plot(gis_models.Model):
    center = gis_models.PointField(srid=2056, verbose_name="Coordonnées")
    sealevel = models.SmallIntegerField("Altitude", blank=True, null=True)
    # Should probably not be optional finally
    municipality = models.ForeignKey(Municipality, null=True, blank=True, on_delete=models.PROTECT)

    objects = PlotQuerySet.as_manager()

    class Meta:
        db_table = 'plot'
        verbose_name = 'placette'

    def __str__(self):
        return "Placette x=%d y=%d" % (self.center.x, self.center.y)

    def as_geojson(self, srid=None, geom_field='center', obs_urls=True):
        geom = getattr(self, geom_field)
        if (srid is not None and srid != geom.srid):
            geom = geom.transform(srid, clone=True)
        geojson = {
            "type": "Feature",
            "id": self.pk,
            "properties": {
                "id": self.pk,
            },
            "geometry": eval(geom.json),
        }
        if obs_urls:
            geojson["properties"]["obsURLs"] = {
                str(obs.year): reverse('plotobs-detail-json', args=[obs.pk])
                for obs in self.plotobs_set.all()
            }
        return geojson


class Density(models.Model):
    name = models.CharField(max_length=50)
    code = models.SmallIntegerField()
    surface = models.IntegerField("Surface représentée en m2")

    class Meta:
        db_table = 'density'
        verbose_name = 'densité'

    def __str__(self):
        return self.name


class Intervention(models.Model):
    name = models.CharField(max_length=50)
    code = models.CharField("Code fbase", max_length=1, blank=True)
    default_pos = models.SmallIntegerField(null=True, blank=True, unique=True,
        help_text="Position par défaut (0-9) de cette intervention sur la fiche Ipasmofix")
    active = models.BooleanField(default=False)

    class Meta:
        db_table = 'intervention'
        ordering = ('name',)

    def __str__(self):
        return self.name


class Nature(models.Model):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=255)
    woods = models.BooleanField("Boisé", default=True)

    class Meta:
        db_table = 'nature'
        ordering = ('code',)

    def __str__(self):
        return Truncator(self.description).chars(45)


class Protocol(models.Model):
    name = models.CharField("Nom", max_length=200, blank=True)
    pfile = models.FileField(upload_to='protocols', blank=True, verbose_name="Fichier de protocole")
    year = models.SmallIntegerField("Année d'inventaire")
    passed = models.BooleanField(default=True, help_text="L’inventaire est terminé.")
    default_density = models.ForeignKey(
        Density, on_delete=models.PROTECT, verbose_name="Densité par défaut", null=True, blank=True
    )
    geom = gis_models.MultiPolygonField(srid=2056, verbose_name="Surface d’inventaire", null=True, blank=True)
    inv_date = models.CharField("Période d’inventaire", max_length=200)
    draft = models.BooleanField(default=False)
    # Vocabularies for reserved fields
    vocab_posp8 = models.ForeignKey(Vocabulary, verbose_name="Champ libre 1",
        help_text="Vocabulaire du champ libre 1 (PARC, position 8)",
        null=True, blank=True, on_delete=models.PROTECT, related_name='+')
    vocab_posp9 = models.ForeignKey(Vocabulary, verbose_name="Champ libre 2",
        help_text="Vocabulaire du champ libre 2 (PARC, position 9)",
        null=True, blank=True, on_delete=models.PROTECT, related_name='+')
    vocab_posd6 = models.ForeignKey(Vocabulary, verbose_name="Champ libre 3",
        help_text="Vocabulaire du champ libre 3 (DESP, position 6)",
        null=True, blank=True, on_delete=models.PROTECT, related_name='+')
    vocab_posd7 = models.ForeignKey(Vocabulary, verbose_name="Champ libre 4",
        help_text="Vocabulaire du champ libre 4 (DESP, position 7)",
        null=True, blank=True, on_delete=models.PROTECT, related_name='+')
    interv_0 = models.ForeignKey(Intervention, related_name='+', null=True, blank=True, on_delete=models.PROTECT)
    interv_1 = models.ForeignKey(Intervention, related_name='+', null=True, blank=True, on_delete=models.PROTECT)
    interv_2 = models.ForeignKey(Intervention, related_name='+', null=True, blank=True, on_delete=models.PROTECT)
    interv_3 = models.ForeignKey(Intervention, related_name='+', null=True, blank=True, on_delete=models.PROTECT)
    interv_4 = models.ForeignKey(Intervention, related_name='+', null=True, blank=True, on_delete=models.PROTECT)
    interv_5 = models.ForeignKey(Intervention, related_name='+', null=True, blank=True, on_delete=models.PROTECT)
    interv_6 = models.ForeignKey(Intervention, related_name='+', null=True, blank=True, on_delete=models.PROTECT)
    interv_7 = models.ForeignKey(Intervention, related_name='+', null=True, blank=True, on_delete=models.PROTECT)
    interv_8 = models.ForeignKey(Intervention, related_name='+', null=True, blank=True, on_delete=models.PROTECT)
    interv_9 = models.ForeignKey(Intervention, related_name='+', null=True, blank=True, on_delete=models.PROTECT)
    spec_pos1 = models.ForeignKey('TreeSpecies', verbose_name="Essence en position 1",
        null=True, blank=True, related_name='+', on_delete=models.PROTECT)
    spec_pos2 = models.ForeignKey('TreeSpecies', verbose_name="Essence en position 2",
        null=True, blank=True, related_name='+', on_delete=models.PROTECT)
    spec_pos3 = models.ForeignKey('TreeSpecies', verbose_name="Essence en position 3",
        null=True, blank=True, related_name='+', on_delete=models.PROTECT)
    spec_pos4 = models.ForeignKey('TreeSpecies', verbose_name="Essence en position 4",
        null=True, blank=True, related_name='+', on_delete=models.PROTECT)
    spec_pos5 = models.ForeignKey('TreeSpecies', verbose_name="Essence en position 5",
        null=True, blank=True, related_name='+', on_delete=models.PROTECT)
    spec_pos6 = models.ForeignKey('TreeSpecies', verbose_name="Essence en position 6",
        null=True, blank=True, related_name='+', on_delete=models.PROTECT)
    spec_pos7 = models.ForeignKey('TreeSpecies', verbose_name="Essence en position 7",
        null=True, blank=True, related_name='+', on_delete=models.PROTECT)
    spec_pos8 = models.ForeignKey('TreeSpecies', verbose_name="Essence en position 8",
        null=True, blank=True, related_name='+', on_delete=models.PROTECT)
    spec_pos9 = models.ForeignKey('TreeSpecies', verbose_name="Essence en position 9",
        null=True, blank=True, related_name='+', on_delete=models.PROTECT)
    spec_posA = models.ForeignKey('TreeSpecies', verbose_name="Essence en position A",
        null=True, blank=True, related_name='+', on_delete=models.PROTECT)
    spec_posB = models.ForeignKey('TreeSpecies', verbose_name="Essence en position B",
        null=True, blank=True, related_name='+', on_delete=models.PROTECT)
    spec_posC = models.ForeignKey('TreeSpecies', verbose_name="Essence en position C",
        null=True, blank=True, related_name='+', on_delete=models.PROTECT)
    spec_posD = models.ForeignKey('TreeSpecies', verbose_name="Essence en position D",
        null=True, blank=True, related_name='+', on_delete=models.PROTECT)
    spec_posE = models.ForeignKey('TreeSpecies', verbose_name="Essence en position E",
        null=True, blank=True, related_name='+', on_delete=models.PROTECT)
    spec_posF = models.ForeignKey('TreeSpecies', verbose_name="Essence en position F",
        null=True, blank=True, related_name='+', on_delete=models.PROTECT)
    comments = models.TextField("Commentaires", blank=True)
    owners = models.ManyToManyField(Owner, verbose_name="Propriétaires", blank=True)

    class Meta:
        db_table = 'protocol'
        verbose_name = 'protocole'

    def __str__(self):
        if self.name:
            return self.name
        return "%s - %s" % (self.year, self.pfile.name)

    def save(self, **kwargs):
        initial_plots = not self.pk and self.geom
        super().save(**kwargs)
        if initial_plots:
            # Create related PlotObsPlanned inside the geom surface
            latest_obs = PlotObs.objects.filter(
                plot_id=OuterRef("id"),
            ).order_by("-protocol__year")
            plots = Plot.objects.filter(center__within=self.geom).annotate(
                latest_ser_num=Subquery(latest_obs.values('ser_num')[:1]),
                latest_div_num=Subquery(latest_obs.values('div_num')[:1]),
                latest_owner_id=Subquery(latest_obs.values('owner_id')[:1]),
            )
            owner_ids = set()
            for plot in plots:
                PlotObsPlanned.objects.create(
                    plot=plot, protocol=self,
                    ser_num=plot.latest_ser_num, div_num=plot.latest_div_num,
                    owner_id=plot.latest_owner_id
                )
                owner_ids.add(plot.latest_owner_id)
            self.owners.set(Owner.objects.filter(pk__in=owner_ids))

    def center(self, srid=2056):
        if self.geom:
            center = self.geom.centroid
        else:
            query = Plot.objects.filter(
                models.Q(plotobs__protocol=self) | models.Q(plotobsplanned__protocol=self)
            ).aggregate(center=Centroid(gis_models.Collect('center')))
            center = query['center']
        if center.srid != srid:
            center = center.transform(srid, clone=True)
        return center

    def envelope(self):
        if self.geom:
            return self.geom
        else:
            return Plot.objects.filter(
                models.Q(plotobs__protocol=self) | models.Q(plotobsplanned__protocol=self)
            ).aggregate(env=Envelope(gis_models.Collect('center')))['env']

    def nb_plots(self):
        return self.plotobs_set.count() + self.plotobsplanned_set.count()

    def get_vocabs(self):
        return [
            getattr(self, fname)
            for fname in ['vocab_posp8', 'vocab_posp9', 'vocab_posd6', 'vocab_posd7']
            if getattr(self, fname)
        ]


class PlotObsPlanned(models.Model):
    plot = models.ForeignKey(Plot, on_delete=models.PROTECT, verbose_name="Placette")
    protocol = models.ForeignKey(Protocol, on_delete=models.CASCADE)
    owner = models.ForeignKey(Owner, on_delete=models.PROTECT, verbose_name="Propriétaire", null=True)
    ser_num = models.PositiveSmallIntegerField(default=0, verbose_name="N° série", blank=True, null=True)
    div_num = models.PositiveSmallIntegerField(default=0, verbose_name="N° division", blank=True, null=True)
    operator = models.ForeignKey(
        User, blank=True, null=True, on_delete=models.SET_NULL, verbose_name="Opérateur"
    )

    class Meta:
        db_table = 'plot_obs_planned'
        verbose_name = "Placette à inventorier"
        verbose_name_plural = "Placettes à inventorier"
        constraints = [
            models.UniqueConstraint(fields=['plot', 'protocol'], name='unique_plot_protocol')
        ]

    @property
    def division(self):
        return str(self.div_num).zfill(2) if self.div_num is not None else None

    def detail_url(self):
        return reverse('plotobs_planned_detail', args=[self.pk])


class PlotObs(models.Model):
    uid = models.IntegerField(unique=True, null=True, blank=True)
    plot = models.ForeignKey(Plot, on_delete=models.PROTECT, verbose_name="Placette")
    protocol = models.ForeignKey(Protocol, on_delete=models.PROTECT)
    imp_file = models.ForeignKey('imports.FileImport', null=True, blank=True, on_delete=models.CASCADE)
    owner = models.ForeignKey(Owner, on_delete=models.PROTECT, verbose_name="Propriétaire")
    radius = models.PositiveSmallIntegerField("Rayon de placette")
    slope = models.PositiveSmallIntegerField("Pente")
    ser_num = models.PositiveSmallIntegerField(default=0, verbose_name="N° série")
    div_num = models.PositiveSmallIntegerField(default=0, verbose_name="N° division")
    density = models.ForeignKey(Density, on_delete=models.PROTECT, verbose_name="Densité")
    main_nature = models.ForeignKey(Nature, on_delete=models.PROTECT, verbose_name="Nature principale de la placette")
    main_perc = models.PositiveSmallIntegerField("% nature principale")
    reserved1 = models.CharField("Champ libre 1", max_length=1, default='', blank=True)
    reserved2 = models.CharField("Champ libre 2", max_length=1, default='', blank=True)
    unnumbered = models.PositiveSmallIntegerField("Tiges non dénombrées")
    conifer = models.PositiveSmallIntegerField("% rés. non dénombrés")
    # TODO: migrate imported values when the import process is complete.
    ecorcage = models.PositiveSmallIntegerField("Écorçage du cerf", null=True, blank=True)
    interv_imp = models.SmallIntegerField("Code intervention", null=True, blank=True)
    interv_int = models.ForeignKey(Intervention, null=True, on_delete=models.PROTECT, verbose_name="Intervention")
    reserved3 = models.CharField("Champ libre 3", max_length=1, default='', blank=True)
    reserved4 = models.CharField("Champ libre 4", max_length=1, default='', blank=True)
    gibi_epicea = models.PositiveSmallIntegerField("Nombre total d’épicéas", null=True, blank=True)
    gibi_epicea_f = models.PositiveSmallIntegerField("Proportion d’épicéa frotté", null=True, blank=True)
    gibi_epicea_a = models.PositiveSmallIntegerField("Proportion d’épicéa abroutis", null=True, blank=True)
    gibi_conifer = models.PositiveSmallIntegerField("Nombre total d’autres résineux", null=True, blank=True)
    gibi_conifer_f = models.PositiveSmallIntegerField("Proportion d’autres résineux frotté", null=True, blank=True)
    gibi_conifer_a = models.PositiveSmallIntegerField("Proportion d’autres résineux abroutis", null=True, blank=True)
    gibi_leaved = models.PositiveSmallIntegerField("Nombre total de feuillus", null=True, blank=True)
    gibi_leaved_f = models.PositiveSmallIntegerField("Proportion de feuillus frotté", null=True, blank=True)
    gibi_leaved_a = models.PositiveSmallIntegerField("Proportion de feuillus abroutis", null=True, blank=True)
    remarks = models.TextField("Remarques", blank=True)

    class Meta:
        db_table = 'plot_obs'
        verbose_name = "Observation sur placette"
        verbose_name_plural = "Observations sur placette"

    def __str__(self):
        return "Plot obs. on %s (%s)" % (self.plot, self.year)

    @property
    def division(self):
        return str(self.div_num).zfill(2) if self.div_num is not None else None

    def detail_url(self):
        return reverse('plotobs_detail', args=[self.pk])

    @property
    def ame_record(self):
        from .db_views import AME_CE
        if self.uid:
            try:
                return AME_CE.objects.get(inv_GuidInv=self.uid)
            except AME_CE.DoesNotExist:
                return None
        else:
            # In older .cor files, there is no uid, then try matching with coords and year
            try:
                ame = AME_CE.objects.get(
                    inv_InvAnnee=self.imp_file.year,
                    CoordX=int(self.plot.center.x + 2000000), CoordY=int(self.plot.center.y + 1000000)
                )
            except AME_CE.DoesNotExist:
                return None
            else:
                self.uid = ame.inv_GuidInv
                self.save()
            return ame

    @property
    def year(self):
        return self.imp_file.year if self.imp_file else self.protocol.year

    @property
    def surface(self):
        return self.radius * self.radius * math.pi * math.cos(self.slope * (math.pi / 180))

    @cached_property
    def ddom(self):
        diams = sorted([tree.diameter for tree in self.tree_set.all()], reverse=True)
        if not diams:
            return 0
        fact = round_up(self.surface / 100)  # / (self.density.surface // 100))
        return round_up(sum(diams[:fact]) / min(fact, len(diams)))

    @property
    def ddom_class(self):
        ddom = self.ddom
        if ddom == 0:
            return 1
        return min(6, (ddom // 10) + 1)

    def as_geojson(self, srid=None, verbose=False):
        """
        Return a FeatureCollection GeoJSON with the plot as first feature and
        the PlotObs trees as following features.
        """
        def serialize(value):
            if isinstance(value, models.Model):
                return [value.pk, str(value)]
            return str(value)

        geojson = {"type": "FeatureCollection", "features": [], "plot": self.plot.pk}
        geom = self.plot.center
        if (srid is not None and srid != geom.srid):
            geom = geom.transform(srid, clone=True)
        geojson['features'].append({
            "type": "Feature",
            "id": self.pk,
            "geometry": eval(geom.json),
            "properties": {"type": "center", "year": self.year},
        })
        geojson['features'][-1]["properties"].update({
            field.verbose_name if verbose else field.name: serialize(getattr(self, field.name))
            for field in self._meta.fields if field.name not in ['plot', 'protocol', 'imp_file']
        })
        # Add trees
        for treeobs in self.tree_set.select_related('spec').all():
            geojson['features'].append({
                "type": "Feature",
                "id": treeobs.pk,
                "properties": {
                    "type": "tree",
                    "diameter": treeobs.diameter,
                    "quality": treeobs.quality,
                    "spec": serialize(treeobs.spec),
                },
            })
        return geojson

    def as_fbase(self):
        """
        Ex: 2U    406843304 15406080  010 7    1 100   7 19  359  11 7 2 22 3            000000401   382549 57 28   5857 16   15
        """
        # When computing a long list of fbase strings, optimize query with
        # select_related and prefetch_related. See EntityFbaseExport view.
        trees = [tree.as_fbase() for tree in self.tree_set.order_by('pk')]
        x, y = map(int, self.plot.center.coords)
        protocol_intervs = {
            val: key for key, val in {
                idx: getattr(self.protocol, f'interv_{idx}_id') for idx in range(0, 10)
            }.items() if val is not None
        }
        if self.interv_imp:
            interv_pos = self.interv_imp
        elif self.interv_int_id:
            interv_pos = protocol_intervs[self.interv_int_id]
        else:
            interv_pos = ''

        def div_by_10(val):
            return '%X' % (val // 10)  # Convert to hexa

        fbase = (
            '{nb_t:>5}U    {type_prop}{no_prop:>3}{ser}{div:0>2}{res1}{res2} {coordx}{coordy}  '
            '{nat}{nat_perc:>2} {interv:>1}    1{surf_repr:>4}  {annee:>2} {pente:0>2}{surface:>5}'
            '  {rayon:0>2}{non_denombr:>2}{conifer:>2}{ddom:>3}{ddom_class:>2}            '
            '{gb_epi_tot}{gb_epi_fr}{gb_epi_ab}{gb_res_tot}{gb_res_fr}{gb_res_ab}'
            '{gb_feu_tot}{gb_feu_fr}{gb_feu_ab}{ident:>9} '
        ).format(
            nb_t=len(trees),
            type_prop=self.owner.typ.num_code,
            no_prop=self.owner.num,
            ser=self.ser_num,
            div=self.div_num,
            res1=self.reserved1 or ' ', res2=self.reserved2 or ' ',
            coordx=str(x)[1:5], coordy=str(y)[1:5],
            nat=self.main_nature.code, nat_perc=self.main_perc // 10,
            interv=interv_pos,
            # Après intervention, les anciens prot. avaient urgence interv et prescriptions
            # Le type d'inventaire est toujours à 1 depuis ?
            surf_repr=self.density.surface // 100,
            annee=int(str(self.year)[-2:]),
            pente=self.slope,
            surface=round(self.surface),
            rayon=self.radius,
            non_denombr=self.unnumbered // 10,
            conifer=self.conifer // 10,
            ddom=self.ddom,
            ddom_class=self.ddom_class,
            gb_epi_tot=div_by_10(self.gibi_epicea), gb_epi_fr=div_by_10(self.gibi_epicea_f),
            gb_epi_ab=div_by_10(self.gibi_epicea_a), gb_res_tot=div_by_10(self.gibi_conifer),
            gb_res_fr=div_by_10(self.gibi_conifer_f), gb_res_ab=div_by_10(self.gibi_conifer_a),
            gb_feu_tot=div_by_10(self.gibi_leaved), gb_feu_fr=div_by_10(self.gibi_leaved_f),
            gb_feu_ab=div_by_10(self.gibi_leaved_a),
            ident=self.uid or '',
        )
        return fbase + ''.join(trees)

    def check_fbase(self):
        """Check our generated FBase string with the one from the ame_ce table."""
        record = self.ame_record
        if record is None:
            return "Unable to find the AME record for this obs."
        our_fbase = self.as_fbase()
        if record.inv_Fbase == our_fbase:
            return "The FBase strings are identical"
        else:
            return diff_lines(record.inv_Fbase, our_fbase)


class TreeSpecies(models.Model):
    TYP_CHOICES = (
        ('f', 'Feuillu'),
        ('r', 'Résineux'),
    )
    species = models.CharField("Nom", max_length=100)
    # Mainly for importing legacy data (code is unique)
    code = models.CharField(max_length=1, blank=True,
        help_text="Laissez ce champ vide si la position est E ou F")
    # For exporting, several species can share the same code
    code_fbase = models.CharField(max_length=1, blank=True)
    abbrev = models.CharField("Abréviation", max_length=4, blank=True)
    order = models.PositiveSmallIntegerField(blank=True, null=True)
    typ = models.CharField("Type", max_length=1, choices=TYP_CHOICES, blank=True)
    active = models.BooleanField(
        "Actif?", default=False,
        help_text="Indique si l’essence peut être saisie dans de nouveaux inventaires"
    )

    class Meta:
        db_table = 'tree_spec'
        verbose_name = 'essence'
        ordering = ('species',)
        constraints = [
            models.UniqueConstraint(
                fields=["code"],
                name="code_unique",
                condition=~models.Q(code='')
            ),
        ]

    def __str__(self):
        return self.species

    def validate_unique(self, **kwargs):
        if self.id is None and self.code and re.match(r'^[0-9A-D]$', self.code):
            # Check that 0-9A-D codes are unique
            if TreeSpecies.objects.filter(code=self.code).exists():
                raise ValidationError({'__all__': ("The code '%s' already exists" % self.code,)})


QUALITY_CHOICES = (
    ('Q0', 'Q0'),
    ('Q1', 'Q1'),
    ('Q2', 'Q2'),
    ('Q3', 'Q3'),
    ('Q4', 'Q4'),
    ('Q5', 'Q5'),
    ('Q6', 'Q6'),
    ('Qn', 'Qn'),
    ('Q-', 'Q-'),
    ('Q+', 'Q+'),
)

class Tree(models.Model):
    obs = models.ForeignKey(PlotObs, on_delete=models.CASCADE)
    spec = models.ForeignKey(TreeSpecies, on_delete=models.PROTECT, verbose_name="Essence")
    diameter = models.SmallIntegerField("Diamètre")
    quality = models.CharField("Qualité", max_length=2, choices=QUALITY_CHOICES, blank=True)

    class Meta:
        db_table = 'tree'
        verbose_name = "arbre"

    @property
    def quality_code(self):
        if not self.quality:
            return None
        try:
            return int(self.quality[1])
        except ValueError:
            return {'Qn': 7, 'Q-': 8, 'Q+': 9}.get(self.quality)

    @property
    def volume(self):
        return TARIF_VOLUME_VD.get(self.diameter, 0)

    def as_fbase(self):
        code = self.spec.code or (
            'E' if self.obs.protocol.spec_posE_id == self.spec_id else (
                'F' if self.obs.protocol.spec_posF_id == self.spec_id else ' '
            )
        )
        return '{ess}{qual}{diam:>3}{vol:>5}'.format(
            ess=code, qual=self.quality_code, diam=self.diameter, vol=int(self.volume)
        )


class Topap(models.Model):
    """Copy of the Topap data (from Service des forêts)"""
    polygon = gis_models.MultiPolygonField(srid=2056)
    guidpa = models.CharField(max_length=32, db_column='top_GuidPa')
    fk_guid = models.CharField(max_length=32, db_column='top_FkGuid')
    lad_fkterr = models.IntegerField(db_column='lad_FkTerr')
    lad_fkardt = models.IntegerField(db_column='lad_FkArdt')
    lad_fkcirc = models.IntegerField(db_column='lad_FkCirc')
    lad_fkci1 = models.IntegerField(db_column='lad_FkCi_1')
    lad_fkdist = models.IntegerField(db_column='lad_FkDist')
    lieu_dit = models.CharField(max_length=110, db_column='top_LieuDi')
    triage_ge = models.CharField(max_length=50, db_column='LvTriageGe')
    propri = models.CharField(max_length=5, db_column='LvPropri')
    remarques = models.CharField(max_length=254, blank=True, db_column='top_Remarq')
    rem_con = models.CharField(max_length=254, blank=True, db_column='top_RemCon')
    resp_maj = models.CharField(max_length=4, db_column='LvRespMaj')
    serie = models.CharField(max_length=1, db_column='top_Serie')
    division = models.CharField(max_length=2, db_column='top_Div')
    serie_div = models.CharField(max_length=4, db_column='top_SerieD')
    lv_stat = models.IntegerField(db_column='top_LvStat')
    code = models.CharField(max_length=6, db_column='top_Code')
    date_ma = models.DateField(db_column='top_DateMa', blank=True, null=True)
    date_1 = models.FloatField(db_column='top_Date_1')
    date_va = models.DateField(db_column='top_DateVa', blank=True, null=True)
    coordx = models.FloatField(db_column='CoordX')
    coordy = models.FloatField(db_column='CoordY')
    auteur_crea = models.CharField(max_length=50, db_column='AuteurCrea')
    auteur_modif = models.CharField(max_length=50, db_column='AuteurModi')
    base_source = models.CharField(max_length=254, db_column='BaseSource')
    date_crea = models.DateField(db_column='DateCreati')
    date_modif = models.DateField(db_column='DateModif')
    machine_crea = models.CharField(max_length=50, db_column='MachineCre')
    machine_mod = models.CharField(max_length=50, db_column='MachineMod')
    resp_fiche = models.CharField(max_length=50, db_column='RespFiche')
    resp_prec_fi = models.CharField(max_length=50, db_column='RespPrecFi')
    table_source = models.CharField(max_length=50, db_column='TableSourc')
    shape_len = models.FloatField(db_column='SHAPE_Leng')
    shape_area = models.FloatField(db_column='SHAPE_Area')


def round_up(num):
    """Python rounds 0.5 to the nearest even value (2.5 -> 2, 3.5 -> 4)."""
    return int(Decimal(num).to_integral_value(rounding=ROUND_HALF_UP))
