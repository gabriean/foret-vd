import csv
import json
from collections import OrderedDict, namedtuple
from datetime import date
from decimal import Decimal
from itertools import chain

from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.gis.db.models.aggregates import Collect
from django.contrib.gis.db.models.functions import GeomOutputGeoFunc
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import D
from django.db import connection
from django.db.models import Count, Max, Model, Prefetch, Sum
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views.generic import (
    CreateView, DeleteView, DetailView, FormView, ListView, TemplateView,
    UpdateView, View
)

from document.models import Document
from municipality.models import Arrondissement, Municipality
from .forms import (
    FBaseForm, FutureProtocolForm, ImportPlotsForm, PassedProtocolForm,
    PlotObsPlannedForm, TopapForm,
)
from .models import (
    Owner, OwnerType, Plot, PlotObs, PlotObsPlanned, Protocol, Topap, Tree,
    TreeSpecies, User, Vocabulary, WebHomepageMunicipality, WebHomepageOwner,
)
from .queries import QUERIES, Query


class HomeView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        view_by = self.request.GET.get('by', 'owner')
        if view_by == 'owner':
            base_view = WebHomepageOwner.objects.all(
                ).select_related('owner', 'protocol'
                ).order_by('owner__typ_id', 'owner__num', 'protocol__year')
        else:
            base_view = WebHomepageMunicipality.objects.all(
                ).select_related('municipality', 'protocol'
                ).order_by('municipality__name', 'protocol__year')
        context.update({
            'cur_year': date.today().year,
            'inventaires': Protocol.objects.filter(year=date.today().year),
            'view_by': view_by,
            'data': base_view,
            'metadata': base_view.model._meta,
            'col1_name': "Propriétaire" if view_by == 'owner' else "Commune",
            'col1_descr': "Propriétaire forestier" if view_by == 'owner' else "Commune territoriale",
        })
        if view_by == 'owner':
            context['entities'] = OwnerType.objects.all().order_by('id')
        return context


class HomeEntityData(TemplateView):
    template_name = 'partial/index_table.html'

    def get_context_data(self, **kwargs):
        owner_type = get_object_or_404(OwnerType, pk=self.kwargs['pk'])
        data = WebHomepageOwner.objects.filter(owner__typ=owner_type).all(
                ).select_related('owner', 'protocol'
                ).order_by('owner__num', 'protocol__year')
        return {**super().get_context_data(**kwargs), 'data': data}


class EntityMixin:
    def get_entity(self):
        if self.kwargs['name'] == 'owner':
            return get_object_or_404(Owner, pk=self.kwargs['pk'])
        else:
            return get_object_or_404(Municipality, pk=self.kwargs['pk'])


class ConvexHull(GeomOutputGeoFunc):
    arity = 1


class EntityView(EntityMixin, TemplateView):
    """Entity is either an owner or a municipality."""
    template_name = 'entity.html'

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        entity = self.get_entity()
        if self.kwargs['name'] == 'owner':
            municipality = None
            if entity.typ.name == 'Commune':
                try:
                    municipality = Municipality.objects.get(lad_nocanc=entity.num)
                except Municipality.DoesNotExist:
                    pass
                except Municipality.MultipleObjectsReturned:
                    municipality = Municipality.objects.get(lad_nocanc=entity.num, fusion_dans__isnull=True)
            plot_obs_base = entity.plotobs_set
            planned_count = entity.plotobsplanned_set.count()
            title = "Propriétaire : %s (%s)" % (entity.name, entity.code)
            entity_geom = entity.plotobs_set.aggregate(geom=ConvexHull(Collect('plot__center')))['geom']
            if entity_geom:
                center = entity_geom.centroid
            else:
                center = Municipality.objects.get(name='Assens').geom.centroid
        else:
            municipality = entity
            plot_obs_base = PlotObs.objects.filter(plot__municipality=municipality)
            planned_count = PlotObsPlanned.objects.filter(plot__municipality=municipality).count()
            title = "Commune : %s" % municipality.name
            entity_geom = municipality.geom
            center = municipality.geom.centroid

        if entity_geom:
            entity_geom.transform(4326)

        context.update({
            'entity': entity,
            'entity_type': self.kwargs['name'],
            'title': title,
            'mun_geom': entity_geom,
            'center': center,
            'plot_obs_by_year': plot_obs_base.values('protocol__year').annotate(Count('id')
                        ).order_by('protocol__year'),
            'plot_obs_url': reverse('plotobs', args=[self.kwargs['name'], entity.pk]),
            'planned_count': planned_count,
        })
        return context


class EntityFbaseExport(EntityMixin, View):
    def get(self, request, *args, **kwargs):
        entity = self.get_entity()
        if kwargs['name'] == 'owner':
            plotobs = PlotObs.objects.filter(
                protocol__year=kwargs['year'], owner_id=kwargs['pk']
            )
        else:
            plotobs = PlotObs.objects.filter(
                protocol__year=kwargs['year'], plot__municipality__id=kwargs['pk']
            )
        response = HttpResponse(content_type='text/plain')
        response['Content-Disposition'] = 'attachment; filename="{}-{}.txt"'.format(
            str(entity), kwargs['year']
        )

        plotobs = plotobs.select_related(
            'plot', 'owner__typ', 'main_nature', 'interv_int', 'protocol',
        ).prefetch_related(
            Prefetch('tree_set', queryset=Tree.objects.select_related('spec'))
        )
        for po in plotobs:
            response.write(po.as_fbase() + "\r\n")
        return response


class OwnerAnalysisView(TemplateView):
    template_name = 'owner_analysis.html'

    def get_context_data(self, **kwargs):
        FEUILLU_DIVERS = 'feuillus divers (tous les feuillus qui ne sont pas spécifiés dans cette liste)'
        RESINEUX_DIVERS = 'résineux divers (if, séquoia, thuya, etc.)'
        species = OrderedDict([
            ('épicéa', 'épicéa'), ('sapin', 'sapin'), ('mélèze', 'mélèze'),
            ('douglas', 'dougla'), ('pins, tous les pins', 'pins'),
            (RESINEUX_DIVERS, 'R-div'),
            ('hêtre', 'hêtre'), ('érable sycomore, plane et obier', 'érable'),
            ('chêne pédonculé et sessile', 'chêne'), ('frêne', 'frêne'),
            ('merisier', 'meris'), ("peuplier, tremble, d'Italie, etc.", 'peupl'),
            (FEUILLU_DIVERS, 'F-div'),
        ])
        db_species = {ts.species: ts for ts in TreeSpecies.objects.all()}
        diam_classes = [
            'N8-15', 'N16-19', 'N20-23', 'N24-27', 'N28-31', 'N32-35', 'N36-39',
            'N40-43', 'N44-47', 'N48-51', 'N52-55', 'N56-59', 'N60-63', 'N64-67',
            'N68-71', 'N72-75', 'N76-79', 'N80+'
        ]

        def size_group(diam_class):
            # Petit bois : < 28 cm
            # Bois moyen : >=28 cm et < 48 cm
            # Gros bois : >=48 cm
            if diam_class == 'N8-15':
                return
            if diam_class.endswith(('19', '23', '27')):
                return 'N-peti'
            elif diam_class.endswith(('31', '35', '39', '43', '47')):
                return 'N-moye'
            return 'N-gros'

        def norm_spec_name(name):
            if name not in species.keys():
                return RESINEUX_DIVERS if db_species[name].typ == 'r' else FEUILLU_DIVERS
            return name

        def is_resineux(name):
            return list(species.keys()).index(name) <= 5

        def calc_perc(part, tot):
            if tot == 0:
                return 0
            return round(part / tot * 100)

        zero = Decimal('0.0')
        empty_line = {
            **{sp: zero for sp in species.keys()},
            'R/HA': zero, 'F/HA': zero, 'T/HA': zero,
            'R-ABS': 0, 'F-ABS': 0, 'T-ABS': 0,
        }
        d_classes = {
            d_name: empty_line.copy() for d_name in diam_classes + [
                'N-peti', 'N-moye', 'N-gros', 'N-T/HA', 'N-Tabs', 'N-%',
                'V-peti', 'V-moye', 'V-gros', 'V-T/HA', 'V-Tabs', 'V-%',
            ]
        }
        owner = get_object_or_404(Owner, pk=self.kwargs['pk'])
        plot_obs = owner.plotobs_set.filter(imp_file__year=self.kwargs['year'])
        surface = plot_obs.aggregate(Sum('density__surface'))['density__surface__sum'] // 10000
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM ksp_calc_num_tiges(%s, %s, %s);", (owner.pk, self.kwargs['year'], surface))
            for spec_id, spec_name, diam_class, sum_abs, sum_ha, sum_vol, sum_vol_ha in cursor.fetchall():
                if spec_id is None:
                    continue
                spec_name = norm_spec_name(spec_name)
                for line_key in (diam_class, size_group(diam_class)):
                    if line_key is None:
                        continue
                    d_classes[line_key][spec_name] += sum_ha
                    d_classes[line_key]['T/HA'] += sum_ha
                    d_classes[line_key]['T-ABS'] += sum_abs
                    if is_resineux(spec_name):
                        d_classes[line_key]['R/HA'] += sum_ha
                        d_classes[line_key]['R-ABS'] += sum_abs
                    else:
                        d_classes[line_key]['F/HA'] += sum_ha
                        d_classes[line_key]['F-ABS'] += sum_abs
                if diam_class != 'N8-15':
                    d_classes['N-T/HA'][spec_name] += sum_ha
                    d_classes['N-T/HA']['T/HA'] += sum_ha
                    d_classes['N-Tabs'][spec_name] += sum_abs
                    d_classes['N-Tabs']['T/HA'] += sum_abs
                    d_classes['V-T/HA'][spec_name] += sum_vol_ha
                    d_classes['V-T/HA']['T/HA'] += sum_vol_ha
                    d_classes['V-Tabs'][spec_name] += sum_vol
                    d_classes['V-Tabs']['T/HA'] += sum_vol
                    v_size_group = size_group(diam_class).replace('N', 'V')
                    d_classes[v_size_group][spec_name] += sum_vol_ha
                    d_classes[v_size_group]['T-ABS'] += sum_vol
                    d_classes[v_size_group]['T/HA'] += sum_vol_ha
                    if is_resineux(spec_name):
                        d_classes[v_size_group]['R/HA'] += sum_vol_ha
                        d_classes[v_size_group]['R-ABS'] += sum_vol
                        d_classes['N-T/HA']['R/HA'] += sum_ha
                        d_classes['N-Tabs']['R/HA'] += sum_abs
                        d_classes['V-T/HA']['R/HA'] += sum_vol_ha
                        d_classes['V-Tabs']['R/HA'] += sum_vol
                    else:
                        d_classes[v_size_group]['F/HA'] += sum_vol_ha
                        d_classes[v_size_group]['F-ABS'] += sum_vol
                        d_classes['N-T/HA']['F/HA'] += sum_ha
                        d_classes['N-Tabs']['F/HA'] += sum_abs
                        d_classes['V-T/HA']['F/HA'] += sum_vol_ha
                        d_classes['V-Tabs']['F/HA'] += sum_vol
        # Calculate N-% and V-% percentages
        for key in d_classes['N-Tabs']:
            if key in ['R-ABS', 'F-ABS', 'T-ABS']:
                d_classes['N-Tabs'][key] = d_classes['N-%'][key] = ''
            else:
                d_classes['N-Tabs'][key] = round(d_classes['N-Tabs'][key])
                d_classes['N-%'][key] = calc_perc(d_classes['N-Tabs'][key], int(d_classes['N-Tabs']['T/HA']))
        for key in d_classes['V-Tabs']:
            if key in ['R-ABS', 'F-ABS', 'T-ABS']:
                d_classes['V-Tabs'][key] = d_classes['V-%'][key] = ''
            else:
                d_classes['V-Tabs'][key] = int(d_classes['V-Tabs'][key])
                d_classes['V-%'][key] = calc_perc(d_classes['V-Tabs'][key], int(d_classes['V-Tabs']['T/HA']))
        # Calculate %R/%F/%T
        for key in d_classes:
            if d_classes[key]['T-ABS']:
                if key == 'N8-15':
                    d_classes[key]['%R'] = calc_perc(d_classes[key]['R-ABS'], d_classes[key]['T-ABS'])
                    d_classes[key]['%F'] = calc_perc(d_classes[key]['F-ABS'], d_classes[key]['T-ABS'])
                    d_classes[key]['%T'] = 100
                else:
                    if key.startswith('N'):
                        d_classes[key]['%R'] = calc_perc(d_classes[key]['R-ABS'], d_classes['N-Tabs']['R/HA'])
                        d_classes[key]['%F'] = calc_perc(d_classes[key]['F-ABS'], d_classes['N-Tabs']['F/HA'])
                        d_classes[key]['%T'] = calc_perc(d_classes[key]['T-ABS'], d_classes['N-Tabs']['T/HA'])
                    else:
                        d_classes[key]['%R'] = calc_perc(d_classes[key]['R-ABS'], d_classes['V-Tabs']['R/HA'])
                        d_classes[key]['%F'] = calc_perc(d_classes[key]['F-ABS'], d_classes['V-Tabs']['F/HA'])
                        d_classes[key]['%T'] = calc_perc(d_classes[key]['T-ABS'], d_classes['V-Tabs']['T/HA'])
                d_classes[key]['R-ABS'] = round(d_classes[key]['R-ABS'])
                d_classes[key]['F-ABS'] = round(d_classes[key]['F-ABS'])
                d_classes[key]['T-ABS'] = round(d_classes[key]['T-ABS'])
            else:
                d_classes[key]['%R'] = d_classes[key]['%F'] = ''

        d_classes['G>=10'] = {
            **empty_line, 'R/HA': 0.0, 'F/HA': 0.0, 'T/HA': 0.0, 'R-ABS': '', 'F-ABS': '', 'T-ABS': '',
        }
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM ksp_calc_surf_terr(%s, %s);", (owner.pk, self.kwargs['year']))
            for spec_id, spec_name, surf_terr in cursor.fetchall():
                spec_name = norm_spec_name(spec_name)
                d_classes['G>=10'][spec_name] = surf_terr
                if is_resineux(spec_name):
                    d_classes['G>=10']['R/HA'] += surf_terr
                else:
                    d_classes['G>=10']['F/HA'] += surf_terr
                d_classes['G>=10']['T/HA'] += surf_terr

        return {
            **super().get_context_data(**kwargs),
            'annee': self.kwargs['year'],
            'surface': surface,
            'placettes': plot_obs.count(),
            'lines': d_classes, 'species': species,
            'sep_before': ['N16-19', 'N-peti', 'N-T/HA', 'V-peti', 'V-T/HA', 'G>=10'],
            'owner': owner,
        }


class PlotObsDataView(View):
    """
    Return a GeoJSON structure containing all PlotObs objects for either an owner
    or a municipality and a year.
    """
    def get(self, request, *args, **kwargs):
        year = request.GET.get('year')
        if year == 'planned':
            plotobs = PlotObsPlanned.objects.select_related('plot'
                ).filter(owner_id=kwargs['pk'])
        elif kwargs['name'] == 'owner':
            plotobs = PlotObs.objects.select_related('plot'
                ).filter(protocol__year=year, owner_id=kwargs['pk'])
        else:
            plotobs = PlotObs.objects.select_related('plot'
                ).filter(protocol__year=year, plot__municipality__id=kwargs['pk'])
        struct = {"type": "FeatureCollection", "features": []}
        for obs in plotobs:
            struct['features'].append({
                "type": "Feature",
                "geometry": eval(obs.plot.center.transform(4326, clone=True).json),
                "properties": {
                    "pk": obs.pk,
                    "url": reverse('plotobs_planned_detail' if year == 'planned' else 'plotobs_detail', args=[obs.pk])},
            })
        return JsonResponse(struct)


class PlotObsDetailJSONView(View):
    def get(self, request, *args, **kwargs):
        plotobs = get_object_or_404(PlotObs, pk=kwargs['pk'])
        srid = self.request.GET.get('srid')
        return JsonResponse(plotobs.as_geojson(srid=srid))


class PlotObsDetailView(TemplateView):
    output = 'json'
    model = PlotObs

    def get_template_names(self):
        return 'plotobs_detail_core.html' if 'X-Partial' in self.request.headers else 'plotobs_detail.html'

    def get_context_data(self, **context):
        self.plotobs = get_object_or_404(self.model.objects.select_related('plot'), pk=self.kwargs['pk'])
        srid = self.request.GET.get('srid')
        center_geom = self.plotobs.plot.center.clone()
        if srid is not None:
            center_geom.transform(srid)
        return {
            'geojson': self.plotobs.as_geojson(srid=4326, verbose=True),
            'center': center_geom,
            'plotobs': self.plotobs,
            'hidden_props': ('id', 'type', 'uid'),
        }

    def render_to_response(self, context, **kwargs):
        if self.output == 'json':
            return JsonResponse(context['geojson'])
        else:
            plot = self.plotobs.plot
            props = context['geojson']['features'][0]["properties"].copy()
            props.update({
                'Altitude': plot.sealevel,
            })
            context.update({
                'properties': props,
                'geojson': context['geojson'],
                'gemeinde': self.plotobs.plot.municipality,
                'treeobs': self.plotobs.tree_set.select_related('spec').all(),
                'siblings': self.plotobs.plot.plotobs_set.exclude(pk=self.plotobs.pk),
            })
            return super().render_to_response(context, **kwargs)


class PlotObsPlannedDetailView(DetailView):
    model = PlotObsPlanned
    template_name = 'partial/plotobs_planned.html'

    def get_context_data(self, **context):
        return {
            **super().get_context_data(**context),
            'history': self.object.plot.plotobs_set.all().order_by('protocol__year'),
            'topap': Topap.objects.filter(polygon__contains=self.object.plot.center).first(),
        }


class PlotObsPlannedEditView(PermissionRequiredMixin, UpdateView):
    model = PlotObsPlanned
    form_class = PlotObsPlannedForm
    template_name = 'partial/plotobs_planned_form.html'
    permission_required = 'inventory.change_plotobsplanned'

    def get_success_url(self):
        return reverse('plotobs_planned_detail', args=[self.object.pk])


class PlotObsPlannedMultiEditView(PermissionRequiredMixin, View):
    permission_required = 'inventory.change_plotobsplanned'

    def post(self, request, *args, **kwargs):
        protocol = get_object_or_404(Protocol, pk=self.kwargs['prot_pk'])
        subm_form = PlotObsPlannedForm(protocol=protocol, data=request.POST)
        point_ids = request.POST['point_ids'].split(',')
        points = PlotObsPlanned.objects.in_bulk(point_ids).values()
        if subm_form.is_valid():
            for field, value in subm_form.cleaned_data.items():
                if value is not None:
                    for pt in points:
                        setattr(pt, field, value)
                        pt.save()
            messages.success(request, 'Les points ont été modifiés avec succès')
        return render(request, 'partial/plotobs_planned_form_multi.html', {
            'protocol': protocol, 'empty_form': PlotObsPlannedForm(protocol=protocol),
        })


class PlotObsPlannedNewView(PermissionRequiredMixin, View):
    permission_required = 'inventory.add_plotobsplanned'

    def post(self, request, *args, **kwargs):
        protocol = get_object_or_404(Protocol, pk=self.kwargs['prot_pk'])
        center = Point(int(request.POST.get('latitude')), int(request.POST.get('longitude')), srid=2056)
        try:
            plot = Plot.objects.get(center=center)
        except Plot.DoesNotExist:
            plot = Plot.objects.create(
                center=center, municipality=Municipality.get_from_point(center)
            )
        planned = PlotObsPlanned.objects.create(plot=plot, protocol=protocol)
        return JsonResponse({
            'result': 'OK',
            'feature': {
                "type": "Feature",
                "id": planned.pk,
                "properties": {
                    "id": planned.pk,
                    "url": planned.detail_url(),
                    "owner": None,
                    "serie": None,
                    "division": None,
                    "operator": None,
                },
                "geometry": eval(planned.plot.center.transform(4326, clone=True).json),
            }
        }, safe=False)


class PlotObsPlannedDeleteView(PermissionRequiredMixin, DeleteView):
    model = PlotObsPlanned
    permission_required = 'inventory.delete_plotobsplanned'

    def delete(self, *args, **kwargs):
        self.get_object().delete()
        return JsonResponse({'result': 'OK'}, safe=False)


class PermissiveEncoder(json.JSONEncoder):
    """
    Custom encoder to simply return 'null' for unserializable objects.
    """
    def default(self, obj):
        if isinstance(obj, type) and issubclass(obj, Model):
            return dict([(f.name, f.verbose_name) for f in obj._meta.fields])
        try:
            return super().default(obj)
        except TypeError:
            return None


class DataPageView(TemplateView):
    template_name = 'data_page.html'

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        context.update({
            'municipalities': Municipality.objects.all().order_by('name'),
            'owners': Owner.objects.all().order_by('typ__alpha_code', 'num'),
            'arronds': Arrondissement.objects.all().order_by('no'),
            'views': json.dumps({k: v.as_dict() for k, v in QUERIES.items()}, cls=PermissiveEncoder),
        })
        return context


class DataGridView(TemplateView):
    template_name = 'data_grid.html'

    def get_context_data(self, **context):
        context = super().get_context_data(**context)

        analyze = self.request.GET.get('bio')
        if not analyze:
            context['error'] = "Erreur: vous devez choisir une donnée d'analyse"
            return context
        elif analyze not in QUERIES:
            context['error'] = "Erreur: l'analyse '%s' n'est pas connue du système" % analyze
            return context
        self.query = QUERIES[analyze]

        # Compute data after request.GET
        aggrs = self.request.GET.getlist('yaggr') + self.request.GET.getlist('aggr')
        perims = {
            'municips': [int(v) for v in self.request.GET.getlist('mun')],
            'owners': [int(v) for v in self.request.GET.getlist('own')],
            'arrs': [int(v) for v in self.request.GET.getlist('arr')],
        }
        if not any(perims.values()):
            context['error'] = "Erreur: Vous devez sélectionner au moins une commune, propriétaire ou arrondissement"
            return context
        context.update(self.query.build_query(perims, aggrs))
        return context

    def render_to_response(self, context, **response_kwargs):
        if self.request.GET.get('format') == 'csv' and 'error' not in context:
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename="vd-data-export.csv"'
            writer = csv.writer(response, delimiter=";")
            writer.writerow(context['field_names'])
            for line in context['query']:
                writer.writerow([to_csv(val) for val in line])
            return response
        return super().render_to_response(context, **response_kwargs)


class ProtocolesView(ListView):
    model = Protocol
    template_name = 'protocoles.html'

    def get_queryset(self):
        qs = super().get_queryset(
            ).annotate(Count('plotobs')
            ).prefetch_related(Prefetch('owners',
                queryset=Owner.objects.select_related('typ').order_by('typ__alpha_code', 'num'))
            ).order_by('-year', 'pfile')
        if self.request.GET.get('draft'):
            qs = qs.filter(draft=True)
        return qs


class ProtocolesByOwnerView(ListView):
    template_name = 'protocoles_by_owner.html'

    def get_queryset(self):
        prot_qs = Protocol.objects.annotate(Count('plotobs')).order_by('-year')
        if self.request.GET.get('draft'):
            prot_qs = prot_qs.filter(draft=True)
        return Owner.objects.all().prefetch_related(Prefetch('protocol_set', queryset=prot_qs)
            ).order_by('typ', 'num')


class ProtocoleDetailView(DetailView):
    model = Protocol
    template_name = 'protocole.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Count plots by owner
        owners1 = {
            res['owner']: res['id__count']
            for res in self.object.plotobs_set.values('owner').annotate(Count('id'))
        }
        owners2 = {
            res['owner']: res['id__count']
            for res in self.object.plotobsplanned_set.values('owner').annotate(Count('id'))
        }
        context.update({
            'owners': {
                owner: owners1.get(owner.pk, 0) + owners2.get(owner.pk, 0)
                for owner in Owner.objects.filter(
                    pk__in=list(set(owners1.keys()) | set(owners2.keys()))
                ).order_by('typ', 'num')
            },
            'owners_none': self.object.plotobsplanned_set.filter(owner__isnull=True).count(),
            'operators': User.objects.all(),
            'empty_form': PlotObsPlannedForm(protocol=self.object),
        })
        return context


class ProtocoleCreateView(CreateView):
    model = Protocol
    template_name = 'protocole_edit.html'
    typ = None

    @property
    def form_class(self):
        return PassedProtocolForm if self.typ == 'passed' else FutureProtocolForm

    def get_template_names(self):
        return ['protocole_edit.html'] if self.typ == 'passed' else ['protocole_new.html']

    def get_success_url(self):
        return reverse('protocole', args=[self.object.pk])


class ProtocoleUpdateView(UpdateView):
    model = Protocol
    template_name = 'protocole_edit.html'

    @property
    def form_class(self):
        return PassedProtocolForm if self.object.passed else FutureProtocolForm

    def get_context_data(self, **kwargs):
        if self.object.geom:
            plots = Plot.objects.filter(center__dwithin=(self.object.geom, D(m=200)))
        else:
            plots = Plot.objects.filter(plotobs__protocol=self.object)
        return {
            **super().get_context_data(**kwargs),
            'plots': json.dumps(plots.as_geojson()),
        }

    def get_success_url(self):
        return reverse('protocoles')


class ProtocolePlotsView(View):
    def get(self, *args, **kwargs):
        inventory = get_object_or_404(Protocol, pk=kwargs['pk'])
        geojson = {"type": "FeatureCollection", "features": []}
        for plotobs in chain(
                inventory.plotobs_set.all().select_related('plot', 'owner', 'owner__typ'),
                inventory.plotobsplanned_set.all().select_related('plot', 'owner', 'owner__typ')):
            operator = getattr(plotobs, 'operator', None)
            geojson["features"].append({
                "type": "Feature",
                "id": plotobs.pk,
                "properties": {
                    "id": plotobs.pk,
                    "url": plotobs.detail_url(),
                    "owner": plotobs.owner.code if plotobs.owner_id else '',
                    "serie": plotobs.ser_num,
                    "division": plotobs.division,
                    "operator": operator.pk if operator else None,
                },
                "geometry": eval(plotobs.plot.center.transform(4326, clone=True).json),
            })
        return JsonResponse(geojson)


class TopapGeoJSONView(View):
    def get(self, *args, **kwargs):
        inventory = get_object_or_404(Protocol, pk=kwargs['pk'])
        geojson = {"type": "FeatureCollection", "features": []}
        for item in Topap.objects.filter(polygon__intersects=inventory.envelope()):
            geojson["features"].append({
                "type": "Feature",
                "id": item.pk,
                "properties": {
                    "id": item.pk,
                    "owner": item.propri,
                    "serie": item.serie,
                    "division": item.division,
                    "triage": item.triage_ge,
                },
                "geometry": eval(item.polygon.transform(4326, clone=True).json),
            })
        return JsonResponse(geojson)


class ProtocoleImportPlotsView(FormView):
    template_name = 'protocole_import.html'
    form_class = ImportPlotsForm

    def dispatch(self, request, *args, **kwargs):
        self.protocol = get_object_or_404(Protocol, pk=kwargs['pk'])
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **context):
        return {
            **super().get_context_data(**context),
            'protocol': self.protocol,
        }

    def form_valid(self, form):
        try:
            results = form.import_plots(self.protocol)
        except Exception as err:
            messages.error(self.request, str(err))
        else:
            msg = f"{results['imported']} placettes ont été importées"
            if results['already']:
                msg += f". {results['already']} placettes étaient déjà créées"
            messages.success(self.request, msg)
        return HttpResponseRedirect(reverse('protocole', kwargs=self.kwargs))


class RFInfoView(View):
    """Parse http://www.rfinfo.vd.ch content and return structured JSON."""
    def get(self, request, *args, **kwargs):
        import httpx
        from bs4 import BeautifulSoup
        r = httpx.get('http://www.rfinfo.vd.ch/rfinfo.php?no_commune=%s&no_immeuble=%s' % (
            request.GET.get('no_commune'), request.GET.get('no_immeuble'))
        )
        soup = BeautifulSoup(r.text, features="html.parser")

        is_propri = is_genre = False
        propris = []
        genre = ''
        for tr in soup.find_all('tr'):
            if tr.td and tr.td.text.startswith('Propriétaire(s)'):
                is_propri = True
            elif tr.td and tr.td.text.startswith('Genre(s) de nature et bâtiment(s)'):
                is_genre = True
            elif tr.td and tr.td.text.startswith('Conditions d'):
                is_propri = False
            else:
                if is_propri:
                    propris.append(tr.td.text)
                elif is_genre:
                    genre = tr.td.text
                    is_genre = False

        return JsonResponse({'owners': propris, 'genre': genre})


class VocabView(DetailView):
    model = Vocabulary
    template_name = 'vocabulary_detail.html'


class DocumentationView(TemplateView):
    template_name = "docs.html"

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        context['docs'] = Document.objects.all().order_by('-weight')
        return context


class EssencesView(TemplateView):
    template_name = "essences.html"

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        context.update({
            'actives': TreeSpecies.objects.filter(active=True).order_by('order'),
            'inactives': TreeSpecies.objects.filter(active=False),
        })
        return context


class EssencesExportView(View):
    def get(self, request, *args, **kwargs):
        content = '\r\n'.join([ts.abbrev for ts in TreeSpecies.objects.filter(active=True).order_by('order')])
        response = HttpResponse(content, content_type='text/plain')
        response['Content-Disposition'] = 'attachment; filename=SPCNAME.TXT'
        return response


class FBaseComparisonView(FormView):
    template_name = 'fbase-comparison.html'
    form_class = FBaseForm

    def form_valid(self, form):
        result = form.check_fbases()
        return self.render_to_response(self.get_context_data(form=form, result=result))


class TopApImportView(FormView):
    template_name = 'import_topap.html'
    form_class = TopapForm

    def form_valid(self, form):
        try:
            form.import_topap()
        except Exception as err:
            messages.error(self.request, "Erreur d'importation du fichier: %s" % err)
        else:
            messages.success(self.request, "Le fichier Topap a été importé avec succès")
        return HttpResponseRedirect(self.request.path)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['last_modif'] = Topap.objects.aggregate(last_mod=Max('date_modif'))['last_mod']
        return context


def to_csv(value):
    if value is None:
        return ''
    return value
