from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0024_plotobs_protocol_nonnull'),
    ]

    operations = [
        migrations.CreateModel(
            name='AME_CE',
            fields=[
                ('id', models.IntegerField(db_column='OBJECTID', primary_key=True, serialize=False)),
                ('inv_GuidInv', models.IntegerField(db_index=True, null=True)),
                ('CoordX', models.IntegerField(null=True)),
                ('CoordY', models.IntegerField(null=True)),
                ('inv_FkCxCy', models.CharField(max_length=15)),
                ('DateCreation', models.TextField(null=True)),
                ('DateModif', models.TextField(null=True)),
                ('MachineCreation', models.TextField(null=True)),
                ('MachineModif', models.TextField(null=True)),
                ('AuteurCreation', models.TextField(null=True)),
                ('AuteurModif', models.TextField(null=True)),
                ('BaseSource', models.TextField(null=True)),
                ('TableSource', models.TextField(null=True)),
                ('RespFiche', models.TextField(null=True)),
                ('RespPrecFiche', models.FloatField(null=True)),
                ('inv_MpmgFeuillus', models.IntegerField(null=True)),
                ('inv_MpmgResineux', models.IntegerField(null=True)),
                ('inv_MpmgTotal', models.IntegerField(null=True)),
                ('inv_GpmgFeuillus', models.IntegerField(null=True)),
                ('inv_GpmgResineux', models.IntegerField(null=True)),
                ('inv_GpmgTotal', models.IntegerField(null=True)),
                ('inv_PpmgFeuillus', models.IntegerField(null=True)),
                ('inv_PpmgResineux', models.IntegerField(null=True)),
                ('inv_PpmgTotal', models.IntegerField(null=True)),
                ('inv_EpiceaAbro', models.FloatField(null=True)),
                ('inv_EpiceaFrot', models.FloatField(null=True)),
                ('inv_FeuillusAbro', models.FloatField(null=True)),
                ('inv_FeuillusFrot', models.FloatField(null=True)),
                ('inv_NbTigeFeuPC', models.IntegerField(null=True)),
                ('inv_NbTigeResPC', models.IntegerField(null=True)),
                ('inv_ResineuxAbro', models.FloatField(null=True)),
                ('inv_ResineuxFrot', models.FloatField(null=True)),
                ('inv_GFeuPC', models.IntegerField(null=True)),
                ('inv_GResPC', models.IntegerField(null=True)),
                ('inv_VoChenePC', models.FloatField(null=True)),
                ('inv_VoDouglasPC', models.FloatField(null=True)),
                ('inv_VoEpiceaPC', models.FloatField(null=True)),
                ('inv_VoErablePC', models.FloatField(null=True)),
                ('inv_VoFeuPC', models.FloatField(null=True)),
                ('inv_VoFeuillus1PC', models.FloatField(null=True)),
                ('inv_VoFeuillus2PC', models.FloatField(null=True)),
                ('inv_VoFeuDivPC', models.FloatField(null=True)),
                ('inv_VoFrenePC', models.FloatField(null=True)),
                ('inv_VoHetrePC', models.FloatField(null=True)),
                ('inv_VoMelezePC', models.FloatField(null=True)),
                ('inv_VoMerisierPC', models.FloatField(null=True)),
                ('inv_VoPeuplierPC', models.FloatField(null=True)),
                ('inv_VoPinPC', models.FloatField(null=True)),
                ('inv_VoResPC', models.FloatField(null=True)),
                ('inv_VoResDivPC', models.FloatField(null=True)),
                ('inv_VoSapinPC', models.FloatField(null=True)),
                ('inv_InvAnnee', models.IntegerField(null=True)),
                ('inv_ChampLib1', models.TextField(null=True)),
                ('inv_ChampLib2', models.TextField(null=True)),
                ('inv_ChampLib3', models.TextField(null=True)),
                ('inv_CodeStratification', models.FloatField(null=True)),
                ('inv_LvDernierInv', models.TextField(null=True)),
                ('inv_DiDom', models.IntegerField(null=True)),
                ('inv_DiMoFeuillus', models.IntegerField(null=True)),
                ('inv_DiMoResineux', models.IntegerField(null=True)),
                ('inv_DiMo', models.IntegerField(null=True)),
                ('inv_Fbase', models.TextField(null=True)),
                ('inv_LvGestionPassee', models.FloatField(null=True)),
                ('inv_Interv', models.TextField(null=True)),
                ('inv_MelMoins10', models.TextField(null=True)),
                ('inv_MelPlus16', models.TextField(null=True)),
                ('inv_Mel10a16', models.TextField(null=True)),
                ('inv_LvNatureInv', models.IntegerField(null=True)),
                ('inv_NbResDispo', models.FloatField(null=True)),
                ('inv_NbTigeChene16P', models.IntegerField(null=True)),
                ('inv_NbTigeChene1016', models.IntegerField(null=True)),
                ('inv_NbTigeDouglas16P', models.IntegerField(null=True)),
                ('inv_NbTigeDouglas1016', models.IntegerField(null=True)),
                ('inv_NbTigeFeu10', models.IntegerField(null=True)),
                ('inv_NbTigeFeu16P', models.IntegerField(null=True)),
                ('inv_NbTigeFeuillus116P', models.IntegerField(null=True)),
                ('inv_NbTigeFeuillus11016', models.IntegerField(null=True)),
                ('inv_NbTigeFeuillus216P', models.IntegerField(null=True)),
                ('inv_NbTigeFeuillus21016', models.IntegerField(null=True)),
                ('inv_NbTigeFeu1016', models.IntegerField(null=True)),
                ('inv_NbTigeFeuDiv16P', models.IntegerField(null=True)),
                ('inv_NbTigeFeuDiv1016', models.IntegerField(null=True)),
                ('inv_NbTigeFrene16P', models.IntegerField(null=True)),
                ('inv_NbTigeFrene1016', models.IntegerField(null=True)),
                ('inv_NbTigeHetre16P', models.IntegerField(null=True)),
                ('inv_NbTigeHetre1016', models.IntegerField(null=True)),
                ('inv_NbTigeMeleze16P', models.IntegerField(null=True)),
                ('inv_NbTigeMeleze1016', models.IntegerField(null=True)),
                ('inv_NbTigeMerisier16P', models.IntegerField(null=True)),
                ('inv_NbTigeMerisier1016', models.IntegerField(null=True)),
                ('inv_NbTigePeuplier16P', models.IntegerField(null=True)),
                ('inv_NbTigePeuplier1016', models.IntegerField(null=True)),
                ('inv_NbTigePin16P', models.IntegerField(null=True)),
                ('inv_NbTigePin1016', models.IntegerField(null=True)),
                ('inv_NbTigeRes10', models.IntegerField(null=True)),
                ('inv_NbTigeRes16P', models.IntegerField(null=True)),
                ('inv_NbTigeRes1016', models.IntegerField(null=True)),
                ('inv_NbTigeResDiv16P', models.IntegerField(null=True)),
                ('inv_NbTigeResDiv1016', models.IntegerField(null=True)),
                ('inv_NbTigeSapin16P', models.IntegerField(null=True)),
                ('inv_NbTigeSapin1016', models.IntegerField(null=True)),
                ('inv_NbTigeEpicea16P', models.IntegerField(null=True)),
                ('inv_NbTigeEpicea1016', models.IntegerField(null=True)),
                ('inv_NbTigeErable16P', models.IntegerField(null=True)),
                ('inv_NbTigeErable1016', models.IntegerField(null=True)),
                ('inv_NbTige10', models.IntegerField(null=True)),
                ('inv_NbTige16P', models.IntegerField(null=True)),
                ('inv_NbTige1016', models.IntegerField(null=True)),
                ('inv_NbEpiceaDispo', models.FloatField(null=True)),
                ('inv_NbFeuillusDispo', models.FloatField(null=True)),
                ('inv_PenteInv', models.IntegerField(null=True)),
                ('Lv_PropriHist', models.TextField(null=True)),
                ('inv_Protocole', models.TextField(null=True)),
                ('inv_protocoleLiaison', models.TextField(null=True)),
                ('inv_Region', models.TextField(null=True)),
                ('inv_RemInv', models.TextField(null=True)),
                ('inv_RemCodeeR1', models.FloatField(null=True)),
                ('inv_RemCodeeR2', models.FloatField(null=True)),
                ('inv_SurfaceRepres', models.IntegerField(null=True)),
                ('inv_Gfeuillus', models.IntegerField(null=True)),
                ('inv_Gresineux', models.IntegerField(null=True)),
                ('inv_GTotal', models.IntegerField(null=True)),
                ('inv_ChampLib1Titre', models.TextField(null=True)),
                ('inv_ChampLib2Titre', models.TextField(null=True)),
                ('inv_ChampLib3Titre', models.TextField(null=True)),
                ('inv_TitreFeuillus1', models.TextField(null=True)),
                ('inv_TitreFeuillus2', models.TextField(null=True)),
                ('inv_IntervTitre', models.TextField(null=True)),
                ('inv_VoChene16P', models.IntegerField(null=True)),
                ('inv_VoDouglas16P', models.IntegerField(null=True)),
                ('inv_VoEpicea16P', models.IntegerField(null=True)),
                ('inv_VoErable16P', models.IntegerField(null=True)),
                ('inv_VoFeu16P', models.IntegerField(null=True)),
                ('inv_VoFeuillus116P', models.IntegerField(null=True)),
                ('inv_VoFeuillus216P', models.IntegerField(null=True)),
                ('inv_VoFeuDiv16P', models.IntegerField(null=True)),
                ('inv_VoFrene16P', models.IntegerField(null=True)),
                ('inv_VoHetre16P', models.IntegerField(null=True)),
                ('inv_VoMeleze16P', models.IntegerField(null=True)),
                ('inv_VoMerisier16P', models.IntegerField(null=True)),
                ('inv_VoPeuplier16P', models.IntegerField(null=True)),
                ('inv_VoPin16P', models.IntegerField(null=True)),
                ('inv_VoRes16P', models.IntegerField(null=True)),
                ('inv_VoResDiv16P', models.IntegerField(null=True)),
                ('inv_VoSapin16P', models.IntegerField(null=True)),
                ('inv_Vo16P', models.IntegerField(null=True)),
                ('inv_Vo1016', models.IntegerField(null=True)),
                ('inv_VoChene1016', models.IntegerField(null=True)),
                ('inv_VoDouglas1016', models.IntegerField(null=True)),
                ('inv_VoEpicea1016', models.IntegerField(null=True)),
                ('inv_VoErable1016', models.IntegerField(null=True)),
                ('inv_VoFeuillus11016', models.IntegerField(null=True)),
                ('inv_VoFeuillus21016', models.IntegerField(null=True)),
                ('inv_VoFeu1016', models.IntegerField(null=True)),
                ('inv_VoFeuDiv1016', models.IntegerField(null=True)),
                ('inv_VoFrene1016', models.IntegerField(null=True)),
                ('inv_VoHetre1016', models.IntegerField(null=True)),
                ('inv_VoMeleze1016', models.IntegerField(null=True)),
                ('inv_VoMerisier1016', models.IntegerField(null=True)),
                ('inv_VoPeuplier1016', models.IntegerField(null=True)),
                ('inv_VoPin1016', models.IntegerField(null=True)),
                ('inv_VoRes1016', models.IntegerField(null=True)),
                ('inv_VoResDiv1016', models.IntegerField(null=True)),
                ('inv_VoSapin1016', models.IntegerField(null=True)),
            ],
            options={
                'db_table': 'imports"."ame_ce',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='BaseDbViewDiametre16',
            fields=[
                ('plot_obs', models.OneToOneField(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='inventory.plotobs', verbose_name='Observation de placette')),
                ('nb_tree', models.IntegerField(db_column='nombre de tiges', verbose_name='Nombre de tiges')),
                ('rpstz', models.FloatField(db_column='RPSTZ')),
                ('nb_tree_ha', models.FloatField(db_column='nombre de tiges/hectare', verbose_name="Nombre de tiges à l'hectare")),
                ('volume_ha', models.FloatField(db_column='volume (m3) tarif vaudois/hectare', verbose_name="Volume à l'hectare [m3] (tarif vaudois)")),
                ('surface_ha', models.FloatField(db_column='surface terriere (cm2)/hectare', verbose_name="Surface terrière à l'hectare [cm2]")),
                ('diam_avg', models.FloatField(db_column='diametre moyen', verbose_name='Diamètre moyen')),
                ('avg_tree_volume', models.FloatField(db_column="volume de l'arbre moyen", verbose_name="Volume de l'arbre moyen")),
                ('avg_tree_surface', models.FloatField(db_column="surface terriere de l'arbre moyen", verbose_name="Surface terrière de l'arbre moyen")),
            ],
            options={
                'db_table': 'base_surf_vol_par_plotobs_diametre_16',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='BaseDbViewDiametre9',
            fields=[
                ('plot_obs', models.OneToOneField(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='inventory.plotobs', verbose_name='Observation de placette')),
                ('nb_tree', models.IntegerField(db_column='nombre de tiges', verbose_name='Nombre de tiges')),
                ('rpstz', models.FloatField(db_column='RPSTZ')),
                ('nb_tree_ha', models.FloatField(db_column='nombre de tiges/hectare', verbose_name="Nombre de tiges à l'hectare")),
                ('volume_ha', models.FloatField(db_column='volume (m3) tarif vaudois/hectare', verbose_name="Volume à l'hectare [m3] (tarif vaudois)")),
                ('surface_ha', models.FloatField(db_column='surface terriere (cm2)/hectare', verbose_name="Surface terrière à l'hectare [cm2]")),
                ('diam_avg', models.FloatField(db_column='diametre moyen', verbose_name='Diamètre moyen')),
                ('avg_tree_volume', models.FloatField(db_column="volume de l'arbre moyen", verbose_name="Volume de l'arbre moyen")),
                ('avg_tree_surface', models.FloatField(db_column="surface terriere de l'arbre moyen", verbose_name="Surface terrière de l'arbre moyen")),
            ],
            options={
                'db_table': 'base_surf_vol_par_plotobs_diametre_9',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='VocabularyValue',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.CharField(max_length=1, verbose_name='Valeur')),
            ],
            options={
                'db_table': 'base_vocabulary_values',
                'managed': False,
            },
        ),
    ]
