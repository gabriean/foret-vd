from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0015_make_integers_positive'),
    ]

    operations = [
        migrations.AddField(
            model_name='protocol',
            name='default_density',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='inventory.Density', verbose_name='Densité par défaut'),
        ),
    ]
