from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0016_prot_default_density'),
    ]

    operations = [
        migrations.AddField(
            model_name='plotobs',
            name='ecorcage',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Écorçage du cerf'),
        ),
    ]
