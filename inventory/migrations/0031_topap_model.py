import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0030_treespecies_order'),
    ]

    operations = [
        migrations.CreateModel(
            name='Topap',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('polygon', django.contrib.gis.db.models.fields.MultiPolygonField(srid=2056)),
                ('guidpa', models.CharField(db_column='top_GuidPa', max_length=32)),
                ('fk_guid', models.CharField(db_column='top_FkGuid', max_length=32)),
                ('lad_fkterr', models.IntegerField(db_column='lad_FkTerr')),
                ('lad_fkardt', models.IntegerField(db_column='lad_FkArdt')),
                ('lad_fkcirc', models.IntegerField(db_column='lad_FkCirc')),
                ('lad_fkci1', models.IntegerField(db_column='lad_FkCi_1')),
                ('lad_fkdist', models.IntegerField(db_column='lad_FkDist')),
                ('lieu_dit', models.CharField(db_column='top_LieuDi', max_length=110)),
                ('triage_ge', models.CharField(db_column='LvTriageGe', max_length=50)),
                ('propri', models.CharField(db_column='LvPropri', max_length=5)),
                ('remarques', models.CharField(blank=True, db_column='top_Remarq', max_length=254)),
                ('rem_con', models.CharField(blank=True, db_column='top_RemCon', max_length=254)),
                ('resp_maj', models.CharField(db_column='LvRespMaj', max_length=4)),
                ('serie', models.CharField(db_column='top_Serie', max_length=1)),
                ('division', models.CharField(db_column='top_Div', max_length=2)),
                ('serie_div', models.CharField(db_column='top_SerieD', max_length=4)),
                ('lv_stat', models.IntegerField(db_column='top_LvStat')),
                ('code', models.CharField(db_column='top_Code', max_length=6)),
                ('date_ma', models.DateField(blank=True, db_column='top_DateMa', null=True)),
                ('date_1', models.FloatField(db_column='top_Date_1')),
                ('date_va', models.DateField(blank=True, db_column='top_DateVa', null=True)),
                ('coordx', models.FloatField(db_column='CoordX')),
                ('coordy', models.FloatField(db_column='CoordY')),
                ('auteur_crea', models.CharField(db_column='AuteurCrea', max_length=50)),
                ('auteur_modif', models.CharField(db_column='AuteurModi', max_length=50)),
                ('base_source', models.CharField(db_column='BaseSource', max_length=254)),
                ('date_crea', models.DateField(db_column='DateCreati')),
                ('date_modif', models.DateField(db_column='DateModif')),
                ('machine_crea', models.CharField(db_column='MachineCre', max_length=50)),
                ('machine_mod', models.CharField(db_column='MachineMod', max_length=50)),
                ('resp_fiche', models.CharField(db_column='RespFiche', max_length=50)),
                ('resp_prec_fi', models.CharField(db_column='RespPrecFi', max_length=50)),
                ('table_source', models.CharField(db_column='TableSourc', max_length=50)),
                ('shape_len', models.FloatField(db_column='SHAPE_Leng')),
                ('shape_area', models.FloatField(db_column='SHAPE_Area')),
            ],
        ),
    ]
