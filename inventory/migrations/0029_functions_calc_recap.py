from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0028_plotobsplanned'),
    ]

    operations = [
        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION public.ksp_vd_tarif_vaudois_tableau(diameter smallint)
    RETURNS integer
    LANGUAGE 'sql'

    COST 100
    IMMUTABLE 
AS $BODY$
SELECT CASE
        WHEN $1 = 10 THEN 7
        WHEN $1 = 11 THEN 8
        WHEN $1 = 12 THEN 9
        WHEN $1 = 13 THEN 10
        WHEN $1 = 14 THEN 11
        WHEN $1 = 15 THEN 13
        WHEN $1 = 16 THEN 15
        WHEN $1 = 17 THEN 17
        WHEN $1 = 18 THEN 19
        WHEN $1 = 19 THEN 21
        WHEN $1 = 20 THEN 24
        WHEN $1 = 21 THEN 27
        WHEN $1 = 22 THEN 31
        WHEN $1 = 23 THEN 35
        WHEN $1 = 24 THEN 39
        WHEN $1 = 25 THEN 43
        WHEN $1 = 26 THEN 48
        WHEN $1 = 27 THEN 53
        WHEN $1 = 28 THEN 58
        WHEN $1 = 29 THEN 64
        WHEN $1 = 30 THEN 70
        WHEN $1 = 31 THEN 76
        WHEN $1 = 32 THEN 82
        WHEN $1 = 33 THEN 88
        WHEN $1 = 34 THEN 95
        WHEN $1 = 35 THEN 102
        WHEN $1 = 36 THEN 109
        WHEN $1 = 37 THEN 117
        WHEN $1 = 38 THEN 125
        WHEN $1 = 39 THEN 133
        WHEN $1 = 40 THEN 142
        WHEN $1 = 41 THEN 151
        WHEN $1 = 42 THEN 160
        WHEN $1 = 43 THEN 169
        WHEN $1 = 44 THEN 178
        WHEN $1 = 45 THEN 188
        WHEN $1 = 46 THEN 198
        WHEN $1 = 47 THEN 208
        WHEN $1 = 48 THEN 218
        WHEN $1 = 49 THEN 229
        WHEN $1 = 50 THEN 240
        WHEN $1 = 51 THEN 251
        WHEN $1 = 52 THEN 262
        WHEN $1 = 53 THEN 273
        WHEN $1 = 54 THEN 284
        WHEN $1 = 55 THEN 296
        WHEN $1 = 56 THEN 308
        WHEN $1 = 57 THEN 320
        WHEN $1 = 58 THEN 333
        WHEN $1 = 59 THEN 346
        WHEN $1 = 60 THEN 359
        WHEN $1 = 61 THEN 373
        WHEN $1 = 62 THEN 387
        WHEN $1 = 63 THEN 401
        WHEN $1 = 64 THEN 415
        WHEN $1 = 65 THEN 429
        WHEN $1 = 66 THEN 444
        WHEN $1 = 67 THEN 459
        WHEN $1 = 68 THEN 475
        WHEN $1 = 69 THEN 491
        WHEN $1 = 70 THEN 507
        WHEN $1 = 71 THEN 523
        WHEN $1 = 72 THEN 540
        WHEN $1 = 73 THEN 557
        WHEN $1 = 74 THEN 574
        WHEN $1 = 75 THEN 591
        WHEN $1 = 76 THEN 608
        WHEN $1 = 77 THEN 625
        WHEN $1 = 78 THEN 643
        WHEN $1 = 79 THEN 661
        WHEN $1 = 80 THEN 679
        WHEN $1 = 81 THEN 697
        WHEN $1 = 82 THEN 715
        WHEN $1 = 83 THEN 733
        WHEN $1 = 84 THEN 752
        WHEN $1 = 85 THEN 771
        WHEN $1 = 86 THEN 790
        WHEN $1 = 87 THEN 809
        WHEN $1 = 88 THEN 828
        WHEN $1 = 89 THEN 848
        WHEN $1 = 90 THEN 868
        WHEN $1 = 91 THEN 888
        WHEN $1 = 92 THEN 908
        WHEN $1 = 93 THEN 928
        WHEN $1 = 94 THEN 949
        WHEN $1 = 95 THEN 970
        WHEN $1 = 96 THEN 991
        WHEN $1 = 97 THEN 1012
        WHEN $1 = 98 THEN 1033
        WHEN $1 = 99 THEN 1055
        WHEN $1 = 100 THEN 1077
        WHEN $1 = 101 THEN 1099
        WHEN $1 = 102 THEN 1121
        WHEN $1 = 103 THEN 1143
        WHEN $1 = 104 THEN 1166
        WHEN $1 = 105 THEN 1189
        WHEN $1 = 106 THEN 1212
        WHEN $1 = 107 THEN 1236
        WHEN $1 = 108 THEN 1260
        WHEN $1 = 109 THEN 1284
        WHEN $1 = 110 THEN 1310
        WHEN $1 = 111 THEN 1337
        ELSE 0
    END AS "volume tarif vaudois tableau";
$BODY$;""",
            "DROP FUNCTION ksp_vd_tarif_vaudois_tableau"),

        migrations.RunSQL("""
CREATE OR REPLACE VIEW public.base_surf_terr_species_gt10
 AS
 SELECT po_calc.obs_id,
    po_calc.spec_id,
    po_calc.species,
    10000::double precision / ksp_vd_surface_placette_exacte(plot_obs.radius, plot_obs.slope) * COALESCE(po_calc.surf_terr, 0::double precision) AS g10,
    prot.year,
    plot_obs.owner_id,
    ksp_vd_surface_terriere(COALESCE(po_calc.diametre_moyen, 0.0)::double precision) AS surf_terr_moyen
   FROM ( SELECT tree.obs_id,
            tree.spec_id,
            tree_spec.species,
            avg(tree.diameter) AS diametre_moyen,
            sum(ksp_vd_surface_terriere(tree.diameter)) AS surf_terr
           FROM tree
             LEFT JOIN tree_spec ON tree_spec.id = tree.spec_id
          WHERE tree.diameter > 9
          GROUP BY tree.obs_id, tree.spec_id, tree_spec.species) po_calc
     LEFT JOIN plot_obs ON plot_obs.id = po_calc.obs_id
     LEFT JOIN protocol prot ON prot.id = plot_obs.protocol_id;""",
            "DROP VIEW base_surf_terr_species_gt10"),

        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION public.ksp_calc_num_tiges(owner_pk integer, annee integer, surf_tot integer)
    RETURNS TABLE(spec_id integer, species text, diam_class text, sum_abs numeric, sum_ha numeric, sum_vol numeric, sum_vol_ha numeric) 
    LANGUAGE 'sql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
SELECT spec_id, species, diam_class, round(sum(num_tiges_real)) as sum, round(sum(num_tiges_real) / surf_tot, 3) as sum_ha, round(sum(volume_real), 1) AS sum_vol, round(sum(volume_real) / surf_tot, 3) AS sum_vol_ha FROM (
SELECT
    po_calc.id,
    trees.spec_id,
    trees.species,
    trees.diam_class,
    trees.volume,
    round(trees.volume * po_calc.repr_fact::numeric, 2) AS volume_real,
    trees.num_tiges,
    trees.num_tiges * po_calc.repr_fact::numeric AS num_tiges_real,
    prot.year
    FROM (
        SELECT plot_obs.id,
            plot_obs.protocol_id,
            plot_obs.owner_id,
            density.surface / round(ksp_vd_surface_placette_exacte(plot_obs.radius, plot_obs.slope)) AS repr_fact
        FROM plot_obs
        LEFT JOIN density ON plot_obs.density_id = density.id
        WHERE owner_id = owner_pk
    ) po_calc
    LEFT JOIN (
        SELECT obs_id,
            spec_id,
            tree_spec.species,
            CASE WHEN diameter < 16 THEN 'N8-15'
                WHEN diameter >= 16 AND diameter < 20 THEN 'N16-19'
                WHEN diameter >= 20 AND diameter < 24 THEN 'N20-23'
                WHEN diameter >= 24 AND diameter < 28 THEN 'N24-27'
                WHEN diameter >= 28 AND diameter < 32 THEN 'N28-31'
                WHEN diameter >= 32 AND diameter < 36 THEN 'N32-35'
                WHEN diameter >= 36 AND diameter < 40 THEN 'N36-39'
                WHEN diameter >= 40 AND diameter < 44 THEN 'N40-43'
                WHEN diameter >= 44 AND diameter < 48 THEN 'N44-47'
                WHEN diameter >= 48 AND diameter < 52 THEN 'N48-51'
                WHEN diameter >= 52 AND diameter < 56 THEN 'N52-55'
                WHEN diameter >= 56 AND diameter < 60 THEN 'N56-59'
                WHEN diameter >= 60 AND diameter < 64 THEN 'N60-63'
                WHEN diameter >= 64 AND diameter < 68 THEN 'N64-67'
                WHEN diameter >= 68 AND diameter < 72 THEN 'N68-71'
                WHEN diameter >= 72 AND diameter < 76 THEN 'N72-75'
                WHEN diameter >= 76 AND diameter < 80 THEN 'N76-79'
                ELSE 'N80+'
            END AS diam_class,
            sum(ksp_vd_tarif_vaudois_tableau(diameter)::decimal / 100) AS volume,
            count(*) AS num_tiges
            FROM tree
            LEFT JOIN tree_spec ON tree_spec.id = tree.spec_id
            GROUP BY obs_id, spec_id, species, diam_class
    ) trees ON po_calc.id = trees.obs_id
    LEFT JOIN protocol prot ON prot.id = po_calc.protocol_id
    WHERE year = annee
) a GROUP BY a.spec_id, a.species, a.diam_class;
$BODY$;""", "DROP FUNCTION ksp_calc_surf_terr(integer, integer)"),

        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION public.ksp_calc_surf_terr(owner_pk integer, annee integer)
RETURNS TABLE(spec_id integer, species text, surf_terr double precision) 
    LANGUAGE 'sql'
    COST 100
    IMMUTABLE 
    ROWS 100
AS $BODY$
    SELECT spec_id, species, sum(g10 / subq.num_plots / 10000)
    FROM base_surf_terr_species_gt10 base
    INNER JOIN (
        SELECT count(*) over() AS num_plots, plot_obs.id FROM plot_obs
            LEFT JOIN protocol on protocol.id=plot_obs.protocol_id
            WHERE owner_id=owner_pk AND protocol.year=annee
        ) subq on base.obs_id = subq.id
GROUP BY spec_id, species;
$BODY$;""", "DROP FUNCTION ksp_calc_surf_terr(integer, integer)"),
    ]
