from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0029_functions_calc_recap'),
    ]

    operations = [
        migrations.AddField(
            model_name='treespecies',
            name='order',
            field=models.PositiveSmallIntegerField(blank=True, null=True),
        ),
    ]
