from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0007_density_surface'),
    ]

    operations = [
        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION ksp_vd_surface_terriere(diameter double precision)
  RETURNS double precision AS
$BODY$
  SELECT pi() * ($1 * $1) / 4 AS surface_terriere_arbre;
$BODY$
  LANGUAGE sql IMMUTABLE
  COST 100;""", "DROP FUNCTION ksp_vd_surface_terriere(diameter double precision)"),

        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION ksp_vd_surface_placette_exacte(radius smallint, slope smallint)
  RETURNS double precision AS
$BODY$
  SELECT pi() * $1 * $1 * cos($2 / (pi() * 180)) AS "surface placette exacte_sans corr_0.5";
$BODY$
  LANGUAGE sql IMMUTABLE
  COST 100;""", "DROP FUNCTION ksp_vd_surface_placette_exacte(radius smallint, slope smallint)"),

        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION ksp_vd_tarif_vaudois_fonction(diameter double precision)
  RETURNS double precision AS
$BODY$
  SELECT GREATEST(0.0, (-0.033804) - (0.010468 * $1) + (0.001184 * $1 * $1)) AS "volume m3 tarif vaudois fonction";
$BODY$
  LANGUAGE sql IMMUTABLE
  COST 100;""", "DROP FUNCTION ksp_vd_tarif_vaudois_fonction(diameter double precision)"),

        migrations.RunSQL("""
CREATE OR REPLACE VIEW base_surf_vol_par_plotobs_diametre_16 AS 
 SELECT plot_obs.id,
    plot_obs.plot_id,
    plot.municipality_id,
    plot_obs.interv_int_id,
    plot_obs.main_nature_id,
    plot_obs.owner_id,
    plot_obs.protocol_id,
    COALESCE(subq."nombre de tiges", 0) AS "nombre de tiges",
        density.surface::double precision / ksp_vd_surface_placette_exacte(plot_obs.radius, plot_obs.slope) AS "RPSTZ",
        density.surface::double precision / ksp_vd_surface_placette_exacte(plot_obs.radius, plot_obs.slope) * COALESCE(subq."nombre de tiges", 0)::double precision AS "nombre de tiges/hectare",
        density.surface::double precision / ksp_vd_surface_placette_exacte(plot_obs.radius, plot_obs.slope) * COALESCE(subq."volume tarif vaudois", 0)::double precision AS "volume (m3) tarif vaudois/hectare",
        density.surface::double precision / ksp_vd_surface_placette_exacte(plot_obs.radius, plot_obs.slope) * COALESCE(subq."surface terriere", 0) AS "surface terriere (cm2)/hectare",
    COALESCE(subq."diametre moyen",0.0) AS "diametre moyen",
    ksp_vd_tarif_vaudois_fonction(COALESCE(subq."diametre moyen", 0.0)) AS "volume de l'arbre moyen",
    ksp_vd_surface_terriere(COALESCE(subq."diametre moyen", 0.0)) AS "surface terriere de l'arbre moyen"
   FROM ( SELECT tree.obs_id,
            count(*) AS "nombre de tiges",
            sum(ksp_vd_tarif_vaudois_fonction(tree.diameter::double precision)) AS "volume tarif vaudois",
            sum(ksp_vd_surface_terriere(tree.diameter::double precision)) AS "surface terriere",
            avg(tree.diameter) AS "diametre moyen"
           FROM tree
          WHERE tree.diameter >= 16
          GROUP BY tree.obs_id) subq
     FULL JOIN plot_obs ON subq.obs_id = plot_obs.id
     LEFT JOIN plot ON plot_obs.plot_id = plot.id
     LEFT JOIN density ON plot_obs.density_id = density.id;""",
        "DROP VIEW base_surf_vol_par_plotobs_diametre_16"),

        migrations.RunSQL("""
CREATE OR REPLACE VIEW web_homepage_municipality_diametre_16 AS 
 SELECT row_number() OVER () AS id,
    base_surf_vol_par_plotobs_diametre_16.protocol_id,
    base_surf_vol_par_plotobs_diametre_16.municipality_id,
    count(*) AS "nombre de placettes",
    avg(base_surf_vol_par_plotobs_diametre_16."nombre de tiges/hectare") AS "nombre de tiges (>= 16cm)/hectare",
    avg(base_surf_vol_par_plotobs_diametre_16."volume (m3) tarif vaudois/hectare") AS "volume (m3) tarif vaudois/hectare",
    avg(base_surf_vol_par_plotobs_diametre_16."surface terriere (cm2)/hectare") AS "surface terriere (cm2) /hectare",
    avg(base_surf_vol_par_plotobs_diametre_16."diametre moyen") AS "diametre moyen (cm2)",
    avg(base_surf_vol_par_plotobs_diametre_16."volume de l'arbre moyen") AS "volume de l'arbre moyen (m3)",
    avg(base_surf_vol_par_plotobs_diametre_16."surface terriere de l'arbre moyen") AS "surface terriere de l'arbre moyen (cm2)",
    avg(base_surf_vol_par_plotobs_diametre_16."RPSTZ") * count(*)::double precision AS "surface forestiere theorique"
   FROM base_surf_vol_par_plotobs_diametre_16
  GROUP BY base_surf_vol_par_plotobs_diametre_16.municipality_id, base_surf_vol_par_plotobs_diametre_16.protocol_id;""",
        "DROP VIEW web_homepage_municipality_diametre_16"),

        migrations.RunSQL("""
CREATE OR REPLACE VIEW web_homepage_owner_diametre_16 AS 
 SELECT row_number() OVER () AS id,
    base_surf_vol_par_plotobs_diametre_16.protocol_id,
    base_surf_vol_par_plotobs_diametre_16.owner_id,
    count(*) AS "nombre de placettes",
    avg(base_surf_vol_par_plotobs_diametre_16."nombre de tiges/hectare") AS "nombre de tiges (>= 16cm)/hectare",
    avg(base_surf_vol_par_plotobs_diametre_16."volume (m3) tarif vaudois/hectare") AS "volume (m3) tarif vaudois/hectare",
    avg(base_surf_vol_par_plotobs_diametre_16."surface terriere (cm2)/hectare") AS "surface terriere (cm2) /hectare",
    avg(base_surf_vol_par_plotobs_diametre_16."diametre moyen") AS "diametre moyen (cm2)",
    avg(base_surf_vol_par_plotobs_diametre_16."volume de l'arbre moyen") AS "volume de l'arbre moyen (m3)",
    avg(base_surf_vol_par_plotobs_diametre_16."surface terriere de l'arbre moyen") AS "surface terriere de l'arbre moyen (cm2)",
    avg(base_surf_vol_par_plotobs_diametre_16."RPSTZ") * count(*)::double precision AS "surface forestiere theorique"
   FROM base_surf_vol_par_plotobs_diametre_16
  GROUP BY base_surf_vol_par_plotobs_diametre_16.owner_id, base_surf_vol_par_plotobs_diametre_16.protocol_id;""",
        "DROP VIEW web_homepage_owner_diametre_16"),

        migrations.RunSQL("""
CREATE OR REPLACE VIEW base_surf_vol_par_plotobs_diametre_9 AS 
 SELECT plot_obs.id,
    plot_obs.plot_id,
    plot.municipality_id,
    plot_obs.interv_int_id,
    plot_obs.main_nature_id,
    plot_obs.owner_id,
    plot_obs.protocol_id,
    COALESCE(subq."nombre de tiges", 0) AS "nombre de tiges",
        density.surface::double precision / ksp_vd_surface_placette_exacte(plot_obs.radius, plot_obs.slope) AS "RPSTZ",
        density.surface::double precision / ksp_vd_surface_placette_exacte(plot_obs.radius, plot_obs.slope) * COALESCE(subq."nombre de tiges", 0)::double precision AS "nombre de tiges/hectare",
        density.surface::double precision / ksp_vd_surface_placette_exacte(plot_obs.radius, plot_obs.slope) * COALESCE(subq."volume tarif vaudois", 0)::double precision AS "volume (m3) tarif vaudois/hectare",
        density.surface::double precision / ksp_vd_surface_placette_exacte(plot_obs.radius, plot_obs.slope) * COALESCE(subq."surface terriere", 0) AS "surface terriere (cm2)/hectare",
    COALESCE(subq."diametre moyen",0.0) AS "diametre moyen",
    ksp_vd_tarif_vaudois_fonction(COALESCE(subq."diametre moyen", 0.0)) AS "volume de l'arbre moyen",
    ksp_vd_surface_terriere(COALESCE(subq."diametre moyen", 0.0)) AS "surface terriere de l'arbre moyen"
   FROM ( SELECT tree.obs_id,
            count(*) AS "nombre de tiges",
            sum(ksp_vd_tarif_vaudois_fonction(tree.diameter::double precision)) AS "volume tarif vaudois",
            sum(ksp_vd_surface_terriere(tree.diameter::double precision)) AS "surface terriere",
            avg(tree.diameter) AS "diametre moyen"
           FROM tree
          WHERE tree.diameter > 9
          GROUP BY tree.obs_id) subq
     FULL JOIN plot_obs ON subq.obs_id = plot_obs.id
     LEFT JOIN plot ON plot_obs.plot_id = plot.id
     LEFT JOIN density ON plot_obs.density_id = density.id;""",
        "DROP VIEW base_surf_vol_par_plotobs_diametre_9"),

        migrations.RunSQL("""
CREATE OR REPLACE VIEW web_homepage_municipality_diametre_9 AS 
 SELECT row_number() OVER () AS id,
    base_surf_vol_par_plotobs_diametre_9.protocol_id,
    base_surf_vol_par_plotobs_diametre_9.municipality_id,
    count(*) AS "nombre de placettes",
    avg(base_surf_vol_par_plotobs_diametre_9."nombre de tiges/hectare") AS "nombre de tiges (> 9cm)/hectare",
    avg(base_surf_vol_par_plotobs_diametre_9."volume (m3) tarif vaudois/hectare") AS "volume (m3) tarif vaudois/hectare",
    avg(base_surf_vol_par_plotobs_diametre_9."surface terriere (cm2)/hectare") AS "surface terriere (cm2) /hectare",
    avg(base_surf_vol_par_plotobs_diametre_9."diametre moyen") AS "diametre moyen (cm2)",
    avg(base_surf_vol_par_plotobs_diametre_9."volume de l'arbre moyen") AS "volume de l'arbre moyen (m3)",
    avg(base_surf_vol_par_plotobs_diametre_9."surface terriere de l'arbre moyen") AS "surface terriere de l'arbre moyen (cm2)",
    avg(base_surf_vol_par_plotobs_diametre_9."RPSTZ") * count(*)::double precision AS "surface forestiere theorique"
   FROM base_surf_vol_par_plotobs_diametre_9
  GROUP BY base_surf_vol_par_plotobs_diametre_9.municipality_id, base_surf_vol_par_plotobs_diametre_9.protocol_id;""",
        "DROP VIEW web_homepage_municipality_diametre_9"),

        migrations.RunSQL("""
CREATE OR REPLACE VIEW web_homepage_owner_diametre_9 AS 
 SELECT row_number() OVER () AS id,
    base_surf_vol_par_plotobs_diametre_9.protocol_id,
    base_surf_vol_par_plotobs_diametre_9.owner_id,
    count(*) AS "nombre de placettes",
    avg(base_surf_vol_par_plotobs_diametre_9."nombre de tiges/hectare") AS "nombre de tiges (> 9cm)/hectare",
    avg(base_surf_vol_par_plotobs_diametre_9."volume (m3) tarif vaudois/hectare") AS "volume (m3) tarif vaudois/hectare",
    avg(base_surf_vol_par_plotobs_diametre_9."surface terriere (cm2)/hectare") AS "surface terriere (cm2) /hectare",
    avg(base_surf_vol_par_plotobs_diametre_9."diametre moyen") AS "diametre moyen (cm2)",
    avg(base_surf_vol_par_plotobs_diametre_9."volume de l'arbre moyen") AS "volume de l'arbre moyen (m3)",
    avg(base_surf_vol_par_plotobs_diametre_9."surface terriere de l'arbre moyen") AS "surface terriere de l'arbre moyen (cm2)",
    avg(base_surf_vol_par_plotobs_diametre_9."RPSTZ") * count(*)::double precision AS "surface forestiere theorique"
   FROM base_surf_vol_par_plotobs_diametre_9
  GROUP BY base_surf_vol_par_plotobs_diametre_9.owner_id, base_surf_vol_par_plotobs_diametre_9.protocol_id;""",
        "DROP VIEW web_homepage_owner_diametre_9"),

        migrations.RunSQL("""
CREATE OR REPLACE VIEW base_vocabulary_values AS
 SELECT row_number() OVER () AS id,
    plot_obs.id AS plot_obs_id,
    protocol.vocab_posd6_id AS vocab_id,
    plot_obs.reserved3 AS value
   FROM plot_obs
     LEFT JOIN protocol ON plot_obs.protocol_id = protocol.id
  WHERE protocol.vocab_posd6_id IS NOT NULL
UNION
 SELECT row_number() OVER () AS id,
    plot_obs.id AS plot_obs_id,
    protocol.vocab_posd7_id AS vocab_id,
    plot_obs.reserved4 AS value
   FROM plot_obs
     LEFT JOIN protocol ON plot_obs.protocol_id = protocol.id
  WHERE protocol.vocab_posd7_id IS NOT NULL
UNION
 SELECT row_number() OVER () AS id,
    plot_obs.id AS plot_obs_id,
    protocol.vocab_posp8_id AS vocab_id,
    plot_obs.reserved1 AS value
   FROM plot_obs
     LEFT JOIN protocol ON plot_obs.protocol_id = protocol.id
  WHERE protocol.vocab_posp8_id IS NOT NULL
UNION
 SELECT row_number() OVER () AS id,
    plot_obs.id AS plot_obs_id,
    protocol.vocab_posp9_id AS vocab_id,
    plot_obs.reserved2 AS value
   FROM plot_obs
     LEFT JOIN protocol ON plot_obs.protocol_id = protocol.id
  WHERE protocol.vocab_posp9_id IS NOT NULL;""",
        "DROP VIEW base_vocabulary_values"),
    ]
