from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('municipality', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(error_messages={'unique': 'A user with that username already exists.'}, help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.', max_length=150, unique=True, validators=[django.contrib.auth.validators.UnicodeUsernameValidator()], verbose_name='username')),
                ('first_name', models.CharField(blank=True, max_length=150, verbose_name='first name')),
                ('last_name', models.CharField(blank=True, max_length=150, verbose_name='last name')),
                ('email', models.EmailField(blank=True, max_length=254, verbose_name='email address')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'db_table': 'auth_user',
            },
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='WebHomepageMunicipality',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('plot_numb', models.IntegerField(verbose_name='Nombre de placettes', db_column='nombre de placettes')),
                ('tree_numb_ha', models.FloatField(verbose_name='Nombre de tiges (> 16cm)/hectare', db_column='nombre de tiges (> 16cm)/hectare')),
                ('vol_vd_ha', models.FloatField(verbose_name='Volume (m3) tarif vaudois/hectare', db_column='volume (m3) tarif vaudois/hectare')),
                ('surf_terr_ha', models.FloatField(verbose_name='Surface terriere (cm2) /hectare', db_column='surface terriere (cm2) /hectare')),
                ('diam_mean', models.FloatField(verbose_name='Diametre moyen (cm2)', db_column='diametre moyen (cm2)')),
                ('vol_mean', models.FloatField(verbose_name="Volume de l'arbre moyen (m3)", db_column="volume de l'arbre moyen (m3)")),
                ('surf_terr_tree_mean', models.FloatField(verbose_name="Surface terriere de l'arbre moyen (cm2)", db_column="surface terriere de l'arbre moyen (cm2)")),
                ('surf_forest', models.FloatField(verbose_name='Surface forestiere theorique', db_column='surface forestiere theorique')),
            ],
            options={
                'db_table': 'web_homepage_municipality_diametre_16',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='WebHomepageOwner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('plot_numb', models.IntegerField(verbose_name='Nombre de placettes', db_column='nombre de placettes')),
                ('tree_numb_ha', models.FloatField(verbose_name='Nombre de tiges (> 16cm)/hectare', db_column='nombre de tiges (> 16cm)/hectare')),
                ('vol_vd_ha', models.FloatField(verbose_name='Volume (m3) tarif vaudois/hectare', db_column='volume (m3) tarif vaudois/hectare')),
                ('surf_terr_ha', models.FloatField(verbose_name='Surface terriere (cm2) /hectare', db_column='surface terriere (cm2) /hectare')),
                ('diam_mean', models.FloatField(verbose_name='Diametre moyen (cm2)', db_column='diametre moyen (cm2)')),
                ('vol_mean', models.FloatField(verbose_name="Volume de l'arbre moyen (m3)", db_column="volume de l'arbre moyen (m3)")),
                ('surf_terr_tree_mean', models.FloatField(verbose_name="Surface terriere de l'arbre moyen (cm2)", db_column="surface terriere de l'arbre moyen (cm2)")),
                ('surf_forest', models.FloatField(verbose_name='Surface forestiere theorique', db_column='surface forestiere theorique')),
            ],
            options={
                'db_table': 'web_homepage_owner_diametre_16',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Density',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('code', models.SmallIntegerField()),
            ],
            options={
                'db_table': 'density',
                'verbose_name': 'densit\xe9',
            },
        ),
        migrations.CreateModel(
            name='Intervention',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('default_pos', models.SmallIntegerField(help_text='Position par d\xe9faut (0-9) de cette intervention sur la fiche Ipasmofix', unique=True, null=True, blank=True)),
            ],
            options={
                'ordering': ('name',),
                'db_table': 'intervention',
            },
        ),
        migrations.CreateModel(
            name='Owner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, blank=True)),
                ('num', models.SmallIntegerField(help_text='Position Ipasmofix')),
            ],
            options={
                'db_table': 'owner',
                'verbose_name': 'propri\xe9taire',
            },
        ),
        migrations.CreateModel(
            name='OwnerType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('description', models.TextField(blank=True)),
                ('num_code', models.SmallIntegerField()),
                ('alpha_code', models.CharField(max_length=2)),
            ],
            options={
                'db_table': 'owner_type',
                'verbose_name': 'type de propri\xe9taire',
            },
        ),
        migrations.CreateModel(
            name='Plot',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('center', django.contrib.gis.db.models.fields.PointField(srid=21781, verbose_name="Coordonnées")),
                ('municipality', models.ForeignKey(blank=True, on_delete=models.PROTECT, to='municipality.Municipality', null=True)),
            ],
            options={
                'db_table': 'plot',
                'verbose_name': 'placette',
            },
        ),
        migrations.CreateModel(
            name='PlotObs',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('uid', models.IntegerField(unique=True, null=True, blank=True)),
                ('radius', models.SmallIntegerField(verbose_name="Rayon de placette")),
                ('slope', models.SmallIntegerField(verbose_name="Pente")),
                ('ser_num', models.SmallIntegerField(default=0, verbose_name="N° série")),
                ('div_num', models.SmallIntegerField(default=0, verbose_name="N° division")),
                ('main_nature', models.SmallIntegerField(verbose_name="Nature principale de la placette")),
                ('main_perc', models.SmallIntegerField(verbose_name="% nature principale")),
                ('reserved1', models.CharField(default='', max_length=1, blank=True, verbose_name="Champ libre 1")),
                ('reserved2', models.CharField(default='', max_length=1, blank=True, verbose_name="Champ libre 2")),
                ('unnumbered', models.SmallIntegerField(verbose_name="Tiges non dénombrées")),
                ('conifer', models.SmallIntegerField(verbose_name="% rés. non dénombrés")),
                ('interv_imp', models.SmallIntegerField(null=True, blank=True, verbose_name="Code intervention")),
                ('reserved3', models.CharField(default='', max_length=1, blank=True, verbose_name="Champ libre 3")),
                ('reserved4', models.CharField(default='', max_length=1, blank=True, verbose_name="Champ libre 4")),
                ('gibi_epicea', models.SmallIntegerField(verbose_name="Nombre total d’épicéas")),
                ('gibi_epicea_f', models.SmallIntegerField(verbose_name="Proportion d’épicéa frotté")),
                ('gibi_epicea_a', models.SmallIntegerField(verbose_name="Proportion d’épicéa abroutis")),
                ('gibi_conifer', models.SmallIntegerField(verbose_name="Nombre total d’autres résineux")),
                ('gibi_conifer_f', models.SmallIntegerField(verbose_name="Proportion d’autres résineux frotté")),
                ('gibi_conifer_a', models.SmallIntegerField(verbose_name="Proportion d’autres résineux abroutis")),
                ('gibi_leaved', models.SmallIntegerField(verbose_name="Nombre total de feuillus")),
                ('gibi_leaved_f', models.SmallIntegerField(verbose_name="Proportion de feuillus frotté")),
                ('gibi_leaved_a', models.SmallIntegerField(verbose_name="Proportion de feuillus abroutis")),
                ('density', models.ForeignKey(to='inventory.Density', on_delete=models.PROTECT, verbose_name="Densité")),
                ('interv_int', models.ForeignKey(to='inventory.Intervention', on_delete=models.PROTECT, null=True, verbose_name="Intervention")),
                ('owner', models.ForeignKey(to='inventory.Owner', on_delete=models.PROTECT, verbose_name="Propriétaire")),
                ('plot', models.ForeignKey(to='inventory.Plot', on_delete=models.PROTECT, verbose_name="Placette")),
            ],
            options={
                'db_table': 'plot_obs',
                'verbose_name': 'Observation sur placette',
                'verbose_name_plural': 'Observations sur placette',
            },
        ),
        migrations.CreateModel(
            name='Protocol',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pfile', models.FileField(upload_to='protocols')),
                ('year', models.SmallIntegerField(verbose_name="Ann\xe9e d'inventaire")),
                ('inv_date', models.CharField(max_length=200, verbose_name='Période d’inventaire')),
                ('comments', models.TextField(verbose_name='Commentaires', blank=True)),
                ('interv_0', models.ForeignKey(related_name='+', blank=True, to='inventory.Intervention', null=True, on_delete=models.PROTECT)),
                ('interv_1', models.ForeignKey(related_name='+', blank=True, to='inventory.Intervention', null=True, on_delete=models.PROTECT)),
                ('interv_2', models.ForeignKey(related_name='+', blank=True, to='inventory.Intervention', null=True, on_delete=models.PROTECT)),
                ('interv_3', models.ForeignKey(related_name='+', blank=True, to='inventory.Intervention', null=True, on_delete=models.PROTECT)),
                ('interv_4', models.ForeignKey(related_name='+', blank=True, to='inventory.Intervention', null=True, on_delete=models.PROTECT)),
                ('interv_5', models.ForeignKey(related_name='+', blank=True, to='inventory.Intervention', null=True, on_delete=models.PROTECT)),
                ('interv_6', models.ForeignKey(related_name='+', blank=True, to='inventory.Intervention', null=True, on_delete=models.PROTECT)),
                ('interv_7', models.ForeignKey(related_name='+', blank=True, to='inventory.Intervention', null=True, on_delete=models.PROTECT)),
                ('interv_8', models.ForeignKey(related_name='+', blank=True, to='inventory.Intervention', null=True, on_delete=models.PROTECT)),
                ('interv_9', models.ForeignKey(related_name='+', blank=True, to='inventory.Intervention', null=True, on_delete=models.PROTECT)),
                ('owners', models.ManyToManyField(blank=True, to='inventory.Owner', verbose_name='Propri\xe9taires')),
            ],
            options={
                'db_table': 'protocol',
                'verbose_name': 'protocole',
            },
        ),
        migrations.CreateModel(
            name='Tree',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('diameter', models.SmallIntegerField(verbose_name="Diamètre")),
                ('quality', models.CharField(max_length=2, blank=True, choices=[('Q0', 'Q0'), ('Q1', 'Q1'), ('Q2', 'Q2'), ('Q3', 'Q3'), ('Q4', 'Q4'), ('Q5', 'Q5'), ('Q6', 'Q6'), ('Qn', 'Qn'), ('Q-', 'Q-'), ('Q+', 'Q+')], verbose_name='Qualité')),
                ('obs', models.ForeignKey(to='inventory.PlotObs', on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 'tree',
                'verbose_name': 'arbre',
            },
        ),
        migrations.CreateModel(
            name='TreeSpecies',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('species', models.CharField(max_length=100)),
                ('code', models.CharField(help_text='Laissez ce champ vide si la position est E ou F', max_length=1, blank=True)),
                ('abbrev', models.CharField(max_length=4, blank=True)),
            ],
            options={
                'ordering': ('species',),
                'db_table': 'tree_spec',
                'verbose_name': 'essence',
            },
        ),
        migrations.CreateModel(
            name='Vocabulary',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=250)),
                ('comment', models.TextField(blank=True)),
            ],
            options={
                'ordering': ('name',),
                'db_table': 'vocabulary',
                'verbose_name': 'vocabulaire',
            },
        ),
        migrations.CreateModel(
            name='VocabValue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=1)),
                ('label', models.CharField(max_length=100)),
                ('vocab', models.ForeignKey(to='inventory.Vocabulary', on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 'vocabvalue',
                'verbose_name': 'valeur de vocabulaire',
            },
        ),
        migrations.AddField(
            model_name='tree',
            name='spec',
            field=models.ForeignKey(to='inventory.TreeSpecies', on_delete=models.PROTECT),
        ),
        migrations.AddField(
            model_name='protocol',
            name='spec_posE',
            field=models.ForeignKey(related_name='+', blank=True, to='inventory.TreeSpecies', on_delete=models.PROTECT, max_length=150, null=True, verbose_name='Esp\xe8ce en position E'),
        ),
        migrations.AddField(
            model_name='protocol',
            name='spec_posF',
            field=models.ForeignKey(related_name='+', blank=True, to='inventory.TreeSpecies', on_delete=models.PROTECT, max_length=150, null=True, verbose_name='Esp\xe8ce en position F'),
        ),
        migrations.AddField(
            model_name='protocol',
            name='vocab_posd6',
            field=models.ForeignKey(related_name='+', blank=True, to='inventory.Vocabulary', on_delete=models.PROTECT, help_text='Vocabulaire du champ libre 3 (DESP, position 6)', null=True, verbose_name='Champ libre 3'),
        ),
        migrations.AddField(
            model_name='protocol',
            name='vocab_posd7',
            field=models.ForeignKey(related_name='+', blank=True, to='inventory.Vocabulary', on_delete=models.PROTECT, help_text='Vocabulaire du champ libre 4 (DESP, position 7)', null=True, verbose_name='Champ libre 4'),
        ),
        migrations.AddField(
            model_name='protocol',
            name='vocab_posp8',
            field=models.ForeignKey(related_name='+', blank=True, to='inventory.Vocabulary', on_delete=models.PROTECT, help_text='Vocabulaire du champ libre 1 (PARC, position 8)', null=True, verbose_name='Champ libre 1'),
        ),
        migrations.AddField(
            model_name='protocol',
            name='vocab_posp9',
            field=models.ForeignKey(related_name='+', blank=True, to='inventory.Vocabulary', on_delete=models.PROTECT, help_text='Vocabulaire du champ libre 2 (PARC, position 9)', null=True, verbose_name='Champ libre 2'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='protocol',
            field=models.ForeignKey(to='inventory.Protocol', null=True, on_delete=models.PROTECT),
        ),
        migrations.AddField(
            model_name='owner',
            name='typ',
            field=models.ForeignKey(to='inventory.OwnerType', db_column='type_id', on_delete=models.PROTECT),
        ),
        migrations.AlterUniqueTogether(
            name='owner',
            unique_together=set([('typ', 'num')]),
        ),
    ]
