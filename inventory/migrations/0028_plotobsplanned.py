from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0027_treespecies_typ'),
    ]

    operations = [
        migrations.CreateModel(
            name='PlotObsPlanned',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ser_num', models.PositiveSmallIntegerField(default=0, null=True, verbose_name='N° série')),
                ('div_num', models.PositiveSmallIntegerField(default=0, null=True, verbose_name='N° division')),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='inventory.owner', verbose_name='Propriétaire')),
                ('plot', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='inventory.plot', verbose_name='Placette')),
                ('protocol', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='inventory.protocol')),
            ],
            options={
                'verbose_name': 'Placette à inventorier',
                'verbose_name_plural': 'Placettes à inventorier',
                'db_table': 'plot_obs_planned',
                'constraints': [models.UniqueConstraint(fields=('plot', 'protocol'), name='unique_plot_protocol')],
            },
        ),
    ]
