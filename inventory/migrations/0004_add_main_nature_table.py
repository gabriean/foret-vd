# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0002_add_fileimport_fk'),
    ]

    operations = [
        migrations.CreateModel(
            name='Nature',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=255)),
                ('woods', models.BooleanField(default=True, verbose_name='Boisé')),
            ],
            options={
                'ordering': ('code',),
                'db_table': 'nature',
            },
        ),
        migrations.AddField(
            model_name='plotobs',
            name='main_nature_id',
            field=models.ForeignKey(to='inventory.Nature', db_column='main_nature_id', on_delete=django.db.models.deletion.PROTECT, null=True, verbose_name="Nature principale de la placette"),
        ),
    ]
