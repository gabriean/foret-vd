# Generated by Django 3.1.1 on 2021-01-25 16:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0032_srid_21781_to_2056'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='phone',
            field=models.CharField(blank=True, max_length=40, verbose_name='Téléphone'),
        ),
    ]
