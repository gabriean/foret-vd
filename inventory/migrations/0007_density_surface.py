# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0006_remove_old_main_nature'),
    ]

    operations = [
        migrations.AddField(
            model_name='density',
            name='surface',
            field=models.IntegerField(verbose_name='Surface représentée en m2', default=0),
            preserve_default=False,
        ),
    ]
