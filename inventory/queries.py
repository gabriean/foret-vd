from collections import namedtuple, OrderedDict

from django.db.models import Avg, Count, F, IntegerField, Sum
from django.db.models.functions import Cast

from municipality.models import Arrondissement, Municipality
from .models import (
    BaseDbViewDiametre9, BaseDbViewDiametre16, Owner, VocabularyValue
)

Field = namedtuple('Field', ['name', 'accessor', 'fkey_target', 'vname'])

# Fields not accessible on the base view/model itself
ext_fields = {
    'protocol__year': Field(name='protocol__year', accessor='protocol', fkey_target='__year', vname='Année'),
    'plot__center': Field(name='plot__center', accessor='plot', fkey_target='__center', vname='Coordonnées'),
    'plot__municipality': Field(name='plot__municipality', accessor='plot__municipality', fkey_target='__name', vname='Commune'),
    'vocab_value': Field(name='vocab_value', accessor='vocab_value', fkey_target='', vname='Valeur'),
}


class Query:
    # Mapping to get the 'interesting' field on a foreign key join
    fkey_map = {
        'municipality': ('name', 'Commune'),
        'plot': ('id', 'Placette'),
        'center': ('', 'Coordonnées'),
        'owner': ('num', 'Propriétaire'),
        'protocol': ('pfile', 'Protocole'),
        'interv_int': ('name', 'Intervention'),
        'main_nature': ('description', 'Nature principale'),
        'year': ('', 'Année'),
    }

    def as_dict(self):
        return {key: getattr(self, key, '') for key in [
            'model', 'fields', 'annot_map', 'groupables'
        ]}

    def get_fkey_target(self, fname):
        name = fname.split('__')[-1]
        if name in self.fkey_map and self.fkey_map[name][0]:
            return '__%s' % self.fkey_map[name][0]
        return ''

    def get_fkey_verbose(self, fname):
        return self.fkey_map.get(fname, ('', fname))[1]

    def base_qs(self):
        return self.model.objects.all()

    def build_query(self, perims, aggrs, stddev=False):
        context = {}
        display_field_names = self.fields[:]
        query = self.base_qs()

        # Filter with perimeter selection
        if perims['municips']:
            query = query.filter(**{f'{self.plot_accessor}municipality__pk__in': perims['municips']})
            context['municipalities'] = Municipality.objects.filter(pk__in=perims['municips']).values_list('name', flat=True)
        if perims['owners']:
            query = query.filter(**{f'{self.plot_obs_accessor}owner__pk__in': perims['owners']})
            context['owners'] = Owner.objects.filter(pk__in=perims['owners'])
        if perims['arrs']:
            query = query.filter(**{f'{self.plot_accessor}municipality__arrond__pk__in': perims['arrs']})
            context['arronds'] = Arrondissement.objects.filter(pk__in=perims['arrs'])

        if 'municip-owner' in aggrs:
            replace_by = 'plot__municipality' if perims['municips'] else 'owner'
            idx = aggrs.index('municip-owner')
            aggrs.remove('municip-owner')
            aggrs.insert(idx, f'{self.plot_obs_accessor}{replace_by}')
            for fname in display_field_names:
                if fname.endswith('municipality'):
                    idx = display_field_names.index(fname)
                    display_field_names.remove(fname)
                    display_field_names.insert(idx, f'{self.plot_obs_accessor}{replace_by}')
                    break

        # Choose fields to display
        fields = OrderedDict()
        for fname in display_field_names + aggrs:
            if '__' in fname:
                parts = fname.rsplit('__', 1)
                fields[fname] = Field(name=fname, accessor=parts[0],
                                      fkey_target='__%s%s' % (parts[1], self.get_fkey_target(fname)),
                                      vname=self.get_fkey_verbose(parts[1]))
            else:
                f = self.model._meta.get_field(fname)
                fields[fname] = Field(f.name, f.name, self.get_fkey_target(f.name), f.verbose_name)

        if aggrs:
            if 'protocol__year' not in aggrs:
                # Always add year, as grouping and mixing years makes no sense
                if 'value' in aggrs:
                    aggrs.insert(aggrs.index('value'), 'protocol__year')
                else:
                    aggrs.append('protocol__year')
            if self.model == VocabularyValue and 'value' not in aggrs:
                # For the vocabulary queries, always group by 'value' if there is some grouping
                aggrs.append('value')
            all_aggr_fields = self.annot_map
            field_names = []
            aggr_crits = []
            for fname in aggrs:
                if fname == 'protocol__year':
                    aggr_crits.append(f'{self.plot_obs_accessor}protocol__year')
                    field_names.append('*Année')
                    continue
                if fname not in fields:
                    for f in fields:
                        if f.endswith(fname):
                            fname = fields[f].name
                aggr_crits.append('%s%s' % (fname, self.get_fkey_target(fname)))
                field_names.append('*%s' % fields[fname].vname)

            annot_list = []
            for fname, ftuple in fields.items():
                if fname in all_aggr_fields: # and fname not in aggrs:
                    if fname == 'value':
                        # 'value' is a CharField(1) and can be cast to int in this case.
                        # Mandatory for computing an average
                        target = Cast(F('value'), output_field=IntegerField())
                        annot_alias = '%s__%s' % (fname, all_aggr_fields[fname].name.lower())
                    else:
                        target = fields[fname].accessor
                        annot_alias = None
                    annot_list.append(all_aggr_fields[fname](target))
                    if annot_alias:
                        annot_list[-1].source_expressions[0].name = annot_alias
                    vname = fields[fname].vname
                    if all_aggr_fields[fname].name.lower() == 'count':
                        vname += " (compte)"
                    elif all_aggr_fields[fname].name.lower() == 'avg':
                        vname += " (moyenne)"
                    field_names.append(vname)
            # Ajout nombres de placettes et de tiges
            if 'plot_obs' in fields:
                annot_list.append(Count(fields['plot_obs'].name, distinct=True))
                field_names.append(fields['plot_obs'].vname)
            if 'nb_tree' in fields:
                annot_list.append(Sum(fields['nb_tree'].name))
                field_names.append(fields['nb_tree'].vname)

            context['query'] = query.values_list(*aggr_crits).annotate(*annot_list).order_by(*aggr_crits)
            context['field_names'] = field_names
        else:
            #order_by = self.model_data['order_by']
            field_list = [f.accessor + f.fkey_target for f in fields.values()]
            context['query'] = query.values_list(*field_list) #.order_by(order_by)
            context['field_names'] = [f.vname for f in fields.values()]  # Extract verbose names
        return context


class VocabQuery(Query):
    model = VocabularyValue
    plot_obs_accessor = 'plot_obs__'
    plot_accessor = 'plot_obs__plot__'
    fields = ['plot_obs', 'plot_obs__plot__municipality', 'plot_obs__plot', 'plot_obs__protocol__year', 'value']
    annot_map = {'value': Count}
    groupables = ['value']

    def __init__(self, vocab):
        self.vocab = vocab

    def base_qs(self):
        return VocabularyValue.objects.filter(vocab__name=self.vocab)


class Nbtiges16Query(Query):
    model = BaseDbViewDiametre16
    plot_obs_accessor = ''
    plot_accessor = 'plot__'
    fields = ['plot_obs', 'plot_obs__uid', 'municipality', 'plot', 'plot__center', 'protocol__year',
              'nb_tree', 'nb_tree_ha', 'volume_ha', 'surface_ha']
    annot_map = {'nb_tree_ha': Avg, 'volume_ha': Avg, 'surface_ha': Avg}
    groupables = ['interv_int', 'main_nature']


class Nbtiges9Query(Nbtiges16Query):
    model = BaseDbViewDiametre9


QUERIES = {
    'Nbtiges16': Nbtiges16Query(),
    'Nbtiges9': Nbtiges9Query(),
    'BoisMort': VocabQuery('Bois mort'),
    'Myrtilles': VocabQuery('Myrtilles'),
    'Cerf': VocabQuery('Écorcage du cerf'),
}
