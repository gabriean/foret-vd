from difflib import SequenceMatcher


def diff_lines(line1, line2, span_attrs='class="diff"'):
    sm = SequenceMatcher(a=line1, b=line2, autojunk=False)
    fb1 = fb2 = ''
    span = f'<span {span_attrs}>%s</span>'
    for tag, i1, i2, j1, j2 in sm.get_opcodes():
        if tag == 'equal':
            fb1 += line1[i1:i2]
            fb2 += line1[i1:i2]
        elif tag == 'replace':
            fb1 += span % line1[i1:i2]
            fb2 += span % line2[j1:j2]
        elif tag == 'delete':
            fb1 += span % line1[i1:i2]
        elif tag == 'insert':
            fb2 += span % line2[j1:j2]
    return [fb1, fb2]
