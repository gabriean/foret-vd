from datetime import date
import tempfile

from django import forms
from django.contrib.gis.forms import OSMWidget
from django.contrib.gis.gdal import DataSource
from django.contrib.gis.geos import MultiPolygon, Point, Polygon
from django.db import IntegrityError, transaction
from django.db.models import Q

from django_select2.forms import Select2MultipleWidget
from openpyxl import load_workbook

from municipality.models import Municipality
from .models import Intervention, Owner, Plot, PlotObsPlanned, Protocol, Topap
from .utils import diff_lines


class VDOSMWidget(OSMWidget):
    """
    An OpenStreetMap widget centered on Canton Vaud, in the case geoadmin is not available.
    """
    default_lon = 6.58
    default_lat = 46.55
    default_zoom = 10


class PassedProtocolForm(forms.ModelForm):
    class Meta:
        model = Protocol
        exclude = ['passed', 'default_density', 'geom']
        widgets = {
            'owners': Select2MultipleWidget,
        }
        labels = {
            'draft': 'En cours de report du PDF',
        }

    def __init__(self, *args, **kwargs):
        # Set initial defaults for Intervention
        interv_vals = set([getattr(kwargs['instance'], f'interv_{idx}', None) for idx in range(10)])
        if not kwargs['instance'] or interv_vals == {None}:
            for idx in range(10):
                if not 'interv_%d' % idx in kwargs['initial']:
                    kwargs['initial']['interv_%d' % idx] = Intervention.objects.filter(default_pos=idx).first()
        super().__init__(*args, **kwargs)
        self.fields['owners'].queryset = Owner.objects.order_by('typ__alpha_code', 'num')

    def clean_year(self):
        year = self.cleaned_data['year']
        if 1950 <= year <= date.today().year:
            return year
        else:
            raise forms.ValidationError("L'année doit être entre 1950 et %d" % date.today().year)


class FutureProtocolForm(forms.ModelForm):
    class Meta:
        model = Protocol
        fields = ['name', 'year', 'default_density', 'inv_date', 'comments']


class PlotObsPlannedForm(forms.ModelForm):
    class Meta:
        model = PlotObsPlanned
        fields = ['owner', 'ser_num', 'div_num', 'operator']

    def __init__(self, *args, protocol=None, **kwargs):
        super().__init__( *args, **kwargs)
        if 'instance' in kwargs:
            self.fields['owner'].queryset = Owner.objects.filter(
                Q(pk__in=kwargs['instance'].protocol.owners.all()) | Q(pk=kwargs['instance'].owner_id)
            )
        else:
            # Empty form for multi-edition
            self.fields['owner'].queryset = protocol.owners.all()
            self.fields['owner'].required = False
            self.fields['ser_num'].initial = None
            self.fields['div_num'].initial = None


class ImportPlotsForm(forms.Form):
    fichier = forms.FileField(label="Fichier à importer")

    def import_plots(self, protocol):
        prot_owners = list(protocol.owners.all())
        wb = load_workbook(self.cleaned_data['fichier'])
        ws = wb.active
        headers = [cell.value for cell in next(ws.rows)]
        imported = already = 0
        for line_no, line in enumerate(ws.rows, start=1):
            if line_no == 1:
                continue  # headers
            values = {headers[idx]: cell.value for idx, cell in enumerate(line)}
            center = Point(values['CoordX'], values['CoordY'], srid=2056)
            try:
                plot = Plot.objects.get(center=center)
            except Plot.DoesNotExist:
                plot = Plot.objects.create(
                    center=center, municipality=Municipality.get_from_point(center)
                )
            owner_code = values['LvPropri_1'] or values['LvPropri']
            owner = Owner.get_by_code(owner_code) if owner_code else None
            ser_div = values['top_SerieD'] or values['ame_SerieD']
            ser_num = ser_div // 100 if ser_div else None
            div_num = ser_div % 100 if ser_div else None
            try:
                with transaction.atomic():
                    PlotObsPlanned.objects.create(
                        plot=plot, protocol=protocol, owner=owner, ser_num=ser_num, div_num=div_num
                    )
            except IntegrityError:
                already += 1
            else:
                imported += 1
                if owner not in prot_owners:
                    protocol.owners.add(owner)
                    prot_owners.append(owner)

        return {'imported': imported, 'already': already}


class FBaseForm(forms.Form):
    fichier1 = forms.FileField(required=True)
    fichier2 = forms.FileField(required=True)

    def check_fbases(self):
        lines1 = [to_str(line) for line in self.cleaned_data['fichier1'].readlines()]
        lines2 = [to_str(line) for line in self.cleaned_data['fichier2'].readlines()]
        identical = 0
        different = []
        ignored_extras = abs(len(lines1) - len(lines2))
        for idx, line1 in enumerate(lines1):
            try:
                line2 = lines2[idx]
            except IndexError:
                break
            if line1 == line2:
                identical += 1
                continue
            different.append(diff_lines(line1, lines2[idx], span_attrs='style="color:red; font-family:monospace"'))
        return {'identical': identical, 'different': different, 'ignored': ignored_extras}


class TopapForm(forms.Form):
    fichier = forms.FileField(required=True)

    def import_topap(self):
        from zipfile import ZipFile

        fichier = self.cleaned_data['fichier']
        with tempfile.TemporaryDirectory() as tmpdirname:
            with ZipFile(fichier, 'r') as zipObj:
                zipObj.extractall(tmpdirname)
            ds = DataSource(tmpdirname + '/TOP_CE_PARCELLE.shp')
            layer = ds[0]
            field_mapping = {f.db_column: f.name for f in Topap._meta.get_fields() if f.db_column}
            with transaction.atomic():
                Topap.objects.all().delete()
                for feature in layer:
                    line = Topap()
                    if isinstance(feature.geom.geos, Polygon):
                        line.polygon = MultiPolygon(feature.geom.geos)
                    else:
                        line.polygon = feature.geom.wkt
                    for field_name in layer.fields:
                        value = feature.get(field_name)
                        if value is None and Topap._meta.get_field(field_mapping[field_name]).get_internal_type() == 'CharField':
                            value = ''
                        setattr(line, field_mapping[field_name], value)
                    line.save()


def to_str(bin_txt):
    try:
        return bin_txt.decode('utf-8')
    except UnicodeDecodeError:
        return bin_txt.decode('latin-1')
