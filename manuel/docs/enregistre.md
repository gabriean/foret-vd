# Séquence d'enregistrement d'une placette

Déplacement sur le terrain et fixation du centre de la placette.

Cette opération s'effectue au pas et par visées successives les plus longues possibles et très précises à la boussole. Des corrections peuvent être apportées sur les longueurs et les azimuts à l'aide de la table des rayons pentes et des repères fournis par les points de repères fiables sur la carte.

Le centre de la placette est matérialisé par la cheville métallique fichée en terre, ceci, sans idée préconçue et sans considération de la nature et du peuplement en présence! Surtout, ne jamais déplacer le centre à 10 ou 15 mètres dans un endroit plus aisé à inventorier.

Sur la carte de terrain, chaque placette est numérotée de 1 à x (nombre de placettes correspondant au mandat) avec le sens (flèche) de déplacement (voir exemple en annexe).

## Mise en place du premier point de prélèvement (mise en place de la première placette)

- Ouvrir le site <https://mobile.inv-vd.ch> et l'inventaire correspondant (selon [les instructions](instructions.md#application))
- Commencer à un point clairement identifiable (sur la carte ou sur une photo aérienne) 
- Calculer l'azimut de la distance avec le bouton «les mesures» (voir [Page 1](instructions.md#page-1-se-connecter-et-selectionner-linventaire), bouton 4)
    - cliquer sur le lieu où vous vous trouvez
    - cliquer sur votre cible (placette)
- Utiliser l'azimut et la distance puis utiliser la boussole pour s'approcher de la placette

## Démarrer l'enregistrement

- Sélectionner la placette (point bleu)
- Démarrer l'enregistrement (cliquer sur l'année (en rouge), puis sur «Commencer»)
- Remplir chaque étape selon les instructions

## Étape 1

À cette étape, il suffit de vérifier que les données sont correctes. Si ce n'est pas le cas, indiquer les erreurs repérées dans les commentaires.

## Étape 2

### Pente

### Nature principale de la placette

### Proportion de la nature principale

Proportion de la surface de la placette occupée par la nature principale.

### Rayon

## Étape 3

### Tiges non dénombrées

Tiges d'avenir non dénombrées, nombre approximatif (en dizaines) de tiges de 30 à 40 cm de hauteur au minimum et de diamètre inférieur au seuil d'inventaire de 9.5 cm (1 position).

### % rés. non dénombrés

Proportion des tiges résineuses (en dixième) par rapport au nombre total des tiges comptées (1 position)

### Ecorçage du cerf

### Bois mort

## Étape 4 – Dégats du gibier

## Étape 5 – Coordonnées réelles

## Étape 6 – Remarques

D'éventuelles remarques saisies à l'étape 1 apparaîtront de nouveau ici. Ne pas les effacer.
Vous pouvez saisir d'autres remarques liées à l'inventaire de cette placette.

## Mesure des tiges à dénombrer

### Avec la pince Bluetooth

- Connecter la pince à la tablette (selon [les instructions](instructions.md#couplage-entre-application-et-pince-bluetooth))
- Le boîtier sur la pince doit se trouver directement en mode d'entrée
- Choisir l'essence sur le boîtier de la pince
- Pincer la tige à 1,30 m du sol (sur la pente, toujours en amont).
Maintenir les bras du compas parfaitement horizontaux, toujours orienter la tringle du compas vers le centre de la placette. Fermer le compas délicatement, le maintenir en tangence sans pression contre le tronc et valider. Confirmer avec la touche d'entrée (rouge) de la pince. La condition pour cela est que le terminal de données soit attaché à la pince avec le périphérique approprié.
- La mesure est transmise directement à la tablette
- Une entrée incorrecte peut être supprimée de la tablette (voir [les instructions](instructions.md#page-5-la-derniere-etape-de-linventaire), bouton 4)

### Manuellement

- Ajouter une tige à mesurer (voir [les instructions](instructions.md#page-5-la-derniere-etape-de-linventaire), bouton 2)
- Choisir l'essence, puis le diamètre ou la circonférence 
- Confirmer, ajouter une autre tige à mesurer ou supprimer une tige

## Contrôle et terminer
- Vérifier la liste des tiges et finir l'enregistrement avec le bouton «Terminer» (voir [les instructions](instructions.md#page-5-la-derniere-etape-de-linventaire), bouton 5)

Si une connexion à Internet existe, l'entrée est stockée directement dans la base de données du serveur

Si vous travaillez hors ligne, l'entrée est stockée localement dans une mémoire cache de la tablette. Le nombre en position 7 sur [l'écran](instructions.md#page-1-se-connecter-et-selectionner-linventaire) est incrémenté de  1 pour indiquer que des données non synchronisées avec le serveur sont présentes sur la tablette. **Attention: Ne pas supprimer le cache du navigateur, car les données seraient alors irrémédiablement perdues**.
