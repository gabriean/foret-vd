# Préparation de l'inventaire forestier [DGE]

<mark>La préparation de l'inventaire ainsi que la vérification se font à l'aide de la plateforme
inv-vd.ch.</mark>

<mark>L’importation d’un inventaire est classé par triage.</mark>

<mark>Par exemple : Inventaires 2020 classé par Triage 55 et triage 113.</mark>

Avant tout inventaire, le responsable des inventaires soumet aux gestionnaires les
points suivants.

## Délimitation du périmètre d'aménagement et d'inventaire

Le périmètre d'inventaire englobe les surfaces boisées et parfois non boisées (les pelouses des pâturages boisés ne sont pas inventoriées), soumises à la législation forestière sises à l'intérieur du périmètre qui font l'objet d'un inventaire forestier. Dans la plupart des cas, ce périmètre s'identifie au périmètre forestier aménagé. L'analyse quantitative de la sylve peut toutefois se limiter aux unités de gestion exploitées intensivement et normalement. Pour cela, l'aménagiste doit connaître à priori la division du périmètre aménagé en unités de gestion caractérisées par la nature et l'intensité de leur exploitation.

Le périmètre forestier aménagé d'un triage, d'une propriété ou série d'aménagement englobe toutes les natures boisées et parfois non boisées soumises (les pelouses des pâturages boisés ne sont pas inventoriées) à la législation forestière de ce bien-fonds et sur lesquelles le forestier exerce son contrôle.

La délimitation du périmètre d'inventaire et l'identification des placettes à inventorier sur la carte (plan d'ensemble) seront exécutées avec beaucoup de minutie. En cas de doute, seule la localisation lors du relevé de terrain décidera de l'inclusion d'une placette limite ou de son abandon. Dans tous les cas, la placette doit être saisie par l’opérateur et une annotation sera faite sur le listage lors de sa correction.

Règles valables pour l'identification des placettes à dénombrer et pour le comptage des surfaces de points:

1. sont comptés, respectivement inventoriés, tous les points d'intersection de la grille d'échantillonnage tombant à l'intérieur des limites fixées par le périmètre inventorié et ceci quelle que soit la nature sise dans la projection du point;
1. sont abandonnés tous les points d'intersection sis à l'extérieur du-dit périmètre;
1. les points situés sur la limite du périmètre seront retenus une fois sur deux, les "non retenus" seront attribués à la propriété mitoyenne;
1. les placettes dont le centre tombe à l'intérieur de la surface abornée des routes cantonales et des voies ferrées, sont abandonnées, cette surface étant hors périmètre inventorié.

Les placettes retenues, dont le centre est à l'intérieur ou à la limite du périmètre inventorié, mais dont la superficie déborde de ces limites, sont à dénombrer intégralement (à l'intérieur et à l'extérieur du périmètre fixé), sans s'occuper ni de la nature, ni du propriétaire.

## Contrôle du parcellaire

<mark>Les placettes saisies par le passé sont à disposition pour l'inventaire en préparation. Il y a la possibilité à la fois d'inclure dans l'inventaire de nouvelles placettes que d'en exclure des plots saisis par le passé.</mark>

Limites, types et numéros du propriétaire, des séries, divisions, secteurs (ou groupes de divisions).

Lors de l'importation du fichier «nuage de points» ces informations sont génerer de l'outils inv-vd.ch selon la démarche suivantes:

 - ...

## Fixation du nombre des tirages «topo-parcellaires»

Préparation d'un exemplaire de l'inventaire sur du papier "terrain" (pour les besoins de l'inventaire).

Il est possible d'intégrer les placettes de la base de données directement dans un SIG de bureau.

Manuel pour ArcGIS: (https://desktop.arcgis.com/fr/arcmap/latest/manage-data/gdbs-in-postgresql/connect-postgresql.htm)

    Connection base de données (postgresql):
    host:	inv-vd.ch
    Port: 	5477
    DB: 	??
    user: 	myuser
    pw: 	mypassword


## Établissement du protocole d'inventaire

Le protocole s'établit entre l'inspecteur forestier, le garde forestier et le responsable cantonal des inventaires:

1. densité d'échantillonnage (depuis 2008 toujours 1 placette par hectare)
2. appréciation du rajeunissement (tiges non dénombrées qui sont des tiges d'avenir)
3. mesures des arbres "martelés" ou non
4. carte et types d'interventions
5. champ libre
6. liste des essences „feuillus“, possibilité d'ajouter deux feuillus
7. maître d'oeuvre pilote défini (adresse de facturation et versement de subventions)
8. détails pour fournir les tableaux de résultats
9. divers, etc.

Les placettes ipasmofix qui font l'objet de ces instructions sont localisées sur une grille systématique carrée de 100 m de côté, à raison de 1 placette par ha, y compris sur les pâturages boisés (sauf les placettes en nature pelouse).

## Attribuer un périmetre à un mandataire

<mark>Au travers de la plateforme, les placettes peuvent être attribuées à un opérateur (mandataire).</mark>

<mark>La plateforme permet de visualiser l'avancement de chaque opérateur.</mark>

<mark>Un opérateur reçoit un accès individuel. Les informations suivantes sont saisies par opérateur : Nom, Prénom, adresse email, numéro de téléphone</mark>

## Charger la liste des essences sur la pince

La liste des essences peut être téléchargée sur la platforme à cet adresse: https://www.inv-vd.ch/essences/

<mark>Ensuite, connectez la pince à l’ordinateur et le fichier (SPCNAME.TXT) est copié dans le classeur ??? de la pince.</mark>
