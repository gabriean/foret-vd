# Matériel d'inventaire

## Objets à avoir sur soi
- Tablette avec les applications suivantes:
    - Navigateurs Chrome et Firefox
    - Boussole
    - Application Rega
- Smartphone pouvant être utilisé comme hot spot mobile
- Boussole Suunto 400 grades avec gravure millimétrique
- Clisimètre Suunto en degrés et pourcents (attention: ne pas utiliser 1 clisimètre en grades)
- Pince Bluetooth 80 cm avec boîtier de données (DPII)
- Ceinturon avec protège-ruban et cheville métallique pour fixer le centre de la placette
- Porte-craie + craies grasses (pour les peuplements très denses)
- Chemise ou veste avec de nombreuses poches ou une sacoche pour "caser" le petit matériel et éviter que les appareils de mesure s'entrechoquent
- une chevillière de 20 mètres 

## Objets à stocker dans le sac à dos

- Le présent guide d'inventaire forestier
- Une carte, un protocole, un crayon très gras et une gomme
- Une batterie externe pour tablette et téléphone portable, câble inclus
- Une protection contre la pluie
- Un kit d'urgence
- Un altimètre (en terrain à forte dénivellation)
- Des piles de rechange

## Objets à avoir dans la voiture

- Chargeur pour smartphone et tablette
