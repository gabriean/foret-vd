import json
from datetime import date
from decimal import Decimal
from pprint import pformat

from django.contrib.gis.db.models.functions import Transform
from django.core.exceptions import ValidationError
from django.core.mail import mail_admins
from django.db import transaction
from django.db.models import Case, Count, IntegerField, Prefetch, When
from django.forms import ChoiceField, ModelChoiceField
from django.http import HttpResponseForbidden, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import TemplateView, View

from inventory.models import Plot, PlotObs, PlotObsPlanned, Protocol, Tree, TreeSpecies
from .forms import PlotForm, POForm, TreeForm


class HttpResponseWithError(HttpResponseForbidden):
    def __init__(self, data, error):
        mail_admins(
            'Sync error on inv-vd.ch',
            ('%s\n\n'
             'Sent data:\n%s') % (error, pformat(data))
        )
        super().__init__(error)


class MainView(TemplateView):
    template_name = 'mobile/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        current_protocols = Protocol.objects.filter(year=date.today().year)
        vocabularies = set()
        for prot in current_protocols:
            vocabularies |= set(prot.get_vocabs())
        current_protocols.values_list('vocab_posp8', 'vocab_posp9', 'vocab_posd6', 'vocab_posd7')
        context.update({
            'inventories': current_protocols,
            'plot_form': PlotForm(),
            'po_form': POForm(opt_vocs=vocabularies),
            'tree_form': TreeForm(),
            'verbose_names': {
                field.name: field.verbose_name for field in PlotObs._meta.fields
            },
        })
        context['verbose_names']['year'] = 'Année'  # property
        return context


class InventoryListView(View):
    """This view returns a list of all accessible inventories."""
    def get(self, request, *args, **kwargs):
        current_protocols = Protocol.objects.filter(year=date.today().year)
        if not request.user.has_perm('inventory.change_protocol'):
            current_protocols = current_protocols.annotate(
                num_plots=Count(Case(
                    When(plotobsplanned__operator=request.user, then=1),
                    output_field=IntegerField(),
                ))
            ).filter(num_plots__gt=0)
        else:
            current_protocols = current_protocols.annotate(
                num_plots=Count('plotobsplanned')
            ).filter(num_plots__gt=0)
        return JsonResponse([
            {'pk': prot.pk,  'name': prot.name, 'url': reverse('inventory-geojson', args=[prot.pk])}
            for prot in current_protocols
        ], safe=False)


class InventoryView(View):
    """This view returns a GeoJSON with Inventory data."""
    def get(self, request, *args, **kwargs):
        inventory = get_object_or_404(Protocol, pk=kwargs['pk'])
        data = {
            "id": inventory.pk,
            "name": inventory.name,
            "center": inventory.center(srid=4326).coords,
            "owners": [(o.pk, o.title) for o in inventory.owners.all()],
            "vocab_ids": [
                voc.pk for voc in [
                    inventory.vocab_posp8, inventory.vocab_posp9, inventory.vocab_posd6, inventory.vocab_posd7
                ] if voc
            ],
            "plotsURL": reverse('inventory-plots', args=[inventory.pk]),
            "layerURLs": {
                #'BestandesKarte': reverse('afw-bestand', args=[inventory.pk]),
                #'Erschliessung': reverse('afw-erschliessung', args=[inventory.pk]),
            },
        }
        return JsonResponse(data)


class InventoryPlots(View):
    """
    Return all plots inside the target inventory geometry as a GeoJSON structure,
    with some properties pre-filled as per latest observation.
    """
    def get(self, request, *args, **kwargs):
        self.inventory = get_object_or_404(Protocol, pk=kwargs['pk'])
        # This is still fuzzy, as we don't know yet how inventory plots are defined.
        # By geometry with exceptions or (more probably) with a set of prepared points.
        if self.inventory.geom:
            return self.get_with_geom()
        else:
            return self.get_with_planned()

    def get_with_geom(self):
        geojson = {"type": "FeatureCollection", "features": []}
        first_plotobs = PlotObs.objects.order_by('plot_id', '-id').distinct('plot_id')
        plot_qs = Plot.objects.filter(center__within=self.inventory.geom
            ).annotate(center2056=Transform('center', 2056)).prefetch_related(
            Prefetch('plotobs_set', queryset=first_plotobs)
        )
        #if self.inventory.excluded_plots:
        #    plot_qs = plot_qs.exclude(pk__in=self.inventory.excluded_plots)
        density = self.inventory.default_density
        for plot in plot_qs:
            geojson["features"].append(plot.as_geojson(geom_field='center', srid=4326))
            props = geojson["features"][-1]["properties"]
            for year in props["obsURLs"].keys():
                props["obsURLs"][year] += '?srid=4326'
            municip = plot.municipality
            props["municipality_id"] = municip.pk if municip else ''
            props["municipality_name"] = str(municip) if municip else ''
            props["coords_2056"] = [
                int(round(c, 0)) for c in plot.center2056.coords
            ]
            if density:
                props["density"] = [density.pk, str(density)]
            try:
                latest_obs = plot.plotobs_set.all()[0]
            except IndexError:
                pass
            else:
                props["owner"] = [latest_obs.owner.pk, str(latest_obs.owner)]
                props["ser_num"] = latest_obs.ser_num
                props["div_num"] = latest_obs.division
                props["slope"] = latest_obs.slope
                props["radius"] = latest_obs.radius
        return JsonResponse(geojson)

    def get_with_planned(self):
        geojson = {"type": "FeatureCollection", "features": []}
        po_planned = self.inventory.plotobsplanned_set.all(
            ).annotate(center2056=Transform('plot__center', 2056)
            ).annotate(center4326=Transform('plot__center', 4326)
            ).select_related('plot', 'plot__municipality', 'owner'
            ).prefetch_related('plot__plotobs_set')
        density = self.inventory.default_density
        for po in po_planned:
            plot = po.plot
            plot.center4326 = po.center4326
            geojson["features"].append(plot.as_geojson(geom_field='center4326', srid=4326))
            props = geojson["features"][-1]["properties"]
            for year in props["obsURLs"].keys():
                props["obsURLs"][year] += '?srid=4326'
            municip = plot.municipality
            props["municipality_id"] = municip.pk if municip else ''
            props["municipality_name"] = str(municip) if municip else ''
            props["coords_2056"] = [
                int(round(c, 0)) for c in po.center2056.coords
            ]
            if density:
                props["density"] = [density.pk, str(density)]
            props["owner"] = [po.owner.pk, str(po.owner)]
            props["ser_num"] = po.ser_num
            props["div_num"] = po.division
            #props["slope"] = latest_obs.slope
            #props["radius"] = latest_obs.radius
        return JsonResponse(geojson)


class SyncView(View):
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body.decode('utf-8'))
        plotobs_props = data['features'][0]['properties']
        plot = get_object_or_404(Plot, pk=int(data['plot']))
        protocol = get_object_or_404(Protocol, pk=plotobs_props['protocol'])

        try:
            existing_plotobs = PlotObs.objects.get(plot=plot, protocol=protocol)
            # It's a new sync with existing data, overwrite old data with the new one
            plotobs = PlotObs(
                plot=existing_plotobs.plot, protocol=existing_plotobs.protocol,
                owner=existing_plotobs.owner, ser_num=existing_plotobs.ser_num,
                div_num=existing_plotobs.div_num, density=protocol.default_density
            )
            existing_plotobs.delete()
        except PlotObs.DoesNotExist:
            planned = get_object_or_404(PlotObsPlanned, plot=plot, protocol=protocol)
            plotobs = PlotObs(
                plot=plot, protocol=protocol, owner=planned.owner, ser_num=planned.ser_num,
                div_num=planned.div_num, density=protocol.default_density
            )

        plotobs_form = POForm(opt_vocs=protocol.get_vocabs(), instance=plotobs)

        # Check PlotObs properties
        errs = []
        # FIXME: decide if exact_long/exact_lat match a Plot.point_exact (to be created) field
        # or just save them on PlotObs?
        for key, value in plotobs_props.items():
            if (key in {'year', 'type', 'exact_long', 'exact_lat', 'sealevel', 'verif_step1'} or
                    key in plotobs_form.disabled_fields):
                continue
            if key.startswith('voc_'):
                # One of the optional vocabulary fields
                voc_id = int(key.split('_')[1])
                for obs_field, prot_field in (
                        ('reserved1', 'vocab_posp8'), ('reserved2', 'vocab_posp9'),
                        ('reserved3', 'vocab_posd6'), ('reserved4', 'vocab_posd7')):
                    if voc_id == getattr(protocol, f'{prot_field}_id'):
                        field_name = obs_field
                        value = value[0]
                        break
                else:
                    # This vocabulary is not referenced in the protocol
                    continue
            else:
                field_name = key
            ffield = plotobs_form[field_name].field
            if isinstance(ffield, ModelChoiceField):
                # Select choices are [pk, human-readable]
                field = plotobs._meta.get_field(field_name)
                pk_value = value[0] if isinstance(value, list) else value
                if pk_value == '':
                    continue
                try:
                    value = field.related_model.objects.get(pk=int(pk_value))
                except ValueError as err:
                    errs.append("%s=%s: %s" % (field_name, value, err))
                    continue
            elif isinstance(ffield, ChoiceField):
                value = value[0] if isinstance(value, list) else value
            if value != '':
                try:
                    setattr(plotobs, field_name, value)
                except ValueError as err:
                    errs.append(err)
        try:
            plotobs.full_clean()
        except ValidationError as err:
            errs.append(err)
        if errs:
            return HttpResponseWithError(data, "Errors: %s" % ", ".join([str(err) for err in errs]))

        with transaction.atomic():
            plotobs.save()
            # Save new TreeObs
            for tree_feature in data['features'][1:]:
                try:
                    self.save_treeobs(plotobs, tree_feature)
                except ValidationError as err:
                    errs.append(err)
            if errs:
                # Raising this error will rollback the transaction
                raise TreeDataError("Errors in observation data")

        if errs:
            return HttpResponseWithError(data, "Errors: %s" % ", ".join([str(err) for err in errs]))
        return JsonResponse({})

    def save_treeobs(self, plotobs, tree_feature):
        if not tree_feature['properties'].get('tree'):
            # New tree
            spec_id = tree_feature['properties']['spec'][0]
            if spec_id == '':
                raise ValidationError("The tree species is mandatory")
            spec = TreeSpecies.objects.get(pk=spec_id)
            tree = Tree.objects.create(
                obs=plotobs, spec=spec,
                diameter=tree_feature['properties']['diameter'],
            )
        else:
            tree = Tree.objects.get(pk=int(tree_feature['properties']['tree']))
