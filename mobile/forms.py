from django import forms

from inventory.models import Intervention, Plot, PlotObs, Tree


class PlotForm(forms.ModelForm):
    exact_long = forms.FloatField(label="Longitude X", required=False)
    exact_lat = forms.FloatField(label="Latitude Y", required=False)

    class Meta:
        model = Plot
        fields = ('exact_long', 'exact_lat', 'sealevel') #, 'exposition', 'slope')

    def save(self, *args, **kwargs):
        plot = super().save(*args, **kwargs)
        lon, lat = self.cleaned_data.get('exact_long'), self.cleaned_data.get('exact_lat')
        if lon and lat:
            plot.point_exact = Point(lon, lat, srid=2056)
            plot.save()
        return plot


class ChoiceField0_to_150(forms.ChoiceField):
    def __init__(self, *args, **kwargs):
        kwargs.pop('min_value')
        kwargs['choices'] = [('', '---')] + [(i, str(i)) for i in range(0, 160, 10)]
        super().__init__(*args, **kwargs)


class ChoiceField0_to_100p(forms.ChoiceField):
    def __init__(self, *args, **kwargs):
        kwargs.pop('min_value')
        kwargs['choices'] = [('', '---')] + [(i, str(i) + '%') for i in range(0, 110, 10)]
        super().__init__(*args, **kwargs)


class ChoiceField60_to_100p(forms.ChoiceField):
    def __init__(self, *args, **kwargs):
        kwargs.pop('min_value')
        kwargs['choices'] = [('', '---')] + [(i, str(i) + '%') for i in range(60, 110, 10)]
        super().__init__(*args, **kwargs)


class POForm(forms.ModelForm):
    radius = forms.IntegerField(label="Rayon", min_value=9, max_value=13)
    slope = forms.IntegerField(label="Pente", min_value=0, max_value=90)

    disabled_fields = ['protocol', 'density', 'owner', 'ser_num', 'div_num']

    class Meta:
        model = PlotObs
        fields = [
            'protocol', 'density', 'owner', 'ser_num', 'div_num',
            'slope', 'main_nature', 'main_perc', 'interv_int', 'radius',
             'reserved1', 'reserved2',
            'unnumbered', 'conifer', 'ecorcage', 'reserved3', 'reserved4',
            'gibi_epicea', 'gibi_epicea_f', 'gibi_epicea_a',
            'gibi_conifer', 'gibi_conifer_f', 'gibi_conifer_a',
            'gibi_leaved', 'gibi_leaved_f', 'gibi_leaved_a',
            'remarks',
        ]
        field_classes = {
            'main_perc': ChoiceField60_to_100p,
            'unnumbered': ChoiceField0_to_150,
            'conifer': ChoiceField0_to_100p,
            'ecorcage': ChoiceField0_to_100p,
            'gibi_epicea': ChoiceField0_to_150,
            'gibi_epicea_f': ChoiceField0_to_100p,
            'gibi_epicea_a': ChoiceField0_to_100p,
            'gibi_conifer': ChoiceField0_to_150,
            'gibi_conifer_f': ChoiceField0_to_100p,
            'gibi_conifer_a': ChoiceField0_to_100p,
            'gibi_leaved': ChoiceField0_to_150,
            'gibi_leaved_f': ChoiceField0_to_100p,
            'gibi_leaved_a': ChoiceField0_to_100p,
        }
        widgets = {
            'protocol': forms.HiddenInput,
        }
        labels = {
            'main_perc': 'Proportion de nature principale',
        }

    help_content = {
        'radius': {
            'title': "Aide sur le choix du rayon",
            'template': "mobile/rayon.html",
        },
        'unnumbered': {
            'title': "Aide sur les tiges non dénombrées",
            'template': "mobile/unnumbered.html",
        },
        'conifer': {
            'title': "Aide sur la proportion des tiges résineuses",
            'template': "mobile/conifer.html",
        },
    }
    units = {
        'slope': 'degrés',
        'radius': 'm',
        'ecorcage': '(dans 5m de rayon)',
    }

    def __init__(self, *args, opt_vocs=(), **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['interv_int'].queryset = Intervention.objects.filter(active=True).order_by('code')
        for field_name in self.disabled_fields:
            self.fields[field_name].disabled = True
        for field_name, field_help in self.help_content.items():
            self.fields[field_name].help_title = field_help['title']
            self.fields[field_name].help_template = field_help['template']
        for field_name, unit in self.units.items():
            self.fields[field_name].unit = unit
        for field_name in self.fields.keys():
            # All gibi fields are mandatory
            if field_name.startswith('gibi_'):
                self.fields[field_name].required = True
        # Dynamically create fields for all possible optional fields
        self.voc_fields = []
        for vocab in opt_vocs:
            if vocab.name == 'Écorçage du cerf':
                continue  # Now in PlotObs.ecorcage
            self.fields[f'voc_{vocab.pk}'] = forms.ChoiceField(
                label=vocab.name,
                choices=((val.code, val.label) for val in vocab.vocabvalue_set.all()),
                help_text=vocab.comment, required=True
            )
            self.voc_fields.append(f'voc_{vocab.pk}')

    def step1_fields(self):
        """Return a list of BoundField objects that are displayed at step 1."""
        return [
            field for field in self
            if field.name in ('owner', 'ser_num', 'div_num', 'density')
        ]

    def step2_fields(self):
        """Return a list of BoundField objects that are displayed at step 2."""
        return [
            field for field in self
            if field.name in ('slope', 'main_nature', 'main_perc', 'interv_int', 'radius')
        ]

    def step3_fields(self):
        """Return a list of BoundField objects that are displayed at step 3."""
        return [
            field for field in self
            if field.name in ('unnumbered', 'conifer', 'ecorcage') or field.name in self.voc_fields
        ]

    def gibi_fields(self):
        """Return a list of BoundField objects that are displayed at step 4."""
        return [
            [field for field in self if field.name in ('gibi_epicea', 'gibi_epicea_f', 'gibi_epicea_a')],
            [field for field in self if field.name in ('gibi_conifer', 'gibi_conifer_f', 'gibi_conifer_a')],
            [field for field in self if field.name in ('gibi_leaved', 'gibi_leaved_f', 'gibi_leaved_a')],
        ]


class SpeciesWidget(forms.Select):
    # Customized to be able to set species abbrev as data attribute of options
    option_template_name = 'forms/widgets/species_select_option.html'


class TreeForm(forms.ModelForm):
    perim = forms.IntegerField(label="Circonférence", max_value=900, required=False)

    class Meta:
        model = Tree
        fields = ['spec', 'diameter']
        widgets = {'spec': SpeciesWidget}

    def __init__(self, *args, opt_vocs=(), **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['diameter'].widget.attrs['min'] = 10
