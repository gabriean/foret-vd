import json
from datetime import date

from django.contrib.auth.models import Permission
from django.contrib.gis.geos import Point
from django.test import TestCase, modify_settings, override_settings
from django.urls import reverse

from inventory.models import (
    Density, Intervention, Nature, Owner, OwnerType, Plot, PlotObs,
    PlotObsPlanned, Protocol, User, Vocabulary, VocabValue
)
from inventory.tests.utils import BaseDataMixin
from municipality.models import Municipality

this_year = date.today().year


# Like common.mobile_settings.py
@override_settings(ROOT_URLCONF='mobile.urls', LOGIN_REDIRECT_URL='/')
@modify_settings(INSTALLED_APPS={'append': 'mobile'})
class MobileTests(BaseDataMixin, TestCase):
    @classmethod
    def setUpTestData(cls):
        myrtilles = Vocabulary.objects.create(name='Myrtilles')
        VocabValue.objects.create(vocab=myrtilles, code='1', label='<10%')

    def test_login(self):
        response = self.client.get(reverse('home'), follow=True)
        self.assertContains(
            response,
            '<input type="password" name="password" autocomplete="current-password" required id="id_password">',
            html=True
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('home'))
        self.assertContains(response, '<div id="inventory-choice-div">')
        # When logged in, return to login redirects to home
        response = self.client.get(reverse('login'), follow=True)
        self.assertContains(response, '<div id="inventory-choice-div">')

    def test_inventory_list(self):
        admin = User.objects.create_user(username='admin', password='password')
        admin.user_permissions.add(Permission.objects.get(codename='change_protocol'))
        operator = User.objects.create_user(username='op', password='password')
        Protocol.objects.create(name="Passed", year=date.today().year - 1)
        prot_1 = Protocol.objects.create(name="Vallorbe1", year=date.today().year)
        prot_2 = Protocol.objects.create(name="Vallorbe2", year=date.today().year)
        Protocol.objects.create(name="Vallorbe-noplots", year=date.today().year)
        vallorbe = Municipality.objects.get(name="Vallorbe")
        plot = Plot.objects.create(municipality=vallorbe, center=Point(2518430, 1173360, srid=2056))
        PlotObsPlanned.objects.create(
            plot=plot, protocol=prot_1, owner=Owner.objects.get(num='277'), operator=None
        )
        plot = Plot.objects.create(municipality=vallorbe, center=Point(2518600, 11731000, srid=2056))
        PlotObsPlanned.objects.create(
            plot=plot, protocol=prot_2, owner=Owner.objects.get(num='277'), operator=operator
        )

        self.client.force_login(admin)
        response = self.client.get(reverse('inventory-list'))
        self.assertEqual(set(inv['name'] for inv in response.json()), set(['Vallorbe1', 'Vallorbe2']))

        self.client.force_login(operator)
        response = self.client.get(reverse('inventory-list'))
        # operator sees only inventory on which he has plot to work on
        self.assertEqual([inv['name'] for inv in response.json()], ['Vallorbe2'])

    def test_inventory_plots(self):
        vallorbe = Municipality.objects.get(name="Vallorbe")
        self.protocol.geom = vallorbe.geom
        self.protocol.save()
        # Duplicate for another year
        new_protocol = Protocol.objects.get(pk=self.protocol.pk)
        new_protocol.pk = None
        new_protocol.year = 2018
        new_protocol.save()

        plot = Plot.objects.create(municipality=vallorbe, center=Point(2518430, 1173360, srid=2056))
        owner = Owner.objects.get(num='277')
        obs = PlotObs.objects.create(
            uid=111111, plot=plot, protocol=self.protocol, owner=owner, radius=9, slope=12,
            unnumbered=0, conifer=10, main_nature=Nature.objects.get(code=1), main_perc=95,
            density=Density.objects.get(surface=10000),
            gibi_epicea=0, gibi_epicea_f=0, gibi_epicea_a=0, gibi_conifer=0, gibi_conifer_f=0,
            gibi_conifer_a=0, gibi_leaved=0, gibi_leaved_f=0, gibi_leaved_a=0
        )
        new_obs = PlotObs.objects.get(pk=obs.pk)
        new_obs.pk = None
        new_obs.uid = 111112
        new_obs.protocol = new_protocol
        new_obs.save()
        density = Density.objects.get(surface=10000)

        self.client.force_login(self.user)
        response = self.client.get(reverse('inventory-plots', args=[self.protocol.pk]))
        self.maxDiff = None
        self.assertEqual(
            response.json(),
            {'type': 'FeatureCollection',
             'features': [{
                'type': 'Feature',
                'id': plot.pk,
                'geometry': {'coordinates': [6.371950632750559, 46.70644671039824],
                             'type': 'Point'},
                'properties': {
                    'coords_2056': [2518430, 1173360],
                    'id': plot.pk,
                    'municipality_id': vallorbe.pk,
                    'municipality_name': 'Vallorbe',
                    'obsURLs': {'2018': '/plotobs/%d/json/?srid=4326' % new_obs.pk},
                    'density': [density.pk, '1 placette par hectare'],
                    'div_num': '00',
                    'ser_num': 0,
                    'owner': [owner.pk, 'C277'],
                    'radius': 9,
                    'slope': 12,
                    },
                }]
            }
        )

    def test_sync_view(self):
        vallorbe = Municipality.objects.get(name="Vallorbe")
        plot = Plot.objects.create(municipality=vallorbe, center=Point(2518430, 1173360, srid=2056))
        myrtilles = Vocabulary.objects.get(name='Myrtilles')
        ecorcage = Vocabulary.objects.get(name='Écorçage du cerf')
        new_protocol = Protocol.objects.get(pk=self.protocol.pk)
        new_protocol.pk = None
        new_protocol.year = this_year
        new_protocol.geom = vallorbe.geom
        new_protocol.vocab_posp8 = myrtilles
        new_protocol.vocab_posp9 = ecorcage
        new_protocol.save()
        plantations = Intervention.objects.get(name='Plantations')
        owner = Owner.objects.get(num='277')
        density = Density.objects.get(surface=10000)
        PlotObsPlanned.objects.create(
            plot=plot, protocol=new_protocol, owner=owner, ser_num=1, div_num=2, operator=self.user
        )

        sync_data = {
            'type': 'FeatureCollection',
            'plot': plot.pk,
            'fillstep': 100,
            'features': [{
                'type': 'Feature',
                'geometry': {'type': 'Point', 'coordinates': [6.485439485739758,46.774384387558044]},
                'properties': {
                    'type': 'center',
                    'protocol': str(new_protocol.pk),
                    'year': this_year,
                    'exact_long': '',
                    'exact_lat': '',
                    'owner': ['', ''],
                    'ser_num': '1',
                    'div_num': '2',
                    'radius': '10',
                    'slope': '4',
                    'density': [str(density.pk), '1 placette par hectare'],
                    'main_nature': ['1','for\xc3\xaat non parcourue par le b\xc3\xa9tail'],
                    'main_perc': ['100', '100'],
                    'unnumbered': ['100', '100'],
                    'conifer': ['90', '90'],
                    f'voc_{myrtilles.pk}': ['1', '<10%'],
                    f'voc_{ecorcage.pk}': ['3', '30 %'],
                    f'voc_5555': ['', ''],
                    'interv_int': [str(plantations.pk), 'Plantations'],
                    'gibi_epicea': ['0', '0'],
                    'gibi_epicea_f': ['0', '0'],
                    'gibi_epicea_a': ['0', '0'],
                    'gibi_conifer': ['0', '0'],
                    'gibi_conifer_f': ['0', '0'],
                    'gibi_conifer_a': ['0', '0'],
                    'gibi_leaved': ['20', '20'],
                    'gibi_leaved_f': ['0', '0'],
                    'gibi_leaved_a': ['10', '10'],
                    'remarks': 'Bla bla',
                }
            }, {
                'type': 'Feature',
                'geometry': {'type': 'Point', 'coordinates': [6.485481121293587,46.774351815338626]},
                'properties': {
                    'spec': ['14', 'bouleau'],
                    'diameter': '16',
                },
            }]
        }

        self.client.force_login(self.user)
        response = self.client.post(
            reverse("sync"), json.dumps(sync_data), content_type="application/json"
        )
        if response.status_code != 200:
            self.fail(response.content)
        self.assertEqual(response.json(), {})
        new_obs = PlotObs.objects.get(plot=plot, protocol=new_protocol)
        self.assertEqual(new_obs.density, density)
        self.assertEqual(new_obs.reserved1, '1')
        self.assertEqual(new_obs.gibi_leaved, 20)
        self.assertEqual(new_obs.remarks, "Bla bla")
        self.assertEqual(new_obs.tree_set.count(), 1)
        tree = new_obs.tree_set.first()
        self.assertEqual(tree.spec.species, 'bouleau')
        self.assertEqual(tree.diameter, 16)
