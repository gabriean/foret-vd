from django.urls import include, path
from django.contrib.auth.views import LoginView
from django.contrib.staticfiles.views import serve as static_serve
from django.views.decorators.csrf import csrf_exempt

from . import views
from inventory.views import PlotObsDetailJSONView

urlpatterns = [
    path('auth/login/', LoginView.as_view(redirect_authenticated_user=True), name='login'),
    path('auth/', include('django.contrib.auth.urls')),

    path('', views.MainView.as_view(), name='home'),
    path('inventory/list/', views.InventoryListView.as_view(), name="inventory-list"),
    path('inventory/<int:pk>/geojson/', views.InventoryView.as_view(), name="inventory-geojson"),
    path('inventory/<int:pk>/plots/', views.InventoryPlots.as_view(), name="inventory-plots"),
    path('plotobs/<int:pk>/json/', PlotObsDetailJSONView.as_view(), name='plotobs-detail-json'),
    path('sync/', csrf_exempt(views.SyncView.as_view()), name='sync'),

    path('serviceworker.js', static_serve, kwargs={
        'path': 'js/serviceworker.js', 'insecure': True}),
]
