var CACHE_NAME = 'vdinv-v1',
    debug = false,
    // No need to cache much at install time considering our "get-from-cache-or-fetch-and-cache" strategy.
    urlsToCache = [
      '/static/img/locked.svg',
      '/static/img/unlocked.svg',
      '/static/img/online.svg',
      '/static/img/offline.svg',
      '/static/img/plan.png',
      '/static/img/satellite.png',
      '/static/img/back.svg',
      '/static/img/okcontinue.svg',
      '/static/img/help.svg',
      '/static/img/cancel.svg',
      '/static/img/newtree.svg',
      '/static/img/tree.svg',
      '/static/img/tree-dead.svg',
      '/static/img/tree-special.svg',
      '/static/img/tree-todo.svg'
    ];

self.addEventListener('install', function(event) {
    event.waitUntil(
        caches.open(CACHE_NAME).then(function (cache) {
            return cache.addAll(urlsToCache);
        })
    );
});

self.addEventListener('fetch', function(event) {
    // Return cached files if found in the cache
    event.respondWith(
        caches.match(event.request).then(function(from_cache) {
            // Even if a response comes from the cache, we still launch a network
            // request so as if the response is a HTTP 200, we replace the cached
            // instance by the new one (and ideally inform the client!).
            // Read https://ponyfoo.com/articles/progressive-networking-serviceworker
            // Evaluate adding {credentials: 'include'} for same origin requests
            var from_network = fetch(event.request.clone()).then(function(response) {
                // Check if we received a valid response (and from our origin with 'basic')
                if(!response || response.status !== 200 || response.type !== 'basic') {
                    return response;
                }
                if (event.request.url.indexOf("/login/") < 0 && event.request.method == 'GET') {
                    // Cache the response
                    console.log("Caching " + event.request.url);
                    var responseToCache = response.clone();
                    caches.open(CACHE_NAME).then(function(cache) {
                        cache.put(event.request, responseToCache);
                    });
                }
                return response;
            }).catch(function (err) { return null; });
            if (debug)  return from_network || from_cache;
            else return from_cache || from_network;
        })
    );
});
