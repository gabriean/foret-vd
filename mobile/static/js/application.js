'use strict';
var AppVersion = "0.3.30";

var audio = null;

function beep(vol, freq, duration) {
  if (audio === null) audio = new AudioContext();
  var v = audio.createOscillator();
  var u = audio.createGain();
  v.connect(u);
  v.frequency.value = freq;
  v.type = "square";
  u.connect(audio.destination);
  u.gain.value = vol*0.01;
  v.start(audio.currentTime);
  v.stop(audio.currentTime + duration * 0.001);
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function isOnline() {
    return window.navigator.onLine;
}

function isArray(variable) {
    return Object.prototype.toString.call(variable) === '[object Array]';
}

function isBlank(form) {
    // FIXME: not valid for radio values
    for (var i=0; i < form.elements.length; i++) {
        if (!form.elements[i].name.length) continue;
        if (form.elements[i].value != '') return false;
    }
    return true;
}

function rounded(num, dec) { return Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec); }

/*
 * Use the data-toggleimg img attribute to toggle its src image.
 * `img`: either the element itself or an ID.
 */
function toggleImg(img) {
    if (!(img instanceof HTMLElement)) {
        img = document.getElementById(img);
    }
    var newSrc = img.dataset.toggleimg;
    img.dataset.toggleimg = img.src;
    img.src = newSrc;
}

function totalHeight(elemId) {
    var style = document.defaultView.getComputedStyle(document.getElementById(elemId), '');
    var result = parseInt(style.getPropertyValue('height'), 10) + parseInt(style.getPropertyValue('margin-top'), 10) + parseInt(style.getPropertyValue('margin-bottom'), 10);
    if (isNaN(result)) return 0;
    return result;
}

function loadJSON(path) {
    return new Promise(function (resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", path);
        xhr.onload = function () {
            if (this.status >= 200 && this.status < 300) {
                if (xhr.responseText.includes('login-form')) {
                    // User needs to login again
                    location.reload(true);
                }
                try {
                    var content = JSON.parse(xhr.responseText);
                    resolve(content);
                } catch(e) {
                    reject({
                        status: this.status,
                        statusText: e
                    });
                }
            } else {
                reject({
                    status: this.status,
                    statusText: xhr.statusText
                });
            }
        };
        xhr.onerror = function () {
            reject({
                status: this.status,
                statusText: xhr.statusText
            });
        };
        xhr.send();
    });
}

function onEachFeature(feature, layer) {
    // Display property content for features
    if (feature.properties && feature.properties.popupContent) {
        layer.bindPopup(feature.properties.popupContent);
    }
}

function gradToRad(angle) {
     return angle * Math.PI / 200;
}

function gradToDeg(angle) {
    return 360 * angle / 400;
}

function degToRad(angle) {
  return angle * (Math.PI / 180);
}

function insertTableRow(tbody, klass, values) {
    let template = document.querySelector('#tree_row');
    let row = template.content.cloneNode(true);
    if (klass) row.classList.add(klass);
    var tds = row.querySelectorAll("td");
    values.forEach(function(val, idx) {
        tds[idx].textContent = val;
    });
    tbody.appendChild(row);
    return row;
}

/*
 * Global variables
 *  - verbose_names is defined in the index.html template
 */
var db = new PouchDB('ksp', {adapter: 'idb'}),
    angleUnit = 'gon',
    currentPlot = null,
    thisYear = new Date().getFullYear(),
    unsyncCounter = null,
    progressBar = null,
    caliperDevice = null,
    PLOTOBS_FORM_STEPS = 6,
    NEW_TREE_STEP = 999,
    FINAL_STEP = 1000,
    CENTER_INITIAL = [46.57, 6.65],
    ZOOM_INITIAL = 17,
    ZOOM_PLOT = 27,
    DISTANCE_WARNING_METERS = 50;

var styles = {
    'BestandesKarte': {
        "color": "#ff7800",
        "weight": 1,
        "opacity": 0.65
    },
    'Erschliessung': {
        "color": "#ff0000",
        "weight": 2,
    }
}

const epsg2056String = "+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs"
var epsg2056Converter = proj4(
    proj4('EPSG:4326'),
    epsg2056String
);

/*
 * Map class
 */
var Map = function () {
    this.map = L.map('map-div', {crs: L.TileLayer.Swiss.EPSG_2056, maxBounds: L.TileLayer.Swiss.latLngBounds});
    this.vLayers = {};
    this.centerOptions = {
        radius: 3,
        fillColor: "#ff1100"
    };
    this.map.createPane('highlightPane');
    this.map.getPane('highlightPane').style.zIndex = 650;
    this.gridLayer = new L.MetricGrid({
        proj4ProjDef: epsg2056String,
        bounds: [[2485000, 1075000] , [2830000, 1300000]],
        color: '#5272E5'
    });
    this.orthoLayer = L.tileLayer.swiss({layer: 'ch.swisstopo.swissimage'});
    this.grundLayer = L.tileLayer.swiss({layer: 'ch.swisstopo.pixelkarte-farbe'});
    /*this.grundLayer = L.tileLayer.swiss({
        url: 'https://ows.asitvd.ch/wmts/1.0.0/{layer}/default/{timestamp}/0/2056/{z}/{y}/{x}.{format}',
        format: 'png',
        layer: 'asitvd.fond_couleur', # or 'asitvd.fond_cadastral',
        timestamp: 'default',
        attribution: '<a href="https://www.asitvd.ch/">ASIT VD</a>'
    });*/

    this.baseLayer = this.grundLayer;
    this.baseLayer.addTo(this.map);
    this.gridLayer.addTo(this.map);
    // Initially empty layers
    this.plotLayer = L.featureGroup().addTo(this.map);
    L.control.scale({imperial: false}).addTo(this.map);
    L.control.polylineMeasure({
        showBearings: true,
        tooltipTextFinish: 'Cliquer pour <b>terminer la ligne</b><br>',
        tooltipTextDelete: 'Appuyer sur SHIFT et cliquer pour <b>supprimer le point</b>',
        tooltipTextMove: 'Cliquer et glisser pour <b>déplacer le point</b><br>',
        tooltipTextResume: '<br>Appuyer sur CTRL et cliquer pour <b>continuer la ligne</b>',
        tooltipTextAdd: 'Appuyer sur CTRL et cliquer pour <b>ajouter un point</b>',
        measureControlTitleOn: 'Activer les mesures',
        measureControlTitleOff: 'Désactiver les mesures',
        clearControlTitle: 'Effacer les mesures',
        angleUnit: 'gradians'
    }).addTo(this.map);
    this.map.setView(CENTER_INITIAL, ZOOM_INITIAL);
};

Map.prototype.attachHandlers = function () {
    var self = this;
    this.map.on('moveend', function() {
        // Store current center and zoom level
        var zoom = self.map.getZoom(),
            center = self.map.getCenter();
        db.get('current-state').then(currentState => {
            var changed = false;
            if (currentState.zoom !== zoom) {
                currentState.zoom = zoom;
                changed = true;
            }
            if (!center.equals(currentState.center)) {
                currentState.center = center;
                changed = true;
            }
            if (changed) {
                db.put(currentState).catch(err => {
                    // Ignoring any error, as this is not critical;
                    console.log("Non-fatal error: " + err);
                    return;
                });
            }
        });
    });
    // Display current position on the map
    this.longDiv = document.getElementById('currentLong');
    this.latDiv = document.getElementById('currentLat');
    this.altDiv = document.getElementById('currentAlt');
    if (navigator.geolocation) {
        navigator.geolocation.watchPosition(position => {
                self.setCurrentPosition(position);
                checkCurrentPointDifference(position);
            }, error => {
                console.log("Geolocation error: " + error.message);
            },
            {timeout: 10000, enableHighAccuracy: true, maximumAge: 5000
        });
    }
};

Map.prototype.setCurrentPosition = function (position) {
    // Diff from my position in CdF (debugging help)
    var devDiff = [0, 0]; //position.coords.longitude < 7 ? [0.457, 0.85] : [0, 0];
    if (!this.currentPositionLayer) {
        this.currentPositionLayer = L.marker(
            [0, 0], {
            icon: L.icon({iconUrl: staticImages.currentPos, iconSize: [20, 20]}),
            opacity: 0.9
        });
        this.map.addLayer(this.currentPositionLayer);
    }
    this.currentPositionLayer.setLatLng([
        position.coords.latitude + devDiff[0],
        position.coords.longitude + devDiff[1]
    ]);
    var projected = epsg2056Converter.forward([
        position.coords.longitude + devDiff[1],
        position.coords.latitude + devDiff[0]]
    );
    this.longDiv.innerHTML = rounded(projected[0], 2);
    this.latDiv.innerHTML = rounded(projected[1], 2);
    if (position.coords.altitude) {
        this.altDiv.innerHTML = rounded(position.coords.altitude, 0);
    }
};

Map.prototype.switchBaseLayer = function () {
    this.map.removeLayer(this.baseLayer);
    this.baseLayer = this.baseLayer === this.orthoLayer ? this.grundLayer : this.orthoLayer;
    this.baseLayer.addTo(this.map);
};

Map.prototype.toCurrentPosition = function () {
    if (this.currentPositionLayer) {
        var newPos = this.currentPositionLayer.getLatLng();
        this.map.setView(newPos, Math.max(this.map.getZoom(), 16));
    } else {
        alert("Votre emplacement n'a pas pu être déterminé.");
    }
};

Map.prototype.resetLayers = function () {
    if (this.vLayers.plotCircle) {
        this.map.removeLayer(this.vLayers.plotCircle);
    }
    if (this.vLayers.plotCenter) {
        this.map.removeLayer(this.vLayers.plotCenter);
    }
};

Map.prototype.highlightPoint = function(coords) {
    // Show the point at coords in red on the map
    this.unHighlight();
    if (isNaN(coords[0])) return;
    this.highlightLayer = L.circle(coords, {
        radius: 1, color: '#ff0000', pane: 'highlightPane'
    });
    this.map.addLayer(this.highlightLayer);
};

Map.prototype.unHighlight = function () {
    if (this.highlightLayer) {
        this.map.removeLayer(this.highlightLayer);
        this.highlightLayer = null;
    }
};

Map.prototype.removePointsLayer = function (layerType) {
    if (this.vLayers[layerType]) {
        this.map.removeLayer(this.vLayers[layerType]);
        this.vLayers[layerType] = null;
    }
};

Map.prototype.loadLayer = function (layerName, url) {
    var self = this;
    loadJSON(url).then(geojson => {
        if (self.vLayers[layerName]) {
            self.vLayers[layerName].clearLayers();
            self.vLayers[layerName].addData(geojson);
        } else {
            self.vLayers[layerName] = L.geoJSON(geojson, {
                style: styles[layerName],
                onEachFeature: onEachFeature
            });
            self.vLayers[layerName].addTo(self.map);
        }
    });
};

Map.prototype.showInventory = function (inventory, fitMap) {
    // we could display other inventory data here.
    return;
};

Map.prototype.resetToOverview = function() {
    // Reset map/info div sizes
    map.unHighlight();
    document.getElementById('map-container').classList.remove('focused');
    document.getElementById('infos-div').classList.remove('focused');
    document.getElementById('error').style.display = 'none';
    var self = this;
    setTimeout(() => { self.map.invalidateSize()}, 400);
    // Return to the inventory view
    if (!this.map.hasLayer(this.plotLayer)) this.map.addLayer(this.plotLayer);
    if (this.map._stored_map_state) {
        this.map.setView(this.map._stored_map_state[0], this.map._stored_map_state[1]);
    } else {
        this.map.fitBounds(this.plotLayer.getBounds());
    }
};

Map.prototype.focusOnPlot = function (coords, radius) {
    this.map.removeLayer(this.plotLayer);
    this.vLayers.plotCircle = L.circle(coords, radius).addTo(this.map);
    this.vLayers.plotCenter = L.circle(coords, 0.1).addTo(this.map);
    // Make the map div smaller when focused on plot.
    document.getElementById('map-container').classList.add('focused');
    document.getElementById('infos-div').classList.add('focused');
    var self = this;
    setTimeout(() => { self.map.invalidateSize()}, 400);
    this.map._stored_map_state = [this.map.getCenter(), this.map.getZoom()];
    this.map.setView(coords, ZOOM_PLOT);
};

/*
 * Store map data offline.
 * Vector data are already automatically cached through serviceworker.js.
 */
Map.prototype.storeOffline = function () {
    var bbox = this.map.getBounds(),
        zoomCurrent = this.map.getZoom(),
        zoomMax = this.baseLayer.options.maxNativeZoom;
    if (zoomMax - zoomCurrent > 4) {
        alert("Sie müssen eine kleinere ? ? um die Karte Daten lokale zu speichern.");
        return;
    }

    document.getElementById('progress').style.display = 'block';
    progressBar = new ProgressBar.Line('#progress', {
        color: '#008000', strokeWidth: 5
    });
    this.baseLayer.on('seedprogress', function(seedData){
        //var percent = 100 - Math.floor(seedData.remainingLength / seedData.queueLength * 100);
        progressBar.set(progressBar.value() + (1 / seedData.queueLength));
    });
    this.baseLayer.on('seedend', function(seedData){
        progressBar.destroy();
        progressBar = null;
        document.getElementById('progress').style.display = 'none';
        alert("Daten wurden erfolgreich gespeichert");
    });

    /*
     * Some figures about tiles caching:
     * tile size: 256x256
     * tile download size: 25Kb
     * tile size in db: (Base64), ~220Kb??
     * on one map level: 12 tiles
     * seeding:
     *   - 1 level: 42/48 tiles (1.1Mb dl / 10Mb stor.)
     *   - 2 levels: 140/150 tiles (3.6Mb dl / 35Mb stor.)
     *   - 3 levels: ~500 tiles (12.5Mb dl / 120Mb stor.)
     *   - 4 levels: ~2000 tiles (50Mb dl / 500Mb stor.)
     * Browser Storage docs:
     *  https://medium.com/dev-channel/offline-storage-for-progressive-web-apps-70d52695513c#.tva434uxh
     */
    this.baseLayer.seed(bbox, zoomCurrent, zoomMax);
    // Progress/end of seeding is handled in 'seedprogress'/'seedend' events
}

class Caliper {
    constructor() {
        this.device = null;
        // UUIDs must be lowercase!
        this.serviceUuid = '6e400001-b5a3-f393-e0a9-e50e24dcca9e';
        // Receive from device
        this.txCharUuid = '6e400003-b5a3-f393-e0a9-e50e24dcca9e';
        // Send to device
        this.rxCharUuid = '6e400002-b5a3-f393-e0a9-e50e24dcca9e';
        document.querySelector('button#caliper-disconnect').addEventListener('click', this.disconnect.bind(this));
        document.querySelector('button#caliper-connect').addEventListener('click', this.connect.bind(this));
    }

    showStatus() {
        if (this.device === null) {
            document.querySelector('#caliper-disconnected').style.display = 'block';
            document.querySelector('#caliper-connected').style.display = 'none';
        } else {
            document.querySelector('#caliper-disconnected').style.display = 'none';
            document.querySelector('#caliper-connected').style.display = 'block';
        }
    }

    connect(ev) {
        if (! navigator.bluetooth) {
            alert("Désolé, ce navigateur ne gère pas les connexions Bluetooth.");
            return;
        }
        console.log('Start connecting to Bluetooth device');
        var caliper = this;
        navigator.bluetooth.requestDevice({
            optionalServices: [caliper.serviceUuid],
            acceptAllDevices: true
        }).then(device => {
            var server = device.gatt.connect();
            caliper.device = device;
            device.addEventListener('gattserverdisconnected', caliper.onDisconnected.bind(caliper));
            caliper.showStatus();
            return server;
        }).then(server => {
            return server.getPrimaryService(caliper.serviceUuid);
        }).then(service => {
            service.getCharacteristic(caliper.txCharUuid).then(char => {
                char.addEventListener('characteristicvaluechanged', caliper.receiveValue.bind(caliper));
                char.startNotifications();
                caliper.txChar = char;
            }).catch(error => {
                console.log("Error getting tx characteristic: " + error);
            });
            service.getCharacteristic(caliper.rxCharUuid).then(char => {
                caliper.rxChar = char;
            }).catch(error => {
                console.log("Error getting rx characteristic: " + error);
            });
        }).catch(error => {
            console.log('Bluetooth error: ' + error);
            caliper.showStatus();
        });
    }

    disconnect(ev) {
        var caliper = this;
        this.txChar.stopNotifications().then(_ => {
            caliper.txChar.removeEventListener('characteristicvaluechanged', caliper.receiveValue);
            caliper.txChar == null;
        });
        this.device.gatt.disconnect();
        this.device = null;
        this.showStatus();
    }

    onDisconnected(event) {
        let device = event.target;
        console.log('Device ' + device.name + ' is disconnected.');
        this.device = null;
        this.showStatus();
    }

    receiveValue(event) {
        // Data is sent as a NMEA string and ends with a checksum and end of line. [CR][LF]
        var rawValue = new Uint8Array(event.target.value.buffer);
        var value = String.fromCharCode.apply(null, rawValue).trim();
        console.log('Value received by Bluetooth: ' + value);
        if (value.startsWith('$PHGF,SPC')) {
            // A new species value was received ($PHGF,SPC,2,ABC,*2B)
            // Start a new tree form
            showStep(NEW_TREE_STEP);

            let parts = value.split(',');
            let specCode = parts[2];
            let specChars = parts[3];
            // Set species to the tree form
            let specOpt = document.querySelector(`[data-abrev='${specChars}']`);
            document.getElementById('id_spec').value = specOpt.value;
        } else if (value.startsWith('$PHGF,DIA')) {
            // A new diameter value was received ($PHGF,DIA,M,277,*2A)
            let parts = value.split(',');
            let mm = parts[3];
            // Set diameter to the tree form (rounded in cm)
            document.getElementById('id_diameter').value = Math.round(mm / 10);

            // End the tree form.
            let treeForm = document.querySelector('#steptree');
            formSet.submitForm(null, treeForm);
            beep(100, 700, 250);
        }
        // Acknowledgement expected from the caliper after a value has been sent (0x06).
        // If not sent within 1 second the MDII will post a message in the display saying “Check connection”
        this.rxChar.writeValue(Uint8Array.of(6));
    }
}

/*
 * TreeObs class
 */
var TreeObs = function (plotobs, data) {
    this.plotobs = plotobs;
    if (data) {
        // Data is a GeoJSON structure received from a query on existing PlotObs
        this.validated = !(data.validated === false);
        this.properties = data.properties;
    } else {
        this.validated = false;
        this.properties = {};
    }
    this._coords = null;  // No tree geometry yet
};

TreeObs.prototype.setProperty = function(name, value) {
    this.properties[name] = value;
};

TreeObs.prototype.getPreviousTreeObs = function() {
    var prevPlotObs = this.plotobs.plot.previousObs(this.plotobs.year);
    if (prevPlotObs) {
        for (var i = 0; i < prevPlotObs.trees.length; i++) {
            if (prevPlotObs.trees[i].properties.tree == this.properties.tree) return prevPlotObs.trees[i];
        }
    }
    return null;
}

TreeObs.prototype.asGeoJSON = function () {
    return {
        type: "Feature",
        geometry: {},
        validated: this.validated,
        properties: this.properties
    };
};

TreeObs.prototype.makeCurrent = function () {
    /* If editing, go to proper step.
     * Else, highlight point and line in tree list.
    */
    if (this.plotobs.fillStep < 100) {
        var step = PLOTOBS_FORM_STEPS + this.plotobs.trees.indexOf(this) + 1;
        goToStep(step);
    } else {
        var treeTable = document.getElementById('obs-infos-trees-table');
        if (treeTable.offsetParent !== null) {
            var current = treeTable.getElementsByClassName("current").item(0);
            if (current) current.classList.remove("current");
            document.getElementById('treeline-' + this.properties.nr).classList.add("current");
        }
    }
};

/*
 * PlotObs class
 */
var PlotObs = function (plot, data) {
    this.trees = [];
    this.plot = plot;
    if (data) {
        // When data is provided, it is a GeoJSON structure.
        // The first feature is the center coords with PlotObs properties
        this.id = data.features[0].id;
        this.plot_id = data.plot;
        this.year = data.features[0].properties.year;
        this.geometry = data.features[0].geometry;
        this._properties = data.features[0].properties;
        // Following features are trees
        function dataEmpty(props) {
            return (props.diameter=='' && props.perim=='');
        }
        for (var i = 1; i < data.features.length; i++) {
            var treeData = data.features[i];
            if (treeData.validate === false && dataEmpty(treeData.properties)) continue;
            this.trees.push(new TreeObs(this, data.features[i]));
        }
        if (data.fillstep) this.fillStep = data.fillstep;
        else this.fillStep = 100;
    } else {
        this.plot_id = plot.id;
        this._properties = {type: "center"};
        this.geometry = plot.center();
        this.fillStep = 1;
    }
};

PlotObs.prototype.asGeoJSON = function () {
    // First feature is the Plot center with the PlotObs properties.
    // All following features are trees.
    var struct = {
        type: "FeatureCollection",
        plot: this.plot_id,
        fillstep: this.fillStep,
        features: [{
            type: "Feature",
            geometry: this.geometry,
            properties: this._properties
        }]
    };
    for (var i=0; i < this.trees.length; i++) {
        struct.features.push(this.trees[i].asGeoJSON());
    }
    return struct;
};

PlotObs.prototype.dbId = function () {
    return 'plotobs-' + this.plot_id + '-' + this.year;
};

PlotObs.prototype.properties = function () {
    var result = {};
    for (var prop in this._properties) {
        // Exclude non-interesting props or data already shown at plot level
        if (['type', 'Gemeinde', 'Aufnahmepunkt (plot)'].indexOf(prop) >= 0) {
            continue;
        }
        result[prop] = this._properties[prop];
    }
    return result;
};

PlotObs.prototype.setProperty = function(name, value) {
    this._properties[name] = value;
    if (name == 'year') {
        this.year = value;
    }
    // When forest edge factor is lower than 0.6, no trees are observed.
    if (name == 'forest_edgef' && parseFloat(value) < 0.6) {
        this.trees = [];
        // Remove allstepselect entries for trees
        var stepSelect = document.getElementById('allstepsselect');
        for (var i = stepSelect.length - 1; i >= 0; i--) {
            if (stepSelect.options[i].text.indexOf("Baum") >= 0) {
                stepSelect.removeChild(stepSelect.options[i]);
            }
        }
        document.getElementById('stepfinal').getElementsByClassName('newtree')[0].style.display = 'none';
    }
};

PlotObs.prototype.addTreeObs = function () {
    var tree = new TreeObs(this);
    this.trees.push(tree);
    return tree;
};

PlotObs.prototype.notCutTrees = function () {
    return [];
    /*return this.trees.filter(item => !item.properties.vita[1].includes("(c)"));*/
};

PlotObs.prototype.getNextAvailableNumber = function () {
    var number = 1;
    for (var i = 0; i < this.trees.length; i++) {
        var nr = parseInt(this.trees[i].properties['nr']);
        if (nr >= number) {
            number = nr + 1;
        }
    }
    return number;
};

PlotObs.prototype.setUpStepSelect = function () {
    var stepSelect = document.getElementById('allstepsselect');
    // Reset step select to its initial state
    if (!stepSelect.dataset.initial) {
        stepSelect.dataset.initial = stepSelect.innerHTML;
    } else {
        stepSelect.innerHTML = stepSelect.dataset.initial;
    }
    for (var i=1; i < stepSelect.options.length; i++) {
        if (this.fillStep >= stepSelect.options[i].value)
            stepSelect.options[i].removeAttribute("disabled");
    }
    document.getElementById('stepfinal').getElementsByClassName('newtree')[0].style.display = 'block';
    // Insert a select option for each tree step
    var finalOpt = stepSelect.options[stepSelect.options.length - 1];
    for (var i=0; i < this.trees.length; i++) {
        var opt = document.createElement('option');
        opt.value = i + PLOTOBS_FORM_STEPS + 1;
        opt.innerHTML = "Étape " + opt.value + ": Arbre";
        if (this.fillStep < PLOTOBS_FORM_STEPS + 1) {
            opt.setAttribute("disabled", "disabled");
        }
        stepSelect.insertBefore(opt, finalOpt);
    }
};

PlotObs.prototype.showDetails = function() {
    document.getElementById('obs-input').style.display = 'none';
    document.getElementById('error').style.display = 'none';
    document.getElementById('button-' + this.year).classList.add("current");

    // Show plotobs data
    var generalTable = document.getElementById('obs-infos-general-table');
    generalTable.innerHTML = "";
    var obsProperties = this.properties();
    for (var prop in obsProperties) {
        var tr = generalTable.insertRow();
        var td = tr.insertCell();
        var verbose_prop = verbose_names[prop];
        if (!verbose_prop) verbose_prop = prop;
        td.appendChild(document.createTextNode(verbose_prop));
        td = tr.insertCell();
        var value = obsProperties[prop];
        if (isArray(value)) {
            value = value[1]; // first is object pk, second is human-readable value
        }
        td.appendChild(document.createTextNode(value));
    }
    // Show tree data
    var treeTable = document.getElementById('obs-infos-trees-table');
    treeTable.innerHTML = "";
    for (var i=0; i<this.trees.length; i++) {
        var tree = this.trees[i];
        if (!tree.validated) continue;
        var tr = treeTable.insertRow();
        tr.setAttribute("id", "treeline-" + tree.properties.id);
        var td = tr.insertCell();
        td.appendChild(document.createTextNode(tree.properties.spec[1]));
        td = tr.insertCell();
        td.appendChild(document.createTextNode('⌀' + tree.properties.diameter));
        tr.addEventListener('click', tree.makeCurrent.bind(tree));
    }
    // Set height and visibility
    resizeObsDivs();
}

PlotObs.prototype.finished = function () {
    this.fillStep = 100;
    var self = this;
    if (isOnline()) {
        this.sendToServer().catch(err => {
            var msg = "Les données d’inventaire n’ont pas pu être enregistrées sur le serveur. Elles ont été enregistrées localement. (L’erreur est: " + err.statusText + ")";
            if (err.responseText) msg += "\nDétails: " + err.responseText;
            alert(msg);
            self.saveLocally();
        }).finally(() => {
            // force icon refresh as its color may change
            self.plot.showOnMap(map);
        });
    } else {
        this.saveLocally();
    }
};

PlotObs.prototype.saveLocally = function () {
    var id = this.dbId();
    saveData(id, this.asGeoJSON());
    // Save the id for future sync
    db.get('new-plotobs').catch(err => {
        if (err.name === 'not_found') {
            return {
                _id: 'new-plotobs',
                newIDs: []
            };
        }
    }).then(doc => {
        if (doc.newIDs.indexOf(id) < 0) {
            doc.newIDs.push(id);
            unsyncCounter.increment();
            return db.put(doc);
        }
    });
    // force icon refresh as its color may change
    if (this.plot) this.plot.showOnMap(map);
};

PlotObs.prototype.remove = function () {
    // Remove an unfinished PlotObs (no UI for this function yet)
    // TODO: change color of year button and plot point
    if (this.fillStep >= 100) {
        alert("Unable to remove a completed observation.");
        return;
    }
    var docId = this.dbId();
    db.get(docId).then(doc => {
        unsyncCounter.decrement();
        // Remove the id from the newIDs list
        db.get('new-plotobs').then(doc => {
            doc.newIDs.splice(doc.newIDs.indexOf(docId), 1);
            db.put(doc);
        });
        return db.remove(doc);
    }).catch(err => {
        alert("An error occurred while trying to remove the current observation: " + err.statusText);
    }).then(() => {
        delete currentPlot.obs[thisYear];
        currentPlot._years.shift();
        var prevObs = currentPlot.previousObs(thisYear);
        if (prevObs) {
            document.getElementById("button-" + prevObs.year).click();
        }
    });
};

PlotObs.prototype.sendToServer = function () {
    // Send data to server
    var self = this;
    return new Promise(function (resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.open("POST", sync_url);
        xhr.onload = function () {
            if (this.status >= 200 && this.status < 300) {
                if (this.responseURL.indexOf('/login/') >= 0) {
                    reject({status: 403, responseText: "Not logged in the platform."});
                } else {
                    resolve(self);
                }
            } else {
                reject({
                    status: this.status,
                    statusText: xhr.statusText,
                    responseText: this.responseText
                });
            }
        };
        xhr.onerror = function () {
            reject({
                status: this.status,
                statusText: xhr.statusText
            });
        };
        var postData = JSON.stringify(self.asGeoJSON());
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.send(postData);
    }).then(() => {
        unsyncCounter.decrement();
        // Remove the id from the newIDs list
        db.get('new-plotobs').then(doc => {
            doc.newIDs.splice(doc.newIDs.indexOf(self.dbId()), 1);
            db.put(doc);
        });
    }).catch(err => {
        if (err.status > 299) {
            // This will allow reediting the PlotObs.
            self.fillStep = 99;
        }
        self.saveLocally();
        throw err;
    });
};

/*
 * Plot class
 */
class Plot {
    constructor(feature) {
        this.id = feature.id;
        this.geoJSON = feature;
        this.properties = feature.properties;
        this.obs = {};
        this.layer = null;
        let arr = Object.keys(this.geoJSON.properties.obsURLs);
        this._years = arr.map(obj => parseInt(obj));
    }
    center() { return this.geoJSON.geometry; }
    years() {
        // Return existing PlotObs years, sorted from more recent to older.
        this._years.sort();
        this._years.reverse();
        return this._years;
    }

    fillYearButtonsDiv() {
        var buttonDiv = document.getElementById('year-buttons'),
            years = this.years().slice(),
            hasNewYear = false,
            self = this;
        if (years.indexOf(thisYear) < 0) {
            // Add "this" year if not existing, in red
            years.unshift(thisYear);
            hasNewYear = true;
        }
        for (var i=0; i<years.length; i++) {
            var button = document.createElement("button"),
                year = years[i];
            button.setAttribute('id', 'button-' + year);
            button.className = "year";
            if (year == thisYear) {
                if (hasNewYear) button.classList.add("new");
                else if (self.obs[year].fillStep < 100) button.classList.add("editing");
            }
            button.innerHTML = year;
            button.addEventListener('click', (ev) => {
                const btn = ev.target;
                if (btn.className.indexOf('current') > -1) return;
                const oldCurrent = document.querySelector("button.current");
                if (oldCurrent) oldCurrent.classList.remove("current");
                btn.classList.add("current");
                var obs = self.obs[parseInt(btn.innerText)];
                if (obs === undefined || obs.fillStep < 100) {
                    formSet.startInput(obs);
                } else {
                    obs.showDetails();
                }
            });
            buttonDiv.appendChild(button);
        }
    }
    hasObs(year) { return this.years().indexOf(year) >= 0; }
    getObs(year) {
        var self = this;
        return new Promise(function(resolve, reject) {
            if (self.obs[year]) resolve(self.obs[year]);
            else if (self.properties.obsURLs[year] !== undefined) {
                loadJSON(self.properties.obsURLs[year]).then(data => {
                    var po = new PlotObs(self, data);
                    self.setObs(po, year);
                    resolve(po);
                }).catch(err => {
                    console.log(err);
                    reject(new Error("Les données de cette placette ne sont malheureusement pas disponibles hors ligne."));
                });
            } else {
                // Check if locally saved
                db.get('plotobs-' + self.id + '-' + year).then(doc => {
                    var po = new PlotObs(self, doc);
                    self.setObs(po, year);
                    resolve(po);
                }).catch(err => {
                    reject(new Error("Aucune donnée pour l'année " + year));
                });
            }
        });
    }
    setObs(po, year) {
        this.obs[year] = po;
        if (this._years.indexOf(year) < 0) this._years.push(year);
    }
    getLatestObs() {
        // Return the more recent finished PlotObs
        var proms = [];
        for (let i = 0; i < this._years.length; i++) {
            proms.push(this.getObs(this.years()[i]));
        }
        return Promise.all(proms).then(obss => {
            for (let i = 0; i < obss.length; i++) {
                if (obss[i].fillStep >= 100) return obss[i];
            }
        });
    }
    previousObs(year) {
        var previousIdx = this.years().indexOf(parseInt(year)) + 1;
        // We suppose the previous obs has been loaded (should be, but...)
        if (previousIdx < this.years().length) return this.obs[this.years()[previousIdx]];
        else return null;
    }
    createObs(year) {
        var po = new PlotObs(this, null);
        po.setProperty('year', year);
        this.obs[year] = po;
        this._years.unshift(year);
        return po;
    }

    showOnMap(map) {
        if (this.layer) {
            map.plotLayer.removeLayer(this.layer);
        }
        var icon = L.divIcon({className: 'icon-blue', iconSize: [20, 20], iconAnchor: [10, 10]});
        // Show different icon color if obs for this year is already done (or partially done)
        if (this.hasObs(thisYear)) {
            if (this.obs[thisYear] && this.obs[thisYear].fillStep < 100) {
                icon = L.divIcon({className: 'icon-orange', iconSize: [20, 20], iconAnchor: [10, 10]});
            } else {
                icon = L.divIcon({className: 'icon-green', iconSize: [20, 20], iconAnchor: [10, 10]});
            }
        }
        var coords = this.center().coordinates.slice().reverse();
        this.layer = L.marker(coords, {icon: icon});
        var self = this;
        this.layer.on({click: function (ev) {
            map.focusOnPlot(coords, self.properties.radius);
            self.getLatestObs().then(obs => {
                self.showDetails();
                if (obs) obs.showDetails();
            }).catch(err => {
                // Display error
                document.getElementById('error').innerHTML = `Erreur: ${err.message}`;
                document.getElementById('error').style.display = 'block';
            });
        }});
        map.plotLayer.addLayer(this.layer);
    }

    showDetails() {
        document.getElementById('obs-input').style.display = 'none';
        // Show general plot data under the map
        document.querySelector('#undermap-commune').textContent = this.properties.municipality_name;
        document.querySelector('#undermap-coords').textContent = this.properties.coords_2056;
        // Show plot data
        document.getElementById('plot-owner').textContent = this.properties.owner[1];
        document.getElementById('plot-div-num').textContent = this.properties.div_num;
        document.getElementById('plot-ser-num').textContent = this.properties.ser_num;
        document.getElementById('plot-density').textContent = this.properties.density ? this.properties.density[1] : '?';
        document.getElementById('plot-data').style.display = 'block';
        if (this != currentPlot) {
            currentPlot = this;
            this.fillYearButtonsDiv();
        }
        if (navigator.geolocation) navigator.geolocation.getCurrentPosition(checkCurrentPointDifference);
    }
}

/*
 * Form instance to edit/add a tree obs.
 */
class TreeForm {
    constructor(tree, step) {
        this.tree = tree;
        this.form = document.getElementById('steptree');
        this.vitaSelect = document.getElementById('id_vita');
        this.reset();

        var isNewTree = tree === null || tree.properties.tree == '' || tree.properties.tree === undefined;
        if (isNewTree) {
            // Only new trees can be canceled
            document.getElementById('canceltree').style.display = "block";
        } else {
            document.getElementById('canceltree').style.display = "none";
            // Inform widget of previous dbh value
            var prevTObs = tree.getPreviousTreeObs();
            if (prevTObs) {
                document.getElementById('id_diameter').dataset.previousValue = prevTObs.properties.dbh;
            }
            document.getElementById('id_diameter').focus();
        }

        if (tree === null) {
            // Entering a blank new tree form
            map.unHighlight();
            // Add a new option to the step select
            var obs = currentPlot.obs[thisYear];
            var stepSelect = document.getElementById('allstepsselect');
            var opt = document.createElement('option');
            opt.value = step;
            opt.innerHTML = "nouvel arbre";
            stepSelect.insertBefore(opt, stepSelect.options[stepSelect.options.length - 1]);
            stepSelect.selectedIndex = stepSelect.options.length - 2;
        } else {
            // Populate form from tree properties
            setFormDefaultFromProperties(this.form, tree.properties);
        }
        this.hideHistoricTrees();

        this.form.dataset.step = step;
    }

    reset() {
        formSet.resetForm(this.form);
        //this.form.elements.id_tree.value = '';
        var dbh = document.getElementById('id_diameter');
        dbh.removeAttribute("disabled");
        dbh.dataset.previousValue = null;
        // Undo hideHistoricTrees
        var specSelect = document.getElementById('id_spec');
        for (var j = 0; j < specSelect.options.length; j++) {
            specSelect.options[j].style.display = 'block';
        }
    }

    hideHistoricTrees() {
        // Hide all 'historic' species unless selected
        var specSelect = document.getElementById('id_spec');
        var warn = false;
        for (var j = 0; j < specSelect.options.length; j++) {
            if (specSelect.options[j].dataset.historic == 'true') {
                if (j == specSelect.selectedIndex) {
                    warn = true;
                    document.getElementById('species_warning').style.display = 'block';
                } else {
                    specSelect.options[j].style.display = 'none';
                }
            }
        }
        if (!warn) document.getElementById('species_warning').style.display = 'none';
    }

    // Class method, not instance related (run only once per app load)
    static initHandlers() {
        document.getElementById('id_perim').addEventListener('input', function (ev) {
            var diam = Math.round(this.value / Math.PI);
            var dbhInput = document.getElementById('id_diameter');
            dbhInput.value = diam;
        });
        document.getElementById('canceltree').addEventListener('click', function (ev) {
            // Cancelling a new tree form.
            ev.preventDefault();
            const currentTree = formSet.treeForm.tree;
            if (currentTree) {
                // Remove from obs.trees if it was added
                const treeArray = currentTree.plotobs.trees;
                const index = treeArray.indexOf(currentTree);
                if (index > -1) {
                    treeArray.splice(index, 1);
                }
            }
            // Remove allstepselect entry
            const stepSelect = document.getElementById('allstepsselect');
            const opt = stepSelect.options[stepSelect.selectedIndex];
            stepSelect.selectedIndex = stepSelect.selectedIndex - 1;
            stepSelect.removeChild(opt);
            showStep(FINAL_STEP);
        });
    }
}


/* Set of input forms */
class FormSet {
    constructor() {
        this.forms = document.querySelectorAll('form.input-form');
        this.treeForm = null;
        this.valueZeroDisableFields = ['id_gibi_epicea', 'id_gibi_conifer', 'id_gibi_leaved', 'id_unnumbered']
    }

    valueZeroDisable(fieldId) {
        // When choosing 0 for such dfields, the following percentage fields must be 0.
        const select = document.getElementById(fieldId),
              opt = select.options[select.selectedIndex],
              disable = (opt.value == '0'),
              targets = fieldId == 'id_unnumbered' ? ['id_conifer'] : [fieldId + '_f', fieldId + '_a'];
        targets.forEach(targetId => {
            let targetField = document.getElementById(targetId);
            if (disable) targetField.value = 0;
            targetField.disabled = disable;
        });
    }

    /* Called once from DOMContentLoaded event.*/
    initHandlers() {
        // Form submit buttons
        for (var i = 0; i < this.forms.length; i++) {
            this.forms[i].addEventListener('submit', this.submitForm.bind(this));
        }
        this.valueZeroDisableFields.forEach(fieldId => {
            document.getElementById(fieldId).addEventListener(
                'change', this.valueZeroDisable.bind(this, fieldId)
            );
        });
        // Show or hide remarks if "not ok" radio is chosen at first step
        [document.querySelector('#verifnotok'), document.querySelector('#verifok')].forEach(radio => {
            radio.addEventListener('change', (ev) => {
                if (ev.target.value == 'notok')
                    document.querySelector('#step1-remarks').classList.remove('hidden');
                else document.querySelector('#step1-remarks').classList.add('hidden');
            });
        });
        // When clicking the new tree or weiter buttons, set hint on the form about
        // which button was clicked.
        var ntButtons = document.querySelectorAll('button.newtree');
        for (var i = 0; i < ntButtons.length; i++) {
            ntButtons[i].addEventListener('click', function(ev) {
                this.form.dataset.nextform = "newtree";
            });
        }
        var wButtons = document.querySelectorAll('button.weiter');
        for (var i = 0; i < wButtons.length; i++) {
            wButtons[i].addEventListener('click', function(ev) {
                this.form.dataset.nextform = "nextstep";
            });
        }
        // Button to keep current GPS coordinates in form inputs
        document.getElementById('keepthis').addEventListener('click', function (ev) {
            ev.preventDefault();
            document.getElementById('id_exact_long').value = document.getElementById('currentLong').innerHTML;
            document.getElementById('id_exact_lat').value = document.getElementById('currentLat').innerHTML;
            checkLonLatDifference();
            if (document.getElementById('currentAlt').innerHTML.length) {
                document.getElementById('id_sealevel').value = document.getElementById('currentAlt').innerHTML;
            }
        });
        document.getElementById('trees_summary').addEventListener('click', (ev) => {
            const treeTR = ev.target.closest('tr');
            if (treeTR) {
                // go to matching tree step
                showStep(PLOTOBS_FORM_STEPS + treeTR.tree_position + 1);
            }
        });
    }

    submitForm(ev, form) {
        var form = form || ev.target;
        if (ev) ev.preventDefault();
        if (this.validateForm(form)) {
            if (form.dataset.nextform == "newtree") {
                showStep(NEW_TREE_STEP);
            } else {
                if (getNextStep() > PLOTOBS_FORM_STEPS) showStep(FINAL_STEP);
                else showStep(getNextStep());
            }
        }
    }

    hideAll() {
        for (var i = 0; i < this.forms.length; i++) {
            this.forms[i].style.display = 'none';
        }
        document.getElementById('allsteps').style.display = 'none';
        document.getElementById('caliper-disconnected').style.display = 'none';
        document.getElementById('caliper-connected').style.display = 'none';
        map.unHighlight();
    }

    startInput(obs) {
        // Show start screen for a new observation
        document.getElementById('obs-data').style.display = 'none';
        var divHeight = document.getElementById('infos-div').clientHeight -
                          totalHeight('inventory-title') -
                          totalHeight('year-buttons') - 25;
        document.getElementById('obs-input').setAttribute("style", "height: " + divHeight + "px");
        document.getElementById('obs-input').style.display = 'block';
        if (obs) {
            // We have a partially-filled PlotObs
            this.initForms(obs);
        } else {
            document.getElementById('step0').style.display = 'block';
        }
    }

    /*
     * Initialize plot obs input forms and show first or 'fillStep' form.
     */
    initForms(obs) {
        // Reset all input forms
        for (var i=0; i < this.forms.length; i++) {
            this.resetForm(this.forms[i]);
        }
        var is_new = (obs === null);
        if (is_new) {
            obs = currentPlot.createObs(thisYear);
        }
        obs.setUpStepSelect();
        // Set inventory team
        db.get('current-state').then(function (data) {
            document.getElementById('id_protocol').value = data.inventory;
        });
        // Set initial values for non-tree form widgets
        var previousObs = currentPlot.previousObs(thisYear);
        // The first form has mixed plot and plotobs properties
        // currentPlot wins for some props like Entwicklungstufe, Mischungsgrad and Schlussgrad
        if (previousObs) {
            var inheritedProps = Object.assign(
                {}, previousObs.properties(), currentPlot.properties
            );
        } else {
            var inheritedProps = Object.assign({}, currentPlot.properties);
        }
        if (is_new) {
            var form1 = document.getElementById('step1');
            setFormDefaultFromProperties(form1, inheritedProps);
            var form2 = document.getElementById('step2');
            setFormDefaultFromProperties(form2, {radius: 11, main_perc: 100});
        } else {
            for (var step = 1; step <= PLOTOBS_FORM_STEPS; step++) {
                var form = document.getElementById('step' + step);
                setFormDefaultFromProperties(form, obs.properties());
            }
        }
        getRadioValue('verif_step1') == 'notok' ? document.querySelector('#step1-remarks').classList.remove('hidden') : document.querySelector('#step1-remarks').classList.add('hidden');
        this.valueZeroDisableFields.forEach(fieldId => { this.valueZeroDisable(fieldId); })

        // Insert previous remarks above the remarks form input
        var form4Body = document.getElementById('previous-remarks');
        form4Body.innerHTML = '';
        while (previousObs !== null) {
            var remarks = previousObs.properties().remarks;
            if (remarks !== undefined && remarks.length > 0) {
                var row = form4Body.insertRow(0);
                var th = document.createElement('th');
                th.appendChild(document.createTextNode(previousObs.year + ':'));
                row.appendChild(th);
                var cell2 = row.insertCell(1);
                cell2.appendChild(document.createTextNode(remarks));
            }
            previousObs = currentPlot.previousObs(previousObs.year);
        }

        if (obs.fillStep > PLOTOBS_FORM_STEPS)
            // Pass directly to final step
            showStep(FINAL_STEP);
        else showStep(obs.fillStep);
    }

    resetForm(form) {
        for (var i = 0; i < form.elements.length; i++) {
            if (['submit', 'button'].indexOf(form.elements[i].type) >= 0 ||
                form.elements[i].type == 'hidden') continue;
            if (form.elements[i].type == 'radio'){
                form.elements[i].checked = form.elements[i].value == '';
            } else if (form.elements[i].type == 'select-one') {
                form.elements[i].value = "";
                // Find any default value
                for (var j = 0; j < form.elements[i].options.length; j++) {
                    if (j > 0 && form.elements[i].options[j].defaultSelected) {
                        form.elements[i].value = form.elements[i].options[j].value;
                        break;
                    }
                }
            }
            else {
                form.elements[i].value = "";
            }
        }
    }

    validateForm(form) {
        if (form === null) form = this.getActive();
        if (form.id === "stepfinal") return true; // Nothing to validate here

        var obs = currentPlot.obs[thisYear];
        var currentStep = parseInt(form.dataset.step);
        // Get form target, PlotObs or TreeObs
        if (currentStep <= PLOTOBS_FORM_STEPS)  {
            var target = obs;
        } else {
            var treeIdx = currentStep - PLOTOBS_FORM_STEPS - 1;
            if (treeIdx >= obs.trees.length) {
                target = obs.addTreeObs();
            } else {
                target = obs.trees[treeIdx];
            }
        }
        // Set form values to either PlotObs or TreeObs
        for (var i = 0; i < form.elements.length; i++) {
            if (!form.elements[i].name.length) continue;
            var value = form.elements[i].value;
            if (form.elements[i].tagName == "SELECT") {
                if (value != '') {
                    value = [value, form.elements[i][form.elements[i].selectedIndex].textContent];
                } else {
                    value = [value, value];
                }
            } else if (form.elements[i].type == 'radio') {
                if (!form.elements[i].checked) continue;
                value = [form.elements[i].value, form.elements[i].nextElementSibling.textContent];
            }
            if (form.elements[i].name == 'remarks') {
                // Remarks may be twice, sync the contents
                document.querySelectorAll('#id_remarks').forEach(inp => inp.value = value);
            }
            target.setProperty(form.elements[i].name, value);
        }
        // Checking validity after saving values, so input are not lost even if validation fails
        var valid = form.reportValidity();
        if (!valid) return false;

        // Mark tree as validated
        if (target !== obs) {
            target.validated = true;
        }
        if ((currentStep + 1) > obs.fillStep) {
            obs.fillStep = currentStep + 1;
        }
        obs.saveLocally();
        map.unHighlight();
        return true;
    }

    getActive() {
        for (var i=0; i < this.forms.length; i++) {
            if (this.forms[i].style.display == 'block') return this.forms[i];
        }
        return null;
    }
}


/*
 * Number widget to reflect the number of unsynchronised PlotObs.
 */
var UnsyncCounter = function () {
    this.count = 0;
    this.div = document.getElementById('unsync-counter');
    var self = this;
    db.get("new-plotobs").then(doc => {
        self.setCount(doc.newIDs.length);
    }).catch(err => {
        self.setCount(0);
    });
    // Allow force-resync of all local ksp data
    this.div.addEventListener('dblclick', function (ev) {
        if (confirm("Möchten sie wirklich alle lokalen Daten neu synchronisieren?")) {
            var promises = [];
            db.allDocs({include_docs: true, startkey: 'plotobs'}).then(result => {
                for (var i=0; i<result.rows.length; i++) {
                    if (result.rows[i].id.indexOf('-' + thisYear) >=0) {
                        var po = new PlotObs(null, result.rows[i].doc);
                        if (po.fillStep >= 100) promises.push(po.sendToServer());
                    }
                }
                Promise.all(promises).then(() => {
                    alert(promises.length + " Punkte wurden neu synchronisiert.");
                });
            });
        }
    });
};

UnsyncCounter.prototype.setCount = function (count) {
    if (count < 0) count = 0;
    this.count = count;
    this.div.innerHTML = this.count;
    if (this.count > 0) {
        this.div.classList.add("nonempty");
    } else {
        this.div.classList.remove("nonempty");
    }
};

UnsyncCounter.prototype.increment = function () { this.setCount(this.count + 1); }
UnsyncCounter.prototype.decrement = function () { this.setCount(this.count - 1); }

var map = new Map();
var formSet = new FormSet();
caliperDevice = new Caliper();

function getRadioValue(radioName) {
    for (const inp of document.querySelectorAll('input[name=' + radioName + ']')) {
        if (inp.checked) return inp.value;
    }
}

function resetInfos() {
    document.getElementById('plot-data').style.display = 'none';
    document.getElementById('year-buttons').innerHTML = "";
    document.getElementById('obs-data').style.display = 'none';
    document.getElementById('obs-input').style.display = 'none';
    formSet.hideAll();
    map.resetLayers();
    currentPlot = null;
}

function getNextStep() {
    // The next step is the next allstepsselect item
    // or the next unvalidated tree.
    var stepSelect = document.getElementById('allstepsselect');
    var nextOption = stepSelect.options[stepSelect.selectedIndex + 1]
    var step = nextOption.value;
    if (step == FINAL_STEP) {
        // Check all existing trees are validated
        var trees = currentPlot.obs[thisYear].trees;
        for (var j = 0; j < trees.length; j++) {
            if (!trees[j].validated) {
                step = PLOTOBS_FORM_STEPS + j + 1;
                break;
            }
        }
    }
    return step;
}

function goToStep(step) {
    // Validate currently-visible form. We don't really care if the form
    // pass validation or not. If not, the values are not saved and the tree
    // not marked as validated.
    if (step > PLOTOBS_FORM_STEPS && currentPlot.obs[thisYear].fillStep <= PLOTOBS_FORM_STEPS) {
        console.log(currentPlot.obs[thisYear].fillStep);
        // Too soon to display tree steps
        return;
    }
    formSet.validateForm(null);
    showStep(step);
}

function showStep(step) {
    var obs = currentPlot.obs[thisYear];
    var previousObs = currentPlot.previousObs(thisYear);
    formSet.hideAll();
    // Select and display the form matching step.
    if (step == FINAL_STEP) {
        var form = document.getElementById('stepfinal');
    } else if (step <= PLOTOBS_FORM_STEPS) {
        var form = document.getElementById('step' + step);
    } else {
        var form = document.getElementById('steptree');
    }
    form.style.display = 'block';
    var stepSelect = document.getElementById('allstepsselect');
    stepSelect.value = step.toString();
    if (step > 0) {
        document.getElementById('allsteps').style.display = 'block';
    }
    // Un-disable any select option before or matching this step
    for (var i=0; i < stepSelect.options.length; i++) {
        if (parseInt(stepSelect.options[i].value) < step.toString()) {
            stepSelect.options[i].removeAttribute("disabled");
        }
        else if (step > PLOTOBS_FORM_STEPS && stepSelect.options[i].text.indexOf('Baum') >= 0) {
            // At tree stages, enable all trees in one step
            stepSelect.options[i].removeAttribute("disabled");
        }
    }
    if (step == FINAL_STEP) {
        stepSelect.options[stepSelect.options.length - 1].removeAttribute("disabled");
        // Populate a summary of the trees
        var treeTable = document.getElementById('trees_summary');
        var newTbody = document.createElement('tbody');
        obs.trees.forEach((tree, index) => {
            const props = tree.properties;
            const klass = props.tree == '' ? 'new' : '';
            insertTableRow(newTbody, klass, ['', props.spec[1], props.diameter]);
            newTbody.lastElementChild.tree_position = index;
        });
        var oldTbody = treeTable.children[1];
        oldTbody.parentNode.replaceChild(newTbody, oldTbody);
        Array.from(document.getElementsByClassName('treenumb')).forEach(
            numb => { numb.classList.remove('hidden'); }
        );
        caliperDevice.showStatus();
    }

    if (step > PLOTOBS_FORM_STEPS && step != FINAL_STEP) {
        // A tree form
        var treeIdx = step - PLOTOBS_FORM_STEPS - 1;
        if (treeIdx < obs.trees.length) {
            // An existing tree (copied from previous obs or recently added).
            formSet.treeForm = new TreeForm(obs.trees[treeIdx], step);
        } else {
            // New tree
            var newStep = PLOTOBS_FORM_STEPS + obs.trees.length + 1
            formSet.treeForm = new TreeForm(null, newStep);
        }
        caliperDevice.showStatus();
    }
    // Must come after values have been populated, as locks depend on values.
    resetLockInputs();
}

function setFormDefaultFromProperties(form, props) {
    for (var i = 0; i < form.elements.length; i++) {
        // Don't touch unnamed inputs or inputs from formset management forms
        if (!form.elements[i].name.length || form.elements[i].name.indexOf('_FORMS') >= 0) continue;
        if (form.elements[i].tagName == 'SELECT') {
            if (props[form.elements[i].name]) {
                form.elements[i].value = props[form.elements[i].name][0];
            } else {
                for (var j = 0; j < form.elements[i].options.length; j++) {
                    if (j > 0 && form.elements[i].options[j].defaultSelected) {
                        form.elements[i].value = form.elements[i].options[j].value;
                        break;
                    }
                }
            }
        } else {
            if (props[form.elements[i].name]) {
                if (form.elements[i].type == 'radio') {
                    form.elements[i].checked = form.elements[i].value == props[form.elements[i].name][0];
                } else {
                    form.elements[i].value = props[form.elements[i].name];
                }
            } else {
                if (form.elements[i].type == 'radio' && form.elements[i].defaultChecked === true) {
                    form.elements[i].checked = true;
                }
            }
        }
    }
}

function tabClick(ev) {
    var tabChildren = document.querySelectorAll('.tab-child');
    for (var j = 0; j < tabChildren.length; j++) {
        tabChildren[j].style.display = "none";
    }
    document.getElementById(this.dataset.target).style.display = "block";
    var tabDiv = document.getElementById('obs-tabs');
    tabDiv.querySelector("li.active").className = "";
    this.className = "active";
    // Show tree numbers on the map only when the active tab is "trees"
    var treenumbs = document.getElementsByClassName('treenumb');
    if (this.dataset.target == 'obs-infos-trees') {
        for(var i = 0; i < treenumbs.length; i++) { treenumbs[i].classList.remove('hidden'); }
    } else {
        map.unHighlight();
        for(var i = 0; i < treenumbs.length; i++) { treenumbs[i].classList.add('hidden'); }
    }
}

function saveData(docId, data) {
    // Save data in local database, replacing any existing occurrences
    db.get(docId).then(doc => {
        return db.remove(doc);
    }).then(result => {
        data._id = docId;
        return db.put(data);
    }).catch(err => {
        if (err.name === 'not_found') {
            data._id = docId;
            return db.put(data);
        } else throw err;
    });
}

/*
 * Load inventory data: inventory boundaries, plots, wms layers.
 * If forCache is true, also load every PlotObs URL to have all data in cache.
 */
function loadInventoryData(option, forCache) {
    return loadJSON(option.dataset.url).then(function(inventory) {
        return new Promise(function(resolve, reject) {
            // Fill owner select with inventory.properties.owners.
            var ownerSelect = document.getElementById('id_owner');
            for(var i = ownerSelect.options.length - 1 ; i >= 0 ; i--) { ownerSelect.remove(i); }
            for(var i = 0; i < inventory.owners.length; i++) {
                var opt = document.createElement("option");
                opt.value = inventory.owners[i][0];
                opt.innerHTML = inventory.owners[i][1];
                ownerSelect.appendChild(opt);
            }
            // Hide/unhide custom vocabulary selects
            var voc_trs = document.querySelectorAll('tr.voc_field');
            for (var i = 0; i < voc_trs.length; i++) {
                voc_trs[i].style.display = 'none';
            }
            for (var i = 0; i < inventory.vocab_ids.length; i++) {
                var voc_tr = document.getElementById('tr_voc_' + inventory.vocab_ids[i]);
                if (voc_tr) { voc_tr.style.display = 'table-row'}
            }

            document.querySelector('#loading').classList.remove('hidden');
            document.querySelector('#modal-div').classList.remove('hidden');
            resolve(loadJSON(inventory.plotsURL));
            // This can go on asynchronously
            if (!forCache)  {
                map.showInventory(inventory, true);
                Object.keys(inventory.layerURLs).forEach(function(k, i) {
                    map.loadLayer(k, inventory.layerURLs[k]);
                });
            }
        });
    }).then(features => {
        // Fetch locally saved PlotObs, if any
        return new Promise(function(resolve, reject) {
            db.get('new-plotobs').then(function(doc) { resolve([features, doc.newIDs]); }
            ).catch(function(err) { resolve([features, []]); });
        });
    }).then(result => {
        var features = result[0],
            newIds = result[1],
            plotIdsWithNew = {}; // Array storing locally-saved PlotObs
        newIds.map(obj => {
            plotIdsWithNew[parseInt(obj.split('-')[1])] = parseInt(obj.split('-')[2]);
        });
        for (var i = 0; i < features.features.length; i ++) {
            var plot = new Plot(features.features[i]);
            if (plotIdsWithNew.hasOwnProperty(plot.id)) {
                plot.getObs(plotIdsWithNew[plot.id]).then(po => {
                    if (!forCache) po.plot.showOnMap(map);
                });
            } else if (!forCache) plot.showOnMap(map);
            if (forCache) {
                // Call every PlotObs URL to get them in the cache
                for (var j = 0, years = plot.years(); j < years.length; j++) {
                    plot.getObs(years[j]);
                }
            }
        }
        document.querySelector('#loading').classList.add('hidden');
        document.querySelector('#modal-div').classList.add('hidden');
        map.map.fitBounds(map.plotLayer.getBounds());
    });
}

function unlockInput(ev) {
    if (this.src.indexOf('unlocked') !== -1) return;
    toggleImg(this);
    var prevElement = this.previousElementSibling;
    while(prevElement !== null) {
        if (prevElement.disabled) {
            prevElement.disabled = false;
            break;
        }
        prevElement = prevElement.previousElementSibling;
    }
}

/*
 * If target inputs have no value set, hide locks, set input disabled to false.
 * If target inputs have values, display lock, reset to lock image, set input
 * disabled to true.
 */
function resetLockInputs() {
    var locks = document.querySelectorAll('img.locked-input');
    for (var i = 0; i < locks.length; i++) {
        var input = locks[i].previousElementSibling;
        if (input && input.tagName == 'SPAN') input = input.previousElementSibling;
        if (input && input.value != '') {
            if (locks[i].src.indexOf('unlocked') >= 0) toggleImg(locks[i]);
            locks[i].style.display = 'inline';
            input.disabled = true;
        } else {
            locks[i].style.display = 'none';
            if (input) input.disabled = false;
        }
    }
}

/* Return the difference in meters between two projected coordinates (unit in meters). */
function computeCoordDifference(coords1, coords2) {
    var latDiff = Math.abs(coords1[1] - coords2[1]);
    var longDiff = Math.abs(coords1[0] - coords2[0]);
    // Pythagore
    return Math.floor(Math.sqrt(latDiff * latDiff + longDiff * longDiff));
}

function checkLonLatDifference() {
    // Check and warn if difference between theoretical and real coordinate is too high
    var theo_longVal = currentPlot.properties.coords_2056[0];
    var theo_latVal = currentPlot.properties.coords_2056[1];
    var longVal = document.getElementById('id_exact_long').value;
    var latVal = document.getElementById('id_exact_lat').value;
    document.getElementById('exact_long_warning').style.display = 'none';
    document.getElementById('exact_lat_warning').style.display = 'none';
    var warn_msg = 'La différence entre la coordonnée théorique et la coordonnée réelle est de : ';
    if (longVal != '' && Math.abs(longVal - theo_longVal) > 20) {
        document.getElementById('exact_long_warning').innerHTML = warn_msg + Math.floor(Math.abs(longVal - theo_longVal)) + 'm';
        document.getElementById('exact_long_warning').style.display = 'block';
    }
    if (latVal != '' && Math.abs(latVal - theo_latVal) > 20) {
        document.getElementById('exact_lat_warning').innerHTML = warn_msg + Math.floor(Math.abs(latVal - theo_latVal)) + 'm';
        document.getElementById('exact_lat_warning').style.display = 'block';
    }
}

function checkCurrentPointDifference(position) {
    if (currentPlot === null) return;
    var projected = epsg2056Converter.forward([
        position.coords.longitude,
        position.coords.latitude
    ]);
    var difference = computeCoordDifference(currentPlot.properties.coords_2056, projected);
    if (difference > DISTANCE_WARNING_METERS) {
        document.querySelector('#coords_warning').style.display = 'block';
    } else {
        document.querySelector('#coords_warning').style.display = 'none';
    }
}

/*
 * After the user choose the inventory, restore the map state to the previous
 * position (if the inventory did not change) or to the inventory overview.
 */
function setInitialState(inventoryOption) {
    // Read state stored in the db (current municipality) and restore it
    db.get('current-state').then(data => {
        return data;
    }).catch(err => {
        if (err.name === 'not_found') {
            console.log("No current-state existing document, creating a new one.");
            var newState = {_id: 'current-state'};
            var newProm = db.put(newState).then(res => {
                return {_id: res.id, _rev: res.rev};
            }).catch(err => {
                console.log("Unable to save a new current-state object: " + err);
            });
            return newProm;
        } else throw err;
    }).then(currentState => {
        document.getElementById('inventory-title').innerHTML = inventoryOption.text;
        if (currentState.inventory && currentState.inventory == inventoryOption.value) {
            loadInventoryData(inventoryOption, false);
            // We are still in the same inventory, just restore the state
        } else if (inventoryOption.value) {
            currentState.inventory = inventoryOption.value;
            db.put(currentState).then(() => {
                // set a new inventory overview
                loadInventoryData(inventoryOption, false);
            }).catch(err => {
                console.log("Unable to save current-state with new inventory id: " + err);
            });
        } else {
            map.map.setView(L.TileLayer.Swiss.unproject_2056(L.point([2600000, 1200000])), 16);
        }
    });
}

function checkOfflineData() {
    // If unsynced content, propose to sync
    db.get('new-plotobs').then(doc => {
        if (doc.newIDs.length == 0) return;
        var promises = [];
        // Get all locally saved ids
        for (var i=0; i < doc.newIDs.length; i++) {
            var docId = doc.newIDs[i];
            var p = db.get(docId).then(podata => {
                return new PlotObs(null, podata);
            }).catch(err => {
                console.log("Unable to retrieve local PlotObs with id " + docId);
            });
            promises.push(p);
        }
        Promise.all(promises).then(pos => {
            pos = pos.filter(po => po !== undefined); // undefined in case of errors
            if (!pos.length) return;
            var finishedObs = pos.filter(po => po.fillStep >= 100);
            if (!finishedObs.length || !confirm("Souhaitez-vous synchroniser maintenant les nouvelles données hors ligne?")) return;
            var promises2 = [];
            for (var i=0; i < finishedObs.length; i++) {
                var p = finishedObs[i].sendToServer();
                promises2.push(p);
            }
            Promise.all(promises2).then(() => {
                alert("Les données sont maintenant enregistrées sur le serveur.");
            });
        });
    }).catch(err => {
        // A not_found indicates we have no unsynced content
        if (err.name !== 'not_found') {
            console.log(err);
        }
    });
}

function resizeObsDivs() {
    document.getElementById('obs-data').style.display = 'block';
    var tabChildHeight = document.getElementById('infos-div').clientHeight -
                         totalHeight('inventory-title') -
                         totalHeight('year-buttons') -
                         totalHeight('obs-tabs') - 25;
    var tabChildren = document.querySelectorAll('.tab-child');
    for (var j = 0; j < tabChildren.length; j++) {
        tabChildren[j].setAttribute("style", "height: " + tabChildHeight + "px");
    }
    document.getElementById('obs-infos-general').style.display = 'block';
}

function loadInventoryChoices() {
    const invSelect = document.querySelector('#inventory-choice');
    fetch(invSelect.dataset.url).then(resp => {
        const contentType = resp.headers.get("content-type");
        if (contentType && contentType.includes("application/json")) {
            return resp.json().then(data => {
                data.forEach(item => {
                    const opt = document.createElement('option');
                    opt.innerHTML = item.name;
                    opt.value = item.pk;
                    opt.dataset.url = item.url;
                    invSelect.appendChild(opt);
                });
            });
        } else {
            // Typically, the user is not connected
            location.href = '/auth/login?next=/';
        }
    });
}

document.addEventListener("DOMContentLoaded", (ev) => {
    redirect_log_messages();
    loadInventoryChoices();
    // Tab handling
    var tabs = document.querySelectorAll('ul.tabs li');
    for (var i = 0; i < tabs.length; i++) {
        tabs[i].addEventListener('click', tabClick);
    }
    unsyncCounter = new UnsyncCounter();
    // Back to overview button
    document.getElementById('gemeinde-back').addEventListener('click', (ev) => {
        resetInfos();
        map.resetToOverview();
    });
    // Start/End observation buttons
    document.getElementById('input-start').addEventListener('click', (ev) => {
        document.getElementById('step0').style.display = 'none';
        formSet.initForms(null);
    });
    document.getElementById('input-end').addEventListener('click', (ev) => {
        // If the current form is filled, validate it
        var form = formSet.getActive();
        if (!isBlank(form)) {
            var form_valid = formSet.validateForm(form);
            if (!form_valid) return;
        }
        formSet.hideAll();
        currentPlot.obs[thisYear].finished();
        currentPlot.obs[thisYear].showDetails();
    });

    // Select widget giving access to steps
    document.getElementById('allstepsselect').addEventListener('change', function (ev) {
        goToStep(this.value);
    });

    // Input unlock buttons
    var locks = document.querySelectorAll('img.locked-input');
    for (var i = 0; i < locks.length; i++) {
        locks[i].addEventListener('click', unlockInput);
    }

    // Form help buttons
    document.querySelectorAll('.form-help').forEach((img) => {
        img.addEventListener('click', function (ev) {
            var modalDiv = document.getElementById("modal-div");
            modalDiv.querySelectorAll('.help').forEach((div) => div.style.display = "none");
            modalDiv.style.display = "block";
            document.getElementById(img.dataset.target).style.display = "block";
        });
    });
    document.querySelectorAll('.modal-close').forEach((button) => {
        button.addEventListener('click', function (ev) {
            document.querySelector('#' + this.dataset.target).style.display = "none";
        });
    });

    TreeForm.initHandlers();
    formSet.initHandlers();

    document.getElementById('sync-unsync').addEventListener('click', (ev) => {
        if (!confirm("Voulez-vous enregistrer localement les données et cartes visibles?")) return;
        var inventorySelect = document.getElementById('inventory-choice'),
            inventoryOption = inventorySelect[inventorySelect.selectedIndex];
        loadInventoryData(inventoryOption, true)
        map.storeOffline();
    });

    function goingOnline(ev) {
        toggleImg('network-status');
        document.getElementById('sync-unsync').style.display = "block";
        checkOfflineData();
    }

    function goingOffline(ev) {
        toggleImg('network-status');
        document.getElementById('sync-unsync').style.display = "none";
        //db.allDocs({startkey: 'gemeinde-', endkey: 'gemeinde-zzz'}).then (function (result) {
            // TODO: disable municipalities without offline content in the select list
        //});
    }

    if (!window.navigator.onLine) toggleImg('network-status');
    window.addEventListener('online',  goingOnline);
    window.addEventListener('offline', goingOffline);

    map.attachHandlers();
    // Base layer switcher
    document.getElementById('baselayer').addEventListener('click', (ev) => {
        toggleImg('baselayer');
        map.switchBaseLayer();
    });
    document.getElementById('tocurrent').addEventListener('click', (ev) => {
        map.toCurrentPosition();
    });
    document.getElementById('toinventory').addEventListener('click', (ev) => {
        map.map.fitBounds(map.plotLayer.getBounds());
    });
    document.getElementById('id_exact_long').addEventListener('input', checkLonLatDifference);
    document.getElementById('id_exact_lat').addEventListener('input', checkLonLatDifference);

    // Inventory selection form at application load
    document.getElementById('inventory-form').addEventListener('submit', function (ev) {
        ev.preventDefault();
        var inventorySelector = this.elements[0];
        if (inventorySelector.selectedIndex < 1) {
            alert("Veuillez choisir un inventaire");
            return;
        }
        document.getElementById('inventory-choice-div').style.display = 'none';
        setInitialState(inventorySelector[inventorySelector.selectedIndex]);
    });
    var versionDiv = document.getElementById('appversion');
    versionDiv.innerHTML = 'v.' + AppVersion;
    versionDiv.addEventListener('dblclick', (ev) => {
        document.querySelector('#debug-pane').style.display = 'block';
    });
    var reloader = document.querySelector('#debug-reload');
    reloader.addEventListener('click', (ev) => {
        window.location.reload(true);
    });
    window.addEventListener('resize', (ev) => {
        if (document.getElementById('obs-data').style.display == 'block') {
            resizeObsDivs();
        }
    }, true);
    checkOfflineData();
});

// Debug function to show cache keys
function show_cache_keys() {
    caches.open('kspmobile-v1').then(cache => {
        cache.keys().then(keys => {
             for (var i=0; i < keys.length; i++) {
                 console.log(keys[i].url);
             }
        });
    });
}

// Debug function to clear locally saved data
function clear_local_data() {
    db.get('new-plotobs').catch(err => {
        return;
    }).then(doc => {
        doc.newIDs = [];
        return db.put(doc);
    });
    unsyncCounter.setCount(0);
}

function redirect_log_messages() {
    var origLog = console.log;
    var logger = document.querySelector('#debug-messages');
    console.log = function (message) {
        if (typeof message == 'object') {
            logger.innerHTML += (JSON && JSON.stringify ? JSON.stringify(message) : message) + '<br>';
        } else {
            logger.innerHTML += message + '<br>';
        }
        origLog(message);
    }
}
