import getpass
from fabric import task
from invoke import Context, Exit
#from patchwork import files

MAIN_HOST = 'www.inv-vd.ch'

project = 'inv-vd.ch'
default_db_owner = 'claude'

@task(hosts=[MAIN_HOST])
def deploy(conn):
    venv_bin = "/var/venvs/%s/bin/" % project

    with conn.cd("/var/www/%s" % project):
        # activate maintenance mode
        conn.run('sed -i -e "s/UPGRADING = False/UPGRADING = True/" common/wsgi.py')
        conn.run('git stash && git pull && git stash pop')
        conn.run(f'{venv_bin}python manage.py migrate')
        conn.run(f'{venv_bin}python manage.py collectstatic --noinput --settings=common.mobile_settings')
        '''# Check media folder existence
        conn.config['sudo']['password'] = getpass.getpass("Enter the sudo password (on the server):")
        for folder in ('imported', 'protocols'):
            path = '/var/www/%s/media/%s' % (project, folder)
            files.directory(conn, path, user='www-data', mode='776', sudo=True)
        '''
        conn.run('sed -i -e "s/UPGRADING = True/UPGRADING = False/" common/wsgi.py')
        conn.run('touch common/wsgi_mobile.py')
    with conn.cd("/var/www/%s/manuel" % project):
        # docs
        conn.run(f'{venv_bin}mkdocs build')


@task(hosts=[MAIN_HOST])
def clone_remote_db(conn, dbname='vd_inv_forestier'):
    """ Dump a remote database and load it locally """
    local = Context()

    def exist_local_db(db):
        res = local.run('psql --list', hide='stdout')
        return db in res.stdout.split()

    def exist_username(user):
        res = local.run('psql -d postgres -c "select usename from pg_user;"', hide='stdout')
        return user in res.stdout.split()

    tmp_path = f'/tmp/{dbname}.dump'
    conn.config['sudo']['password'] = getpass.getpass("Enter the sudo password (on the server):")
    conn.sudo(f'pg_dump --no-owner --no-privileges -Fc {dbname} > {tmp_path}', user='postgres')
    #conn.run(f'tar czf {tmp_path}.tar.gz {tmp_path}')
    conn.get(tmp_path, None)

    if exist_local_db(dbname):
        rep = input(f'A local database named "{dbname}" already exists. Overwrite? (y/n)')
        if rep == 'y':
            local.run(f'psql -d postgres -c "DROP DATABASE {dbname};"')
        else:
            raise Exit("Database not copied")

    if exist_username(dbname):
        owner = dbname
    else:
        owner = default_db_owner
    local.run(f'psql -d postgres -c "CREATE DATABASE {dbname} OWNER={owner};"')
    # Looks like postgis is indirectly installed by postgis schema.
    #local.run(f'psql -d {dbname} -c "CREATE EXTENSION IF NOT EXISTS postgis;"')
    #local.run('tar xzf %(db)s.sql.tar.gz' % {'db': dbname})
    local.run(f'psql -d {dbname} -c "ALTER DATABASE {dbname} set search_path = public, postgis;"')
    local.run(f'pg_restore --no-owner --no-privileges -d {dbname} {dbname}.dump')
