"""
Django settings for vdinv project.
"""

from pathlib import Path
BASE_DIR = Path(__file__).resolve().parent.parent

DEBUG = True

ALLOWED_HOSTS = ['localhost']


# Application definition

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.gis',
    'leaflet',
    'django_select2',

    'inventory',
    'imports',
    'dbdocs',
    'document',
    'municipality',

    'django.contrib.admin',  # Allow registration templates override
]

MIDDLEWARE = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # Custom:
    'common.middleware.LoginRequiredMiddleware',
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                # Default list
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.request',
                'django.contrib.messages.context_processors.messages',
            ],
            'builtins': ['django.templatetags.static'],
        },
    },
]

ROOT_URLCONF = 'common.urls'

WSGI_APPLICATION = 'common.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'vd_inv_forestier',
    }
}

AUTH_USER_MODEL = 'inventory.User'

LANGUAGE_CODE = 'fr'
TIME_ZONE = 'CET'
USE_I18N = True
USE_L10N = True
USE_TZ = False

DATA_UPLOAD_MAX_NUMBER_FIELDS = 10000  # Mainly for importing with many errors.

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_ROOT = BASE_DIR / 'static'
STATIC_URL = '/static/'

MEDIA_ROOT = BASE_DIR / 'media'
MEDIA_URL = '/media/'

LOGIN_URL = '/auth/login/'
LOGOUT_REDIRECT_URL = '/'

# For protocol PDF inclusion in edition form
X_FRAME_OPTIONS = 'SAMEORIGIN'

DBDOCS_MAIN_APP_NAME = 'inventory'
DBDOCS_LOOKUP_TABLE_WHITELIST = [
    'density', 'intervention', 'nature', 'owner', 'owner_type', 'tree_spec', 'vocabulary',
]
DBDOCS_MAIN_TABLE_WHITELIST = [
    'plot', 'plot_obs', 'tree', 'municipality'
]

LEAFLET_CONFIG = {
    'DEFAULT_CENTER': (46.5, 6.8),
    'DEFAULT_ZOOM': 10,
    'MIN_ZOOM': 3,
    'MAX_ZOOM': 18,
}

from .local_settings import *
