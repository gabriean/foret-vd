from .settings import *

ROOT_URLCONF = 'mobile.urls'

INSTALLED_APPS += ['mobile']

LOGIN_REDIRECT_URL = '/'
