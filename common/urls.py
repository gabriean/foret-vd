from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path, re_path

from django.contrib import admin

from inventory import views
from imports.views import CheckFBaseView, ImportView, ImportedListView


urlpatterns = [
    path('admin/', admin.site.urls),
    path('auth/', include('django.contrib.auth.urls')),
    path('select2/', include('django_select2.urls')),
    path('dbdocs/', include('dbdocs.urls')),

    path('', views.HomeView.as_view(), name='home'),
    path('entity/<int:pk>/data/', views.HomeEntityData.as_view(), name='entity-data'),
    re_path(r'^entity/(?P<name>(owner|municipality))/(?P<pk>\d+)/$',
        views.EntityView.as_view(), name='entity'),
    re_path(r'^plotobs/(?P<name>(owner|municipality))/(?P<pk>\d+)/$',
        views.PlotObsDataView.as_view(), name='plotobs'),
    path('plotobs/<int:pk>/json/', views.PlotObsDetailJSONView.as_view(), name='plotobs-detail-json'),
    re_path(r'^entity/(?P<name>(owner|municipality))/(?P<pk>\d+)/(?P<year>\d+)/fbase/$',
        views.EntityFbaseExport.as_view(), name='entity-fbase'),
    path('owner/analysis/<int:pk>/<int:year>/', views.OwnerAnalysisView.as_view(), name='owner_analysis'),
    path('plotobs/<int:pk>/', views.PlotObsDetailView.as_view(output='html'), name='plotobs_detail'),
    path('plotobs/planned/<int:pk>/', views.PlotObsPlannedDetailView.as_view(),
        name='plotobs_planned_detail'),
    path('plotobs/planned/<int:pk>/edit/', views.PlotObsPlannedEditView.as_view(),
        name='plotobs_planned_edit'),
    path('plotobs/planned/<int:prot_pk>/multiedit/', views.PlotObsPlannedMultiEditView.as_view(),
        name='plotobs_planned_edit_multi'),
    path('plotobs/planned/new/<int:prot_pk>/', views.PlotObsPlannedNewView.as_view(),
        name='plotobs_planned_new'),
    path('plotobs/planned/<int:pk>/delete/', views.PlotObsPlannedDeleteView.as_view(),
        name='plotobs_planned_delete'),

    path('data/', views.DataPageView.as_view(), name='data_page'),
    path('data/grid/', views.DataGridView.as_view(), name='data_grid'),
    path('docs/', views.DocumentationView.as_view(), name='docs'),
    path('essences/', views.EssencesView.as_view(), name='essences'),
    path('essences/export/', views.EssencesExportView.as_view(), name='essences-export'),
    path('comp-fbase/', views.FBaseComparisonView.as_view(), name='fbase-comp'),
    path('protocoles/', views.ProtocolesView.as_view(), name='protocoles'),
    path('protocoles/<int:pk>/', views.ProtocoleDetailView.as_view(), name='protocole'),
    path('protocoles/<int:pk>/plots/', views.ProtocolePlotsView.as_view(), name='protocole_plots'),
    path('protocoles/<int:pk>/edition/', views.ProtocoleUpdateView.as_view(), name='protocole_edit'),
    path('protocoles/<int:pk>/import-plots/', views.ProtocoleImportPlotsView.as_view(), name='protocole_import'),
    path('protocoles/<int:pk>/topapp/', views.TopapGeoJSONView.as_view(), name='topapp-layer'),
    path('protocoles/nouveau/futur/', views.ProtocoleCreateView.as_view(typ='future'), name='protocole_addfuture'),
    path('protocoles/nouveau/passe/', views.ProtocoleCreateView.as_view(typ='passed'), name='protocole_addpassed'),
    path('protocoles/par_prop/', views.ProtocolesByOwnerView.as_view(), name='protocoles_by_owner'),
    path('rfinfo/', views.RFInfoView.as_view(), name='rfinfo'),

    path('import/topap/', views.TopApImportView.as_view(), name='import_topap'),
    path('import/all/', ImportedListView.as_view(), name='import_list'),
    path('import/form/<int:pk>/', ImportView.as_view(), name='import'),
    path('import/<int:pk>/checkfbase/', CheckFBaseView.as_view(), name='checkfbase'),
    path('vocabulary/<int:pk>/', views.VocabView.as_view(), name='vocabulary'),
]

if settings.DEBUG:
    from pathlib import Path
    from posixpath import normpath
    from django.utils._os import safe_join
    from django.views.static import serve

    def serve_index(request, path, document_root=None, **kwargs):
        ''' Simulate auto-loading of index.html '''
        norm_path = normpath(path).lstrip('/')
        fullpath = Path(safe_join(document_root, norm_path))
        if fullpath.is_dir():
            path = path + 'index.html'
        return serve(request, path, document_root=document_root, **kwargs)

    # Should be an Apache alias in production
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(
        '/manuel/', view=serve_index, document_root=settings.BASE_DIR / 'manuel' / 'site'
    )
