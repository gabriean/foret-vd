# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='FileImport',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ifile', models.FileField(upload_to='imported')),
                ('protocol', models.FileField(null=True, upload_to='protocols', blank=True)),
                ('invent_year', models.SmallIntegerField(verbose_name="Ann\xe9e d'inventaire")),
                ('vocab_interv', models.TextField(verbose_name="Vocabulaire d'intervention")),
                ('imported', models.BooleanField(default=False)),
                ('spec_posE', models.ForeignKey(related_name='+', blank=True, to='inventory.TreeSpecies', max_length=150, null=True, on_delete=models.PROTECT, verbose_name='Esp\xe8ce en position E')),
                ('spec_posF', models.ForeignKey(related_name='+', blank=True, to='inventory.TreeSpecies', max_length=150, null=True, on_delete=models.PROTECT, verbose_name='Esp\xe8ce en position F')),
                ('vocab_pos16', models.ForeignKey(related_name='+', on_delete=models.PROTECT, verbose_name='Vocabulaire du champ r\xe9serv\xe9 1 (PARC, position 8)', blank=True, to='inventory.Vocabulary', null=True)),
                ('vocab_pos17', models.ForeignKey(related_name='+', on_delete=models.PROTECT, verbose_name='Vocabulaire du champ r\xe9serv\xe9 2 (PARC, position 9)', blank=True, to='inventory.Vocabulary', null=True)),
                ('vocab_posd6', models.ForeignKey(related_name='+', on_delete=models.PROTECT, verbose_name='Vocabulaire du champ r\xe9serv\xe9 3 (DESP, position 6)', blank=True, to='inventory.Vocabulary', null=True)),
                ('vocab_posd7', models.ForeignKey(related_name='+', on_delete=models.PROTECT, verbose_name='Vocabulaire du champ r\xe9serv\xe9 4 (DESP, position 7)', blank=True, to='inventory.Vocabulary', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='ImpError',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('no_line', models.SmallIntegerField()),
                ('raw_line', models.CharField(max_length=200)),
                ('message', models.TextField()),
                ('wrong', models.CharField(max_length=50)),
                ('fixed', models.CharField(default='', max_length=50, blank=True)),
                ('fixed_ok', models.BooleanField(default=False)),
                ('imp_file', models.ForeignKey(on_delete=models.CASCADE, related_name='errors', to='imports.FileImport')),
            ],
            options={
                'ordering': ('imp_file', 'no_line'),
            },
        ),
    ]
