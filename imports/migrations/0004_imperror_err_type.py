from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('imports', '0003_fileimport_year'),
    ]

    operations = [
        migrations.AddField(
            model_name='imperror',
            name='err_type',
            field=models.CharField(blank=True, choices=[('code_ill', 'Code tige illisible'), ('spec_unk', 'Code d’espèce inconnu'), ('no_prot', 'Pas de protocole'), ('too_prot', 'Plusieurs protocoles possibles'), ('prop_missing', 'Propriétaire inexistant'), ('uid_dbl', 'Identifiant unique déjà existant'), ('plot_dbl', 'Placette à double'), ('line_ill', 'Ligne illisible')], max_length=20, verbose_name='Type d’erreur'),
        ),
    ]
