from collections import OrderedDict, deque
from datetime import date
from pathlib import Path
import re
import sys

from django.contrib.gis.geos import Point
from django.core.exceptions import ValidationError
from django.db import DataError, IntegrityError, transaction
from django.utils.encoding import force_text

from inventory.models import Density, Nature, Owner, OwnerType, Plot, PlotObs, Protocol, Tree, TreeSpecies
from municipality.models import Municipality
from .models import FileImport, ImpError

header_regex = re.compile(r'[A-Z0-9_]+( I\d{2})?( <ERREUR>)?$')
# Format 1992 and before
main_line_regex_old = re.compile(
    r'A(?P<coordx>\d{4})(?P<coordy>\d{4})\s+B(?P<parc>\d{7}[0-9A-F]{,2})\s+'
    r'C(?P<inv>\d{6})\s+D(?P<desp>[0-9A-F][0-9A]\d[0-9A]\d?)\s+'
    r'0?(?P<gibi>[0-9A-F]{,9})(\s+(?P<trees>\*.*))?'
)
# Format after 1992
main_line_regex_new = re.compile(
    r'A(?P<coordx>\d{4})(?P<coordy>\d{4})\s+B(?P<parc>\d{7}[0-9A-F]{,2})\s+'
    r'C(?P<inv>\d{6})\s+D(?P<desp>[0-9A-F]{4}\d?)\s+'
    r'0(?P<gibi>[0-9A-F]{9})(\s+N(?P<id>\d{6}))?'
)
tree_regex = re.compile(r'\*(?P<spec>[1-9A-F])(?P<quality>\d)/(?P<diam>\d{1,3})\.')

class ImportingError(Exception):
    pass


class ValueExtractor:
    def __init__(self, descr, group, mstart, mend, vmin, vmax,
                 optional=False, to_int_base=16, transform=None):
        self.descr = descr
        self.group = group
        self.mstart = mstart
        self.mend = mend
        self.vmin = vmin
        self.vmax = vmax
        self.optional = optional
        self.to_int_base = to_int_base
        self.transform = transform

    def extract(self, groups):
        """
        Return the char(s) at specified position in the regex result.
        """
        try:
            if self.mstart is not None and self.mend is not None:
                val = groups[self.group][self.mstart:(self.mend + 1)]
            elif self.mstart is not None:
                val = groups[self.group][self.mstart]
            else:
                val = groups[self.group]
        except (IndexError, KeyError):
            if not self.optional:
                raise
            val = None
        return self.cast(val)

    def cast(self, value):
        if value is not None and self.to_int_base:
            value = int(value, self.to_int_base)
        if self.transform:
            value = self.transform(value)
        return value

    def validate(self, value):
        if (self.vmin, self.vmax) == (None, None):
            return
        if value is None and self.optional:
            return
        if self.vmin is not None and value < self.vmin:
            raise ValidationError("La valeur %s est en-dessous de la limite inférieure %d" % (
                value, self.vmin))
        if self.vmax is not None and value > self.vmax:
            raise ValidationError("La valeur %s est en-dessus de la limite supérieure %d" % (
                value, self.vmax))

class CoordValueExtractor(ValueExtractor):
    """
    Encoded coordinates only contain the 4 middle digits of the coordinate.
    """
    def __init__(self, descr, group, prefix, suffix):
        self.descr = descr
        self.group = group
        self.prefix = prefix
        self.suffix = suffix

    def extract(self, groups):
        return int(self.prefix + groups[self.group] + self.suffix)

    def validate(self, code):
        pass

class GibiValueExtractor(ValueExtractor):
    def extract(self, match):
        for idx, key in enumerate(('gibi_epicea', 'gibi_epicea_f', 'gibi_epicea_a',
                'gibi_conifer', 'gibi_conifer_f', 'gibi_conifer_a', 'gibi_leaved',
                'gibi_leaved_f', 'gibi_leaved_a')):
            self.obs[key] = int(code[idx], 16) * 10

extractors = OrderedDict((
    # Group A (COOR, 'A23107410')
    ('coordx', CoordValueExtractor("Coordonnée X", 'coordx', '5', '0')),
    ('coordy', CoordValueExtractor("Coordonnée Y", 'coordy', '1', '0')),

    # Group B (PARC, 'B42771074')
    ('owner_type', ValueExtractor("Type de propriété", 'parc', 0, None, 1, 7)),
    ('owner_num', ValueExtractor("Numéro de propriété", 'parc', 1, 3, 0, 999, to_int_base=10)),
    ('ser_num', ValueExtractor("Série", 'parc', 4, None, 0, 9)),
    ('div_num', ValueExtractor("Division", 'parc', 5, 6, 0, 99, to_int_base=10)),
    ('reserved1', ValueExtractor("Libre 1", 'parc', 7, None, None, None, optional=True, to_int_base=None,
        transform=lambda x:'' if x is None else x)),
    ('reserved2', ValueExtractor("Libre 2", 'parc', 8, None, None, None, optional=True, to_int_base=None,
        transform=lambda x:'' if x is None else x)),

    # Group C (INV, 'C152310')
    ('density', ValueExtractor("Densité d'échantillonnage", 'inv', 1, None, 0, 8)),
    ('slope', ValueExtractor("Pente en degrés", 'inv', 2, 3, 0, 99, to_int_base=10)),
    ('radius', ValueExtractor("Rayon en mètres", 'inv', 4, 5, 5, 50, to_int_base=10)),

    # Group D (DESP)
    ('unnumbered', ValueExtractor("Tiges non dénombrées", 'desp', 0, None, None, None,
        transform=lambda x:x*10)),
    ('conifer', ValueExtractor("Proportion tiges résineuses", 'desp', 1, None, 0, 100,
        transform=lambda x:x*10)),
    ('main_nature', ValueExtractor("Nature principale boisée", 'desp', 2, None, 0, 9)),
    ('main_perc', ValueExtractor("Proportion de nature principale", 'desp', 3, None, 60, 100,
        transform=lambda x:x*10)),
    ('interv_imp', ValueExtractor("Type d'intervention sylvicole", 'desp', 4, None, 0, 9,
        optional=True)),
    ('reserved3', ValueExtractor("Libre 3", 'desp', 5, None, None, None, optional=True, to_int_base=None,
        transform=lambda x:'' if x is None else x)),
    ('reserved4', ValueExtractor("Libre 4", 'desp', 6, None, None, None, optional=True, to_int_base=None,
        transform=lambda x:'' if x is None else x)),

    # Group 0 (GIBI)
    ('gibi_epicea', ValueExtractor("Nombre total d'épicéas", 'gibi', 0, None, None, None,
        transform=lambda x:x*10)),
    ('gibi_epicea_f', ValueExtractor("Proportion d'épicéa frotté", 'gibi', 1, None, 0, 100,
        transform=lambda x:x*10)),
    ('gibi_epicea_a', ValueExtractor("Proportion d'épicéa abroutis", 'gibi', 2, None, 0, 100,
        transform=lambda x:x*10)),
    ('gibi_conifer', ValueExtractor("Nombre total d'autres résineux", 'gibi', 3, None, None, None,
        transform=lambda x:x*10)),
    ('gibi_conifer_f', ValueExtractor("Proportion d'autres résineux frotté", 'gibi', 4, None, 0, 100,
        transform=lambda x:x*10)),
    ('gibi_conifer_a', ValueExtractor("Proportion d'autres résineux abroutis", 'gibi', 5, None, 0, 100,
        transform=lambda x:x*10)),
    ('gibi_leaved', ValueExtractor("Nombre total de feuillus", 'gibi', 6, None, None, None,
        transform=lambda x:x*10)),
    ('gibi_leaved_f', ValueExtractor("Proportion de feuillus frotté", 'gibi', 7, None, 0, 100,
        transform=lambda x:x*10)),
    ('gibi_leaved_a', ValueExtractor("Proportion de feuillus abroutis", 'gibi', 8, None, 0, 100,
        transform=lambda x:x*10)),
    # Group N (present after 2000 only)
    ('uid', ValueExtractor("Identifiant de ligne", 'id', None, None, None, None, optional=True,
        to_int_base=10)),
))

class ImpRecord:
    def __init__(self, importer, coordx, coordy, match1, line_no):
        """
        Decode all values from main line.
        """
        self.importer = importer
        self.coordx, self.coordy = coordx, coordy
        self.line_no = line_no
        self.trees = []
        self.values = {}
        self.importable = True
        groups = match1.groupdict()
        year = importer.import_obj.year

        # Gibi, fix format, TODO: define exact switch date
        if year < 1994:
            if groups['gibi'] in ('E', ''):
                groups['gibi'] = '000000000'
            else:
                groups['gibi'] = groups['gibi'].replace('F', '0')
            if len(groups['gibi']) < 9:
                # Right pad with '0's
                groups['gibi'] = groups['gibi'] + '0' * (9 - len(groups['gibi']))

        for key, extractor in extractors.items():
            if key == 'coordx':
                self.values[key] = int('5' + coordx + '0')
            elif key == 'coordy':
                self.values[key] = int('1' + coordy + '0')
            else:
                self.values[key] = extractor.extract(groups)
            try:
                extractor.validate(self.values[key])
            except ValidationError as err:
                err_msg = "champ %s: %s" % (extractor.descr, err.message)
                err = self.importer.add_error(line_no, err_msg, self.values[key], extractor=extractor)
                if err.fixed:
                    fixed_norm = extractor.cast(err.fixed)
                    try:
                        extractor.validate(fixed_norm)
                    except ValidationError as err:
                        pass
                    else:
                        self.values[key] = fixed_norm
                        self.importer.last_error_fixed()


        # Find owner
        msg = self.get_owner(self.values['owner_type'], self.values['owner_num'])
        if msg:
            err = self.importer.add_error(
                line_no, msg, self.values['owner_type'] + self.values['owner_num'], err_type='prop_missing'
            )
            if err.fixed:
                msg = self.get_owner(err.fixed[0], err.fixed[1:4])
            if msg:
                self.importable = False
                return
            else:
                self.importer.last_error_fixed()

        # Find protocol
        if (year, self.owner) in self.importer._used_protocols:
            # Optimize by getting protocols from cache by (year, owner)
            self.values['protocol'] = self.importer._used_protocols[(year, self.owner)]
        else:
            candidates = Protocol.objects.filter(year=year, owners=self.owner)
            if len(candidates) == 1:
                self.values['protocol'] = candidates[0]
                self.importer._used_protocols[(year, self.owner)] = self.values['protocol']
            elif len(candidates) == 0:
                msg = "Il n'existe pas encore de protocole pour l'année %d et le propriétaire %s" % (
                    year, self.owner)
                err = self.importer.add_error(line_no, msg, '', err_type='no_prot')
                fixed = False
                if err.fixed:
                    msg = self.get_owner(err.fixed[0], err.fixed[1:4])
                    if not msg:
                        try:
                            self.values['protocol'] = Protocol.objects.get(year=year, owners=self.owner)
                            self.importer.last_error_fixed()
                            fixed = True
                        except Protocol.DoesNotExist:
                            pass
                if not fixed:
                    self.importable = False
                    return
            elif len(candidates) > 1:
                # Compare with already used protocols and take one of them, if one result matches
                candidates = set(candidates) & set(self.importer._used_protocols.values())
                if len(candidates) == 1:
                    self.values['protocol'] = list(candidates)[0]
                else:
                    msg = "Il existe plus d'un protocole pour l'année %d et le propriétaire %s (%s)" % (
                        year, self.owner,
                        ", ".join(str(p) for p in Protocol.objects.filter(year=year, owners=self.owner)))
                    self.importer.add_error(line_no, msg, '', err_type='too_prot')
                    self.importable = False
                    return

        # Used to detect duplicates
        self.plot_code = 'A%s%s' % (match1.group('coordx'), match1.group('coordy'))
        self.center = Point(
            self.values['coordx'] + 2000000, self.values['coordy'] + 1000000, srid=2056)

        # Trees on main line (format 92 and before)
        try:
            trees = match1.group('trees')
        except IndexError:
            pass
        else:
            if trees:
                self.add_trees(trees, line_no)

    def get_owner(self, owner_type, owner_num):
        # Find owner
        if isinstance(owner_num, int):
            owner_num = '%03d' % owner_num
        try:
            self.owner = Owner.objects.get(typ__num_code=owner_type, num=owner_num)
        except Owner.DoesNotExist:
            return "Le propriétaire %s%s n'existe pas dans la base de données" % (
                OwnerType.objects.get(num_code=owner_type).alpha_code, owner_num
            )

    def add_trees(self, line, line_no):
        protocol = self.values['protocol']
        tree_codes = line.split()
        for code in tree_codes:
            m = tree_regex.match(code)
            if not m:
                err_msg = "Impossible de lire ce code de tige: %s" % (code,)
                err = self.importer.add_error(line_no, err_msg, code, err_type='code_ill')
                if err.fixed:
                    if err.fixed == '-':
                        # Ignoring
                        self.importer.last_error_fixed()
                        continue
                    m = tree_regex.match(err.fixed)
                    if not m:
                        continue
                    else:
                        self.importer.last_error_fixed()
                else:
                    continue
            diameter = int(m.group('diam'))
            # Let too low diameter, maybe checked in a subsequent quality check
            #if diameter < 10:
            #    err_msg = "Diamètre < 10 ignoré (%s)" % code
            #    self.importer.add_error(line_no, err_msg, code)
            #    continue
            quality = int(m.group('quality'))
            quality = {7: 'Qn', 8: 'Q-', 9: 'Q+'}.get(quality, 'Q%d' % quality)
            spec_code = m.group('spec')
            custom_spec_value = getattr(protocol, f'spec_pos{spec_code}')
            # If code = E or F, check a custom value is defined
            if ((spec_code == 'E' and not protocol.spec_posE) or
                    (spec_code == 'F' and not protocol.spec_posF)):
                err_msg = "Code d'espèce '%s' non défini" % spec_code
                self.importer.add_error(
                    line_no, err_msg, spec_code, err_type='spec_unk', protocol=protocol
                )
                continue
            spec = custom_spec_value or TreeSpecies.objects.get(code=spec_code)
            self.trees.append(Tree(spec=spec, diameter=diameter, quality=quality))

    def save(self, line_no):
        """
        Insert content into the database.
        """
        plot, created = Plot.objects.get_or_create(center=self.center)
        if created:
            plot.municipality = Municipality.get_from_point(plot.center)
            plot.save()

        self.values['density'] = Density.objects.get(code=self.values['density'])
        self.values['main_nature'] = Nature.objects.get(code=self.values['main_nature'])
        obs_values = dict([(k, v) for k, v in self.values.items() if k not in (
            'coordx', 'coordy', 'owner_type', 'owner_num',)])
        if obs_values['interv_imp'] is not None:
            obs_values['interv_int'] = getattr(obs_values['protocol'], 'interv_%d' % obs_values['interv_imp'])
        if obs_values['uid'] is not None:
            if PlotObs.objects.filter(uid=obs_values['uid']).exists():
                msg = "Erreur d'importation: l'identifiant unique (uid) %s existe déjà" % (obs_values['uid'],)
                self.importer.add_error(line_no, msg, "N%s" % obs_values['uid'], err_type='uid_dbl')
                return
        try:
            plot_obs = PlotObs.objects.create(
                plot=plot, imp_file=self.importer.import_obj, owner=self.owner, **obs_values
            )
        except DataError as err:
            raise ImportingError(
                "Erreur d'importation: %s\nValeurs à enregistrer: %s" % (err, obs_values)
            )

        for tree in self.trees:
            tree.obs = plot_obs
        Tree.objects.bulk_create(self.trees)


class Importer:
    def __init__(self, import_obj):
        # import_obj is a FileImport instance
        self.import_obj = import_obj
        self._errs = []
        self._fixed_errs = []
        self._used_protocols = {}  # Used protocols keyed by (year, owner)

    @classmethod
    def check_validity(cls, file_, check_dup_name=True):
        """
        Should be quick, called from the form clean method.
        Raise ValidationError if file not found valid.
        """
        # Second line should be in the form:
        # A23607470 B42771070 C152811 D160A2 0100103100  N378225
        try:
            line1 = file_.readline().strip()
            line2 = file_.readline().strip()
            if getattr(file_, 'encoding', None) != 'UTF-8':
                line1 = line1.decode('utf-8')
                line2 = line2.decode('utf-8')
        except IOError:
            raise ValidationError("Impossible de lire le fichier")
        file_name = Path(file_.name).name
        if check_dup_name and FileImport.objects.filter(ifile=f'imported/{file_name}').exists():
            raise ValidationError(f"Ce fichier semble déjà avoir été importé ({file_name})")
        m1 = main_line_regex_old.match(line2)
        m2 = main_line_regex_new.match(line2)
        if not any([m1, m2]):
            raise ValidationError(
                "Désolé, le contenu du fichier ne ressemble pas à un fichier IPASMOFIX valable.")

    def add_error(self, line_no, message, wrong, err_type='', extractor=None, **kwargs):
        if extractor is not None:
            # We should be able to spot the location of the error in line with extractor infos
            start_pos = self.current_line.index(self.res_main.groupdict()[extractor.group]) + extractor.mstart
            if extractor.mend:
                end_pos = start_pos + (extractor.mend - extractor.mstart)
            else:
                end_pos = start_pos + 1
            line = ImpError.add_err_span(self.current_line, start_pos, end_pos)
        else:
            line = self.current_line
        try:
            err = self.import_obj.errors.get(raw_line=line, no_line=line_no, message=message, wrong=wrong)
        except ImpError.DoesNotExist:
            # Errors are saved later, outside of the possible-rollbacked transaction
            err = ImpError(
                imp_file=self.import_obj, raw_line=line,
                no_line=line_no, err_type=err_type, message=message, wrong=wrong, **kwargs
            )
        self._errs.append(err)
        return err

    def last_error_fixed(self):
        self._fixed_errs.append(self._errs.pop())

    def save_record(self, idx):
        if self.record is not None and self.record.importable:
            self.imported.append(self.record.coordx + self.record.coordy)
            if self.continue_on_error or not self._errs:
                # Don't bother to save if the file contains errors
                self.record.save(idx)
        self.record = None

    def do_import(self, continue_on_error=False):
        """
        Run real import of data. This process *could* be run out of request.
        """
        self.import_obj.ifile.seek(0)
        # If it's a reimport, delete previously imported PlotObs objects, and errors with no fix
        self.import_obj.plotobs_set.all().delete()
        self.import_obj.errors.filter(fixed='').delete()

        self.continue_on_error = continue_on_error
        self.record = None
        self.imported = []  # Also use to detect coordinate duplicates
        year = self.import_obj.year
        self.main_line_regex = main_line_regex_old if year <= 1992 else main_line_regex_new

        self.prev_coords = deque(maxlen=3)
        self.errs_to_complete = []
        try:
            with transaction.atomic():
                for idx, line in enumerate(self.import_obj.ifile, start=1):
                    line = self.current_line = line.strip().decode('utf-8').strip('\x1b')
                    if len(line) < 2:  # Empty or isolated char
                        continue
                    if 'C000000 D00000' in line:  # Totally unvalid
                        continue
                    self.import_line(line, idx)

                # Save the last one
                self.save_record(idx)
                if self._errs and not self.continue_on_error:
                    # Raising an exception here has the side-effect of rollbacking the transaction
                    raise ImportingError("Des erreurs se sont produites durant l'importation")
        except ImportingError:
            self.imported = []
            self.import_obj.imported = False
        else:
            self.import_obj.imported = bool(len(self.imported))
        self.import_obj.full_clean()
        self.import_obj.save()

        if self._errs:
            # Saving new errors outside of the atomic block
            ImpError.objects.bulk_create([err for err in self._errs if err.pk is None])

        if self._fixed_errs:
            ImpError.objects.filter(pk__in=[err.pk for err in self._fixed_errs if not err.fixed_ok]).update(fixed_ok=True)
        return len(self.imported)

    def import_line(self, line, line_no, try_fixed=True):
        self.res_main = self.main_line_regex.match(line)
        if self.res_main is not None:
            # We got a main data line
            self.save_record(line_no)  # The previous one
            coordx, coordy = self.res_main.group('coordx'), self.res_main.group('coordy')
            plot_loc = coordx + coordy
            if plot_loc in self.imported:
                err = self.add_error(
                    line_no, "Erreur: placette à double", 'A%s' % plot_loc,
                    err_type='plot_dbl',
                    context={'before': list(self.prev_coords), 'after': []},
                )
                if err.fixed:
                    coordx, coordy = err.fixed[-8:-4], err.fixed[-4:]
                    plot_loc = coordx + coordy
                    if plot_loc in self.imported:
                        return False
                    else:
                        self.last_error_fixed()
                else:
                    self.errs_to_complete.append(err)
                    return False

            self.prev_coords.append('A' + plot_loc)
            # Add the plotloc context following the error(s)
            for err in self.errs_to_complete:
                err.context['after'].append('A' + plot_loc)
                if len(err.context['after']) >= 3:
                    self.errs_to_complete.remove(err)

            # Start a new record
            self.record = ImpRecord(self, coordx, coordy, self.res_main, line_no)
        elif header_regex.match(line):
            # Header line, may be in the middle of a file!
            self.save_record(line_no)
        elif tree_regex.match(line.split()[0]):
            if self.record is not None and self.record.importable:
                self.record.add_trees(line, line_no)
        else:
            if line_no == 1:  # Don't bother importing/reporting an unknown first line
                return False
            err = self.add_error(
                line_no, "Impossible d'interpréter la ligne", line, err_type='line_ill'
            )
            if err.fixed and try_fixed:
                # Retry with new line
                succeed = self.import_line(err.fixed, line_no, try_fixed=False)
                if (succeed):
                    # last_error_fixed unusable because other errors might have been added
                    self._fixed_errs.append(self._errs.pop(self._errs.index(err)))
                return succeed
            else:
                return False
        return True
