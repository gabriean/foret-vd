from datetime import date
import os

from django.conf import settings
from django.contrib.gis.geos import Point
from django.core.exceptions import ValidationError
from django.urls import reverse
from django.test import TestCase

from inventory.models import OwnerType, Owner, Plot, PlotObs, Protocol, Tree
from inventory.tests.utils import BaseDataMixin, MockFile
from .imports import Importer, ImportingError
from .models import FileImport

TEST_FILES_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), "test_files")


class ImportTests(BaseDataMixin, TestCase):

    def tearDown(self):
        dir_to_clean = os.path.join(settings.MEDIA_ROOT, 'imported')
        for f in os.listdir(dir_to_clean):
            if f.startswith("TEST"):
                os.remove(os.path.join(dir_to_clean, f))

    def assert_gibi(self, obs, *values):
        self.assertListEqual(
            [obs.gibi_epicea, obs.gibi_epicea_f, obs.gibi_epicea_a,
             obs.gibi_conifer, obs.gibi_conifer_f, obs.gibi_conifer_a,
             obs.gibi_leaved, obs.gibi_leaved_f, obs.gibi_leaved_a],
            list(values))

    def test_form_import(self):
        file_path = os.path.join(TEST_FILES_DIR, "TEST.TER")
        self.client.login(username='user', password='password')
        with open(file_path) as f:
            form_data = {'ifile': f, 'year': '2005'}
            response = self.client.post(reverse('import', args=[0]), form_data, follow=True)

        self.assertContains(response, "Les données du fichier TEST.TER ont été importées")
        self.assertEqual(FileImport.objects.count(), 1)

    def test_bad_file_content(self):
        imp_data = FileImport(ifile=MockFile('VALL1 I08\r\nA2 B427 C15 D1 0100  N225\r\ngarbage'))
        with self.assertRaisesMessage(ValidationError, "ne ressemble pas"):
            Importer(imp_data).check_validity(imp_data.ifile)

    def test_good_file_content(self):
        test_f = MockFile(
            'VALL1 I08\r\n'
            'A23607470 B42771070 C152811 D160A2 0100103E02  N378225\r\n'
            ' *67/020. *67/021. *67/025. *47/015. *57/021.\r\n'
            ' *57/011. *67/015. *67/022.\r\n'
            'A23507460 B42771072 C152311 D000A4 0110000209  N378226\r\n'
            ' *47/011. *47/013. *47/016. *47/022. *E7/054.')
        num, impfile = self.import_data(test_f, 2005)
        self.assertEqual(impfile.errors.count(), 0,
                         "Errors found: %s" % ', '.join(i.message for i in impfile.errors.all()))
        self.assertEqual(num, 2)

        self.assertEqual(Plot.objects.all().count(), 2)
        self.assertEqual(PlotObs.objects.all().count(), 2)
        self.assertEqual(
            set([p.center.wkt for p in Plot.objects.all()]),
            set((Point(2523600, 1174700, srid=2056).wkt, Point(2523500, 1174600, srid=2056).wkt))
        )
        obs1, obs2 = PlotObs.objects.all()
        self.assertEqual(obs1.uid, 378225)
        self.assertEqual(obs1.unnumbered, 10)
        self.assertEqual(obs1.protocol, self.protocol)
        self.assertEqual(obs1.slope, 28)
        self.assertEqual(obs1.main_nature.code, 0)
        self.assertEqual(obs1.main_perc, 100)
        self.assertEqual(obs1.interv_imp, 2)
        self.assertEqual(obs1.interv_int.name, 'Soins culturaux')
        self.assertEqual(obs1.gibi_epicea, 10)
        self.assertEqual(obs1.gibi_conifer, 10)
        self.assertEqual(obs1.gibi_leaved, 140)
        self.assertEqual(obs1.tree_set.count(), 8)
        self.assertEqual(obs2.tree_set.count(), 5)
        self.assertEqual(obs1.tree_set.all()[0].quality, 'Qn')
        # TODO more assertions

    def _test_static_plot_value_changed(self):
        pass
        #plot = Plot.objects.create(...)
        #import content with different static value for same plot
        #test ImportError raised

    def test_format_92(self):
        self.protocol.year = 1992
        self.protocol.save()
        old_protocol = Protocol.objects.create(year=1992, inv_date="Printemps 1992")
        old_protocol.owners.set([
            Owner.objects.create(
                typ=OwnerType.objects.get(alpha_code='E'),
                num='84')
        ])
        test_f = MockFile(
            'VALLOR1 I92\r\n'
            'A23607470 B20841103 C152811 D400A4 0E *17/33. *87/11.\r\n'
            ' *67/20. *67/21. *67/25. *47/15. *57/21.\r\n'
            'A16207180 B427721000 C151011 D008A 0E\r\n'
            'VALLOR61 I92 <ERREUR>\r\n'
            'A23507460 B20841106 C152311 D100A4 0FFFFFF5F1 *87/71. *67/16.\r\n'
            ' *47/11. *47/13. *47/16. *47/22. *47/54.')
        num, impfile = self.import_data(test_f, 1992)
        self.assertEqual(impfile.errors.count(), 0,
                         "Errors found: %s" % ", ".join(i.message for i in impfile.errors.all()))
        self.assertEqual(num, 3)

        obs1, obs2, obs3 = PlotObs.objects.all()
        self.assertIsNone(obs1.uid)
        self.assert_gibi(obs1, 0,0,0,0,0,0,0,0,0)
        self.assert_gibi(obs3, 0,0,0,0,0,0,50,0,10)
        self.assertEqual(obs1.tree_set.count(), 7)
        self.assertEqual(obs2.tree_set.count(), 0)

    def test_format_97(self):
        self.protocol.year = 1997
        self.protocol.save()
        test_f = MockFile(
            'MORRENS1 I97\r\n'
            'A38156235 B42771060 C160509 D000A3 0000000000\r\n'
            '*50/015. *50/023. *50/024. *50/022. *50/017.\r\n')
        num, impfile = self.import_data(test_f, 1997)
        self.assertEqual(impfile.errors.count(), 0,
                         "Errors found: %s" % ", ".join(i.message for i in impfile.errors.all()))
        self.assertEqual(num, 1)

    def test_bad_line(self):
        test_f = MockFile(
            'VALL1 I08\r\n'
            'A23607470 B42771070 C152811 D160A2 0100103100  N378221\r\n'
            ' *67/020. *67/021. *67/025. *47/015. *57/021.\r\n'
            'A23507460 B49991072 C152311 C4 D000A4 0110000209  N378224\r\n'  # C4 is error
            ' *47/011. *47/013. *47/016. *47/022. *E7/054.\r\n'
            'A23607520 B42771072 C152311 D000A4 0110000209  N378226\r\n'
            ' *47/011.\r\n')
        num, impfile = self.import_data(test_f, 2005)
        self.assertEqual(impfile.errors.count(), 1)
        self.assertEqual(num, 0)
        self.assertEqual(
            impfile.errors.all()[0].message, "Impossible d'interpréter la ligne"
        )
        # Fixed data
        err = impfile.errors.first()
        err.fixed = 'A23507460 B42771072 C152311 D000A4 0110000209  N378224'
        err.save()
        importer = Importer(impfile)
        num = importer.do_import()
        self.assertEqual(impfile.errors.count(), 1)
        self.assertEqual(num, 3)

    def test_duplicate_plots(self):
        test_f = MockFile(
            'VALL1 I08\r\n'
            'A23607470 B42771070 C152811 D160A2 0100103100  N378221\r\n'
            ' *67/020. *67/021. *67/025. *47/015. *57/021.\r\n'
            'A23607480 B42771070 C152811 D160A2 0100103100  N378222\r\n'
            ' *67/020. *67/021. *67/025. *47/015. *57/021.\r\n'
            'A23607490 B42771070 C152811 D160A2 0100103100  N378223\r\n'
            ' *67/020. *67/021. *67/025. *47/015. *57/021.\r\n'
            'A23607500 B42771070 C152811 D160A2 0100103100  N378224\r\n'
            ' *67/020. *67/021. *67/025. *47/015. *57/021.\r\n'
            'A23607470 B42771070 C152811 D160A2 0100103100  N378225\r\n' # The dup
            ' *67/020. *67/021. *67/025. *47/015. *57/021.\r\n'
            'A23607520 B42771072 C152311 D000A4 0110000209  N378226\r\n'
            ' *47/011.\r\n')
        num, impfile = self.import_data(test_f, 2005)
        self.assertEqual(impfile.errors.count(), 1,
                         "Errors found: %s" % ", ".join(i.message for i in impfile.errors.all()))
        error = impfile.errors.all()[0]
        self.assertIn("placette à double", error.message)
        self.assertEqual(error.wrong, 'A23607470')
        self.assertEqual(error.err_type, 'plot_dbl')
        self.assertEqual(
            error.context,
            {'before': ['A23607480', 'A23607490', 'A23607500'], 'after': ['A23607520']}
        )

        # Try second import, should not generate more errors
        importer = Importer(impfile)
        num = importer.do_import()
        self.assertEqual(num, 0)
        self.assertEqual(impfile.errors.count(), 1)

        # Try second import with fixed data
        err = impfile.errors.first()
        err.fixed = 'A23607510'
        err.save()
        importer = Importer(impfile)
        num = importer.do_import()
        # Import should succeed, but error record stays
        self.assertEqual(num, 6)
        self.assertEqual(impfile.errors.count(), 1)

    def test_wrong_owner(self):
        Owner.objects.create(typ=OwnerType.objects.get(num_code=4), num=555) 
        test_f = MockFile(
            'VALL1 I08\r\n'
            'A23507460 B49991072 C152311 D000A4 0110000209  N378226\r\n'
            ' *47/011. *47/013. *47/016. *47/022. *E7/054.')
        num, impfile = self.import_data(test_f, 2005)
        self.assertEqual(impfile.errors.count(), 1)
        self.assertEqual(num, 0)
        self.assertEqual(
            impfile.errors.all()[0].message,
            "Le propriétaire C999 n'existe pas dans la base de données"
        )
        # Fixed with existing owner but protocol is missing
        err = impfile.errors.first()
        err.fixed = '4555'
        err.save()
        importer = Importer(impfile)
        num = importer.do_import()
        self.assertEqual(impfile.errors.count(), 2)
        self.assertEqual(
            impfile.errors.filter(err_type='no_prot')[0].message,
            "Il n'existe pas encore de protocole pour l'année 2005 et le propriétaire C555"
        )
        self.assertEqual(num, 0)
        # The good one
        err = impfile.errors.first()
        err.fixed = '4277'
        err.save()
        importer = Importer(impfile)
        num = importer.do_import()
        self.assertEqual(impfile.errors.count(), 2)
        self.assertEqual(num, 1)

    def test_duplicate_plot_obs_uids(self):
        test_f = MockFile(
            'VALL1 I08\r\n'
            'A23607420 B42771070 C152811 D160A2 0100103100  N378225\r\n'
            ' *67/020. *67/021. *67/025. *47/015. *57/021.\r\n'
            'A23607470 B42771072 C152311 D000A4 0110000209  N378225\r\n'
            ' *47/011.\r\n')
        num, impfile = self.import_data(test_f, 2005)
        self.assertEqual(impfile.errors.count(), 1)
        self.assertIn("l'identifiant unique (uid)", impfile.errors.all()[0].message)

    def test_value_outofrange(self):
        test_f = MockFile(
            'VALL1 I08\r\n'
            'A23607470 B42771070 C152802 D160A2 0100103100  N378225\r\n'
            ' *67/020. *67/021. *67/025. *47/015. *57/021.\r\n')
        num, impfile = self.import_data(test_f, 2005)
        self.assertEqual(num, 0)
        self.assertEqual(impfile.errors.count(), 1)

        # Try second import with fixed data
        err = impfile.errors.first()
        err.fixed = '12'
        err.save()
        importer = Importer(impfile)
        num = importer.do_import()
        # Import should succeed, but error record stays
        self.assertEqual(num, 1)
        self.assertEqual(impfile.errors.count(), 1)
        self.assertEqual(PlotObs.objects.all()[0].radius, 12)

    def test_unreadable_tree_entry(self):
        test_f = MockFile(
            'VALL1 I08\r\n'
            'A23607470 B42771070 C152811 D160A2 0100103100  N378225\r\n'
            ' *67/020. 67-076 *47/015.\r\n')
        num, impfile = self.import_data(test_f, 2005)
        self.assertEqual(num, 0)
        self.assertEqual(impfile.errors.count(), 1)
        err = impfile.errors.first()
        err.fixed = '*67/076.'
        err.save()
        # Try second import with fixed data
        importer = Importer(impfile)
        num = importer.do_import()
        # Import should succeed, but error record stays
        self.assertEqual(num, 1)
        self.assertEqual(impfile.errors.count(), 1)
        self.assertEqual(Tree.objects.count(), 3)

    def test_import_even_with_errors(self):
        # First owner, unknown, latest 2: duplicate_plots, only the second line
        # should be imported
        test_f = MockFile(
            'VALL1 I08\r\n'
            'A24605320 B49991070 C152811 D160A2 0100103100  N378225\r\n'
            ' *67/020. *67/021. *67/025. *47/015. *57/021.\r\n'
            'A23607470 B42771070 C152811 D160A2 0100103100  N378225\r\n'
            ' *67/020. *67/021. *67/025. *47/015. *57/021.\r\n'
            'A23607470 B42771072 C152311 D000A4 0110000209  N378226\r\n'
            ' *47/011.\r\n')
        num, impfile = self.import_data(test_f, 2005, continue_on_error=True)
        self.assertEqual(impfile.errors.count(), 2)
        self.assertEqual(num, 1)
        self.assertEqual(PlotObs.objects.all().count(), 1)
        self.assertEqual(impfile.plotobs_set.count(), 1)
        # Re-import with fixed duplicate data
        err = impfile.errors.filter(message__contains='placette à double')[0]
        err.fixed = 'A23607480'
        err.save()
        importer = Importer(impfile)
        num = importer.do_import(continue_on_error=True)
        self.assertEqual(num, 2)
        self.assertEqual(PlotObs.objects.all().count(), 2)
        self.assertEqual(impfile.errors.count(), 2)
