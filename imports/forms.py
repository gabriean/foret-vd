from datetime import date

from django import forms
from django.forms.models import modelformset_factory
from django.core.exceptions import ValidationError
from django.urls import reverse
from django.utils.safestring import mark_safe

from inventory.models import TreeSpecies, Vocabulary
from .imports import Importer, tree_regex
from .models import FileImport, ImpError


class ImportForm(forms.ModelForm):
    '''
    imp_file = forms.FileField(label="Fichier à importer")
    res_vocab1 = forms.ModelChoiceField(queryset=Vocabulary.objects.all().order_by('name'),
        label="Vocabulaire du champ libre 1 (position 16)", required=False)
    res_vocab2 = forms.ModelChoiceField(queryset=Vocabulary.objects.all().order_by('name'),
        label="Vocabulaire du champ libre 2 (position 17)", required=False)
    '''
    year = forms.IntegerField(label="Année d'inventaire",
                              widget=forms.TextInput(attrs={'size':'4'}))
    cont_on_error = forms.BooleanField(label="Importer malgré les erreurs", initial=False, required=False)

    class Meta:
        model = FileImport
        fields = ['ifile', 'year']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance and self.instance.pending_errors().exists():
            ErrorsFormSet = modelformset_factory(ImpError, form=ImpErrorForm, extra=0)
            formset_kwargs = kwargs.copy()
            formset_kwargs.pop('instance', None)
            self.error_fset = ErrorsFormSet(queryset=self.instance.errors.filter(fixed_ok=False), **formset_kwargs)
        else:
            self.error_fset = None

    def is_valid(self):
        return all([super().is_valid() and (self.error_fset.is_valid() if self.error_fset else True)])

    def clean_ifile(self):
        imp_file = self.cleaned_data['ifile']
        Importer.check_validity(imp_file, check_dup_name=self.instance.pk is None)
        return imp_file

    def save(self, **kwargs):
        imp_file = super().save(**kwargs)
        if self.error_fset:
            self.error_fset.save()
        return imp_file


class ImpErrorForm(forms.ModelForm):
    class Meta:
        model = ImpError
        fields = ['fixed']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance.err_type == 'code_ill':
            self.fields['fixed'].help_text = 'Écrivez un tiret (-) pour ignorer'
            self.fields['fixed'].widget.attrs['placeholder'] = '*xx/xx.'
            self.fields['fixed'].widget.attrs['size'] = '8'
        elif self.instance.err_type == 'spec_unk':
            if self.instance.protocol:
                self.fields['fixed'].help_text = mark_safe(
                    'ou corrigez dans le <a href="%s">protocole</a>' % reverse(
                        'admin:inventory_protocol_change', args=[self.instance.protocol_id]
                    )
                )
            else:
                self.fields['fixed'].help_text = 'ou corrigez dans le protocole'
        elif self.instance.err_type == 'plot_dbl':
            self.fields['fixed'].widget.attrs['placeholder'] = 'Axxxxyyyy'
            self.fields['fixed'].widget.attrs['size'] = '10'
        elif self.instance.err_type == 'line_ill':
            self.fields['fixed'].widget.attrs['size'] = '80'
        elif self.instance.err_type in ('no_prot', 'prop_missing'):
            self.fields['fixed'].help_text = 'ou ajoutez un nouveau protocole'
            self.fields['fixed'].widget.attrs['size'] = '5'
            self.fields['fixed'].widget.attrs['placeholder'] = 'xxxx'

    def clean_fixed(self):
        value = self.cleaned_data.get('fixed')
        # Depending on err type, add some validation
        if self.instance.err_type == 'code_ill' and value:
            m = tree_regex.match(value)
            if not m:
                raise forms.ValidationError("Le code de tige doit respecter la syntaxe *xx/xx.")
        return value
