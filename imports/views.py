from collections import defaultdict

from django.contrib import messages
from django.db import transaction
from django.db.models import Count
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views.generic import FormView, ListView, TemplateView

from inventory.models import PlotObs, VocabValue
from .forms import ImpErrorForm, ImportForm
from .imports import Importer, ImportingError
from .models import FileImport, ImpError


class ImportedListView(ListView):
    model = FileImport
    template_name = 'imports/list.html'

    def get_queryset(self):
        return FileImport.objects.annotate(errs=Count('errors', distinct=True)
            ).annotate(succ=Count('plotobs', distinct=True)
            ).order_by('-year', 'ifile')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        imp_prot_map = defaultdict(list)
        for res in PlotObs.objects.values('imp_file', 'protocol').distinct():
            imp_prot_map[res['imp_file']].append(res['protocol'])
        context['protocols'] = imp_prot_map
        return context


class ImportView(FormView):
    form_class = ImportForm
    template_name = 'imports/import.html'

    def get(self, request, *args, **kwargs):
        if int(kwargs['pk']) > 0:
            self.object = FileImport.objects.get(pk=kwargs['pk'])
        else:
            self.object = None
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if int(kwargs['pk']) > 0:
            self.object = FileImport.objects.get(pk=kwargs['pk'])
        else:
            self.object = None
        return super().post(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if self.object:
            kwargs.update({'instance': self.object})
        return kwargs

    def get_initial(self):
        return dict(('interv_%d'% int(vocv.code), vocv.label)
                    for vocv in VocabValue.objects.filter(vocab__name='Intervention'))

    def form_valid(self, form):
        imp_file = form.save()
        imp_file.fbase_check = None
        imp_file.save()
        importer = Importer(imp_file)
        continue_on_error = form.cleaned_data.get('cont_on_error', False)
        num_imported = importer.do_import(continue_on_error=continue_on_error)

        if imp_file.pending_errors().exists():
            # Non fatal errors
            messages.warning(self.request,
                "Des erreurs ont été rencontrées durant l'importation.")
            if not imp_file.imported:
                messages.warning(self.request, "Les données de ce fichier n'ont pas été importées.")
                return HttpResponseRedirect(reverse('import', args=[imp_file.pk]))

        messages.success(self.request,
            "Les données du fichier %s ont été importées (%d placettes)." % (
                form.cleaned_data['ifile'].name, num_imported))
        return HttpResponseRedirect(reverse('import_list'))


class CheckFBaseView(TemplateView):
    template_name = 'imports/checkfbase.html'

    def get_context_data(self, **kwargs):
        imp = get_object_or_404(FileImport, pk=self.kwargs['pk'])
        if not imp.fbase_check or self.request.GET.get('clear') == '1':
            imp.check_fbases()
        return {
            **super().get_context_data(**kwargs),
            'imp': imp,
        }
