# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = []

    operations = [
        migrations.CreateModel(
            name='Municipality',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('objectid', models.IntegerField()),
                ('name', models.CharField(max_length=32, verbose_name='Nom', db_column='lad_nommin')),
                ('name_up', models.CharField(max_length=32, verbose_name='Nom (maj.)', db_column='lad_nommaj')),
                ('canton', models.IntegerField(db_column='lad_nocant', choices=[(2, 'BE'), (10, 'FR'), (22, 'VD'), (23, 'VS'), (24, 'NE'), (25, 'GE'), (9999, 'Autre')])),
                ('lad_idsffn', models.IntegerField()),
                ('lad_nofedc', models.IntegerField()),
                ('lad_nocanc', models.IntegerField(verbose_name='Numéro cantonal')),
                ('lad_nomm_1', models.CharField(max_length=16)),
                ('lad_nomm_2', models.CharField(max_length=16)),
                ('lad_nocand', models.IntegerField()),
                ('lad_nofedd', models.IntegerField()),
                ('shape_leng', models.FloatField()),
                ('shape_area', models.FloatField()),
                ('geom', django.contrib.gis.db.models.fields.MultiPolygonField(srid=21781)),
            ],
            options={
                'ordering': ('name',),
                'db_table': 'municipality',
                'verbose_name': 'Commune',
            },
        ),
    ]
