import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('municipality', '0003_remove_unused_fields'),
    ]

    operations = [
        migrations.CreateModel(
            name='Arrondissement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('no', models.PositiveSmallIntegerField(verbose_name='Numéro')),
                ('nom', models.CharField(max_length=50)),
                ('geom', django.contrib.gis.db.models.fields.MultiPolygonField(srid=21781)),
            ],
            options={
                'db_table': 'arrondissements',
            },
        ),
    ]
